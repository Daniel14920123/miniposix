#ifndef MINIPOSIX_SYSCALLS_H
#define MINIPOSIX_SYSCALLS_H

#ifndef uint32_t
#define uint32_t unsigned int
#endif

#ifndef size_t
#define size_t uint32_t
#endif

#define pid_t unsigned short
#define signal_t unsigned short

#ifndef uint8_t
#define uint8_t unsigned char
#endif

#define ___SYSCALL0(code) ___SYSCALL_STACK_b0(code, dummy)

#define ___SYSCALL_STACK_b0(code, xxx) \
	{asm volatile("pusha"); \
	asm volatile("mov %%eax, %%eax": : "a" (&code)); \
	asm volatile("int $0x80");\
	asm volatile("popa");}


#define ___SYSCALL_STACK_b1(code, value) \
	{int buff[2] = {0};\
	buff[0] = (int)code; \
	buff[1] = (int)value; \
	asm volatile("pusha"); \
	asm volatile("mov %%eax, %%eax": : "a" (buff)); \
	asm volatile("int $0x80");\
	asm volatile("popa");}

#define ___SYSCALL(n, code, value) ___SYSCALL_STACK_b##n (code, value)

void __exit(int status);
void print(char* str);
int fork();
int thread();
int getpid();
int getppid();
/**
 * @brief Send to a port
 * @param port_pid The port which will receive the message
 * @param msg Pointer to the message to send
 * @param msg_len The size of the msg
 */
void send_ipc(size_t port_id, void* msg, size_t msg_len);
void receive_ipc(size_t port_pid, void* buffer, size_t len, void (*callback)(void*, size_t, pid_t));
size_t bind_port();
pid_t request(size_t port_id, void* buffer, size_t len);
void* sbrk(int len);
void wait();
int write (int fd, char *buf, int count);
void _sighandler_return();
void _set_signal_handler(signal_t* signal_swap, void (*handler)(void));
void send_signal(signal_t sig, pid_t proc);
void _execve(char* path, char** argv, char** environ);
void request_io_permission();
void* get_phys_buff(void* addr, size_t size);
char receive_irqs_on(uint32_t port_number, uint32_t irq_number);
char bind_that_port(uint32_t port_id);
char mmap(void* addr, uint32_t length, uint8_t flags);
char munmap(void* addr, uint32_t length);

#undef uint8_t
#undef size_t
#undef uint32_t

#endif