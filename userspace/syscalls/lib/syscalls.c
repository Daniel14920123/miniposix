#include "../../../header/defines/defines.h"
#include "syscalls.h"

#define uint32_t unsigned int
#define size_t uint32_t
#define pid_t unsigned short
#define signal_t unsigned short
#define uint8_t unsigned char

static int parent_port = -1;

/**
 * @brief Waits a children to terminate. Uses port based ipc.
 * 
 */
void wait(){
    char buffer[10];
    request(parent_port, buffer, 10);
}

/**
 * @brief Exits task
 * 
 * @param status status code used to exit the task.
 */
void __exit(int status){
    ___SYSCALL(1, EXIT, &status);
}

/**
 * @brief Debugging syscall used to pring without having to map the VGA buffer.
 * Uses the kernel routines.
 * 
 * @param str Null-terminated string to print
 */
void print(char* str){
    ___SYSCALL(1, PRINT, str);
}

/**
 * @brief Forks a process. Creates a perfect copy of the current task, with two differents pid
 * 
 * @return int - The child pid in the father, 0 in the childre,
 */
int fork(){
    int to_return;
    if(parent_port == -1)
        parent_port = (int)bind_port();
    ___SYSCALL(1, FORK, &to_return);
    return to_return;
}

/**
 * @brief Gets the current pid
 * 
 * @return int - The current process' pid
 */
int getpid(){
    int to_return;
    ___SYSCALL(1, GETPID, &to_return);
    return to_return;
}

/**
 * @brief Gets the parent pid
 * 
 * @return int - The current parent's pid
 */
int getppid(){
    int to_return;
    ___SYSCALL(1, GETPPID, &to_return);
    return to_return;
}

/**
 * @brief Sends a message by IPC. This is asynchronous, but used on both receiving protocol.
 * 
 * @param port_id - The destination port
 * @param msg - The address of the message to send
 * @param msg_len - The length of the message to send
 */
void send_ipc(size_t port_id, void* msg, size_t msg_len){
    typedef struct {
        size_t signal;
        size_t data_size;
        void* data;
    } message;
    typedef struct
    {
        size_t port_pid;
        message msg;
    } send_ipc_params;
    send_ipc_params sip = { port_id, (message){ 0, msg_len, msg} };
    ___SYSCALL(1, SEND, &sip);
}

/**
 * @brief Receives a message via IPC. Asynchronous. Calls the callback if success.
 * 
 * @param port_pid The port from which the message will be pulled.
 * @param buffer The destination buffer.
 * @param len The length of the destination buffer. Kernel prevents overflows.
 * @param callback The function to call if a message has been pulled successfully.
 */
void receive_ipc(size_t port_pid, void* buffer, size_t len, void (*callback)(void*, size_t, pid_t)){
     typedef struct{
            size_t port_pid;
            void* buffer;
            size_t len;
            size_t written;
            pid_t sender;
        } send_ipc_params;
    send_ipc_params sip = {port_pid, buffer, len, 0};
    ___SYSCALL(1,RECEIVE,&sip);
    if(sip.written && callback)
        callback(buffer, sip.written, sip.sender);
}

/**
 * @brief Gets a port, starting at port 30.
 * 
 * @return size_t The port number.
 */
size_t bind_port(){
    size_t port;
    ___SYSCALL(1, BIND_PORT, &port);
    return port;
}

/**
 * @brief Synchronous reception routine. Process will be paused according to the desired port.
 * It will awoken only if you receive a message on this port. Warning with deadlocks.
 * 
 * @param port_id The port from which the message will be pulled.
 * @param buffer The destination buffer.
 * @param len The length of the destination buffer. Kernel prevents overflows.
 * @return pid_t Returns the pid of the process which sent the message.
 */
pid_t request(size_t port_id, void* buffer, size_t len){
        typedef struct{
            size_t port_pid;
            void* buffer;
            size_t len;
            size_t written;
            pid_t sender;
        } send_ipc_params;
    send_ipc_params sip = {port_id, buffer, len, 0, 0};
    ___SYSCALL(1, RECEIVE, &sip);
    if(sip.written)
        return sip.sender;
    ___SYSCALL(1, REQUEST, &sip);
    ___SYSCALL(1, RECEIVE, &sip);
    return sip.sender;
}

/**
 * @brief Expands heap.
 * 
 * @param len The desired incrementation.
 * @return void* The buffer to the beginning of the incremented buffer (last program break).
 */
void* sbrk(int len){
    typedef struct{
        void* addr;
        size_t len;
    } brk_info;
    brk_info info = {.addr = 0, .len = len};
    ___SYSCALL(1,SBRK, &info);
    return info.addr;
}

/**
 * @brief Don't use this. It won't work. The filesystem is in userspace in a microkernel
 * 
 * @param fd 
 * @param buf 
 * @param count 
 * @return int 
 */
int write (int fd, char *buf, int count){
    char temp = buf[count];
    buf[count] = 0;
    print(buf);
    buf[count] = temp;
}

/**
 * @brief Mandatory to return from a sighandler. Not using it is undefined behavior.
 * 
 */
void _sighandler_return(){
    int code = SIGHANDLER_RETURN;
    ___SYSCALL(0, code, dummy);
}

/**
 * @brief Sets a signal handler. Used in crt0. Signals are totally configurable.
 * 
 * @param signal_swap A buffer of size two to store the signal triggered in the userspace.
 * @param handler The sighandler function.
 */
void _set_signal_handler(signal_t* signal_swap, void (*handler)(void)){
    typedef struct{
        uint32_t sighandler;
        signal_t* sigdef;
    } sighandler_info;
    sighandler_info info = (sighandler_info){.sighandler = (uint32_t)handler, .sigdef = signal_swap};
    ___SYSCALL(1, SET_SIGNAL_HANDLER, &info);
}

/**
 * @brief Send a signal to a process.
 * 
 * @param sig Signal code to send.
 * @param proc Process that will receive the signal.
 */
void send_signal(signal_t sig, pid_t proc){
    typedef struct{
        pid_t proc;
        signal_t sig;
    } signal_info;
    signal_info info = {.proc = proc, .sig = sig};
    ___SYSCALL(1, SEND_SIGNAL, &info);
}

/**
 * @brief execve. uses an offset in ramdisk. Minimal implementation for booting. path and argv will be mapped in destination space.
 * 
 * @param path - The path "\0\0" for first file. "\1\0" for file two.
 * @param argv - The command line arguments.
 * @param environ - The environment passed to the process.
 */
void _execve(char* path, char** argv, char** environ){
    typedef struct {
        char* path;
        char** argv;
        char** environ;
    } execve_struct;
    execve_struct info = {.path = path, .argv = argv, .environ = environ};
    ___SYSCALL(1, EXECVE, &info);
}

/**
 * @brief Requests permission from kernel to use in and out assembly instructions without triggering a general protection fault.
 * 
 */
void request_io_permission(){
    int a = RQST_IO_PERMS;
    ___SYSCALL0(a);
}

/**
 * @brief Maps a physical ram buffer in process heap. Uses program break (sbrk(0))
 * 
 * @param addr - The buffer start addr. Better have it 4Kb aligned to not messwith other regions.
 * @param size - The size of the buffer to map. It will be converted to the next 4096 multiple by the kernel.
 * @return void* - Address of the mapped buffer in the memory.
 */
void* get_phys_buff(void* addr, size_t size){
    typedef struct{
        size_t addr;
        size_t len;
        void* mapped_addr;
    } phys_buff_info;
    phys_buff_info info = {.addr = (size_t)addr, .len = size, .mapped_addr = 0};
    ___SYSCALL(1, GET_PHYS_BUFF, &info);
    return info.mapped_addr;
}

/**
 * @brief Sets the kernel to forward irqs on a port. The message will be empty. It's process' job to use in and out instructions.
 * 
 * @param port_number - The port id to which have the irqs forwarded.
 * @param irq_number - The IRQ to forward. Note : It's the IRQ notation and NOT an offset in the IDT.
 * @return char - 0 if it failed, another value otherwise.
 */
char receive_irqs_on(uint32_t port_number, uint32_t irq_number){
    typedef struct{
            uint32_t port_number;
            unsigned short irq_number;
            char has_succeded;
    } broadcast_struct;
    broadcast_struct info = {.port_number=port_number, .irq_number=irq_number, .has_succeded=0};
    ___SYSCALL(1,RECEIVE_IRQS_ON,&info);
    return info.has_succeded;
}

/**
 * @brief Syscall to get pseudo-randomly-generated number. In ramdisk context, the clock being predictable, you will always get the same sequence.
 * 
 * @param seed A seed to randomize the rand function.
 * @return size_t The random value.
 */
size_t rand(size_t* seed){
    ___SYSCALL(1,RANDOM,seed);
    return *seed;
}

/**
 * @brief Creates a new thread from this process.
 * Currently sbrk() mapped pages will be shared. For other pages, the behavior is undefined, 
 * but will work on little scales.
 * 
 * @return int - if you are the detached thread, another value otherwise.
 */
int thread()
{
    int to_ret;
    ___SYSCALL(1, THREAD, &to_ret);
    return to_ret;
}

/**
 * @brief Bind a specific port. Note, it is the only way to access ports 1-29.
 * 
 * @param port_id The id of the port to bind.
 * @return char - 0 if failed, another value otherwise.
 */
char bind_that_port(uint32_t port_id){
        typedef struct{
            uint32_t port;
            char status;
        } bind_port_msg;
        bind_port_msg port = (bind_port_msg){.port=port_id,.status=0}; 
    ___SYSCALL(1,BIND_THAT_PORT,&port);
    return port.status;

}

#define uint8_t unsigned char

/**
 * @brief Maps a new page in the current process space.
 * 
 * @param addr The address to wwhich map the memory.
 * @param length The minimal length to get.
 * @param flags The paging flags to set. You can also set the Stack Flag.
 * @return char - 0 if failed, another value otherwise.
 */
char mmap(void* addr, uint32_t length, uint8_t flags){
    typedef struct{
            uint32_t addr; //below 1M
            uint32_t length;
            uint8_t success;
            uint8_t flags; // bool
        } mmap_info;
    mmap_info info = (mmap_info){.addr=addr, .length=length,.success=0 ,.flags=flags};
    ___SYSCALL(1,MMAP, &info);
    return info.success;
}

/**
 * @brief Unmaps each 4Kb page completely in the range [addr; addr+length]
 * 
 * @param addr The addr to start the unmapping. Won't take effect below 1 Mb (the kernel is there).
 * @param length The length to unmap. Below 4Mb will never take effect.
 * @return char - 0 if failed, another value otherwise
 */
char munmap(void* addr, uint32_t length){
    typedef struct{
        uint32_t addr;
        uint32_t length;
        uint8_t success;
    } munmap_info;
    if(length < 0x1000)
        return 0;
    munmap_info info = (munmap_info){.addr=addr, .length=length,.success=0};
    ___SYSCALL(1,MUNMAP, &info);
    return info.success;
}