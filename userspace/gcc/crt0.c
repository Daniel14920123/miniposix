#include "syscalls.h"
#include "../../header/defines/defines.h"

typedef struct{
    unsigned char mode;
    void (*crt0_def)(void);
    void (*userset)(void);
} signals;

void segfault(){
    print("Segmentation Fault (core dumped).");
    __exit(1);
}

void invalid_arith(){
    print("Invalid arithmetic operation");
    __exit(1);
}

void invalid_perm(){
    print("Privilege error");
    __exit(1);
}

signals lol[MAX_SIGNALS];
signal_t signal;

void handler_default(){
    print("sighandler called\n");
    if(lol[signal].mode | 1 && lol[signal].userset){
        lol[signal].userset();
    }
    if(lol[signal].mode | 2 && lol[signal].crt0_def){
        lol[signal].crt0_def();
    }

    _sighandler_return();
}


char** environ;

extern int main();

void normal_end(){
    __exit(0);
}

void _start(){
    //char** environ;
    char** params;
    asm volatile("mov %%eax,%0":"=r"(environ):);
    asm volatile("mov %%ebx,%0":"=r"(params):);
    print("_start called\n");
    if(environ)
        print(environ[0]);
    lol[SIGTERM] = (signals){.mode = 3, .userset = 0, .crt0_def = &normal_end};
    lol[SIGSEGV] = (signals){.mode = 2, .userset = 0, .crt0_def = &segfault};
    lol[SIGARITH] = (signals){.mode = 2, .userset = 0, .crt0_def = &invalid_arith};
    lol[SIGPERM] = (signals){.mode = 2, .userset = 0, .crt0_def = &invalid_perm};
    _set_signal_handler(&signal, &handler_default);
    int ret = main();
    __exit(ret);
}