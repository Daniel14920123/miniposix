/**
 * @file std.h
 * @author daniel.frederic
 * @brief includes the headers with macros (no syscalls and standalone)
 * @version 1.0
 * @date 2021-01-12
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef kSTD_H
#define kSTD_H

#pragma GCC diagnostic ignored "-Wint-conversion"
#pragma GCC diagnostic ignored "-Wdiscarded-qualifiers"
#include <float.h>
#include <iso646.h>
#include <limits.h>
#include <stdalign.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdnoreturn.h>
#include <stdint.h>

#endif