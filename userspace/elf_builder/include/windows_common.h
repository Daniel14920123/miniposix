#ifndef WINDOW_COMMON
#define WINDOW_COMMON

#define WM_ADD_WINDOW 0

#ifndef uint32_t
#define uint32_t unsigned int
#endif

typedef struct{
    char code;
    uint32_t message;
} WM_Request;

#endif