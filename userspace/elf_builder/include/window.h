#ifndef OSS117_WINDOWS_H_INCLUDED
#define OSS117_WINDOWS_H_INCLUDED
#include "syscalls.h"
#include "utils.h"
#include "vga_struct.h"
#include "windows_common.h"


#define bool char


struct Widget;
typedef struct {
    void (*on_click)(struct Widget*,int,int);
    void (*draw)(struct Widget*);
} Function;

typedef struct Widget
{
    Function function;
    Box box;
    struct Widget* brother;
    struct Widget* child;
    bool clickable;
    uint8_t color;
} Widget;


void draw(Widget*);
void initWindow(Widget*);
#endif