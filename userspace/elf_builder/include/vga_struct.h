#ifndef VGA_STRUCT_H
#define VGA_STRUCT_H

#ifndef uint64_t
#define uint64_t unsigned long long
#endif
#ifndef uint32_t
#define uint32_t size_t
#endif
#ifndef uint16_t
#define uint16_t unsigned short
#endif
#ifndef uint8_t
#define uint8_t unsigned char
#endif

#define D_BOX 1
#define D_COMMAND 2


#define VGA_SET_3h 0
#define VGA_SET_13h 1

#define VGA_COMMUNICATION_PORT 1

typedef struct
{
    size_t x;
    size_t y;
    size_t w;
    size_t h;
} Box;

typedef struct
{
    uint8_t color;
    Box box;
}  vga_request_box ;

typedef struct
{
    uint64_t nb_pixels;
    uint32_t *pixels_x;
    uint32_t *pixels_y;
    uint8_t *pixels_color;
} vga_request_pixels;

typedef union {
    vga_request_box box;
    uint32_t code;
} vga_body;

typedef struct{
    uint8_t type;
    size_t port;
    vga_body body;
} vga_message;

#endif