
#ifndef GTGRAS
#define GTGRAS

#include "syscalls.h"
#include "utils.h"
#include "vga_struct.h"
#include "windows_common.h"

#define NULL 0

#define VGA_COMMUNICATION_PORT 1
// #define WINDOW_MANAGER_COMMUNICATION_PORT 1

#define GTK_CONTAINER 1


struct Widget;
typedef struct {
    void (*on_click)(struct Widget*,int,int);
    void (*draw)(struct Widget*);
} Function;

typedef struct Widget
{
    char type;
    Function function;
    void* data;
    struct Widget* brother;
    struct Widget* child;
} Widget;

typedef struct {
    Box box;
    uint8_t color;
} GTK_Container;

void RegisterWindows();


#endif