#ifndef UTILS_H
#define UTILS_H

#ifndef NULL
#define NULL ((void*)0)
#endif

#ifndef uint64_t
#define uint64_t unsigned long long
#endif

#ifndef size_t
#define size_t unsigned int
#endif

#ifndef uint32_t
#define uint32_t unsigned int
#endif

#ifndef size_t
#define size_t unsigned int
#endif

#ifndef uint16_t
#define uint16_t unsigned short
#endif

#ifndef uint8_t
#define uint8_t unsigned char
#endif

#include "../include/stdarg.h"

void *memset(void *b, int c, size_t len);
void *memcpy(void *dest, const void *src, size_t n);
void sprintf(char *format, char *buff, va_list args);
void printf(char *format, ...);
char strcmp(char* s, char* cmp);


#endif