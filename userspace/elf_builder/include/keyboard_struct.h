#ifndef KEYBOARD_STRUCT_H
#define KEYBOARD_STRUCT_H

#ifndef uint64_t
#define uint64_t unsigned long long
#endif
#ifndef uint32_t
#define uint32_t size_t
#endif
#ifndef uint16_t
#define uint16_t unsigned short
#endif
#ifndef uint8_t
#define uint8_t unsigned char
#endif

#define KEYBOARD_SET_RECEIVER 1
#define KEYBOARD_STOP_RECEIVER 2
#define KEYBOARD_RESTAURE_RECEIVER 3
#define KEYBOARD_PAUSE_RECEIVER 4
#define KEYBOARD_RESUME_RECEIVER 5
#define KEYBOARD_COMMUNICATION_PORT 2


typedef struct{
    uint8_t type;
    size_t content;
} IN_KEYBOARD_message;


static const unsigned char keyboard_map[128] =
    {
        0, 27, '1', '2', '3', '4', '5', '6', '7', '8',    /* 9 */
        '9', '0', '-', '=', '\b',                         /* Backspace */
        '\t',                                             /* Tab */
        'q', 'w', 'e', 'r',                               /* 19 */
        't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',     /* Enter key */
        0,                                                /* 29   - Control */
        'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', /* 39 */
        '\'', '`', 0,                                     /* Left shift */
        '\\', 'z', 'x', 'c', 'v', 'b', 'n',               /* 49 */
        'm', ',', '.', '/', 0,                            /* Right shift */
        '*',
        0,   /* Alt */
        ' ', /* Space bar */
        0,   /* Caps lock */
        0,   /* 59 - F1 key ... > */
        0, 0, 0, 0, 0, 0, 0, 0,
        0, /* < ... F10 */
        0, /* 69 - Num lock*/
        0, /* Scroll Lock */
        0, /* Home key */
        0, /* Up Arrow */
        0, /* Page Up */
        '-',
        0, /* Left Arrow */
        0,
        0, /* Right Arrow */
        '+',
        0, /* 79 - End key*/
        0, /* Down Arrow */
        0, /* Page Down */
        0, /* Insert Key */
        0, /* Delete Key */
        0, 0, 0,
        0, /* F11 Key */
        0, /* F12 Key */
        0, /* All other keys are undefined */
};


static const unsigned char keyboard_map_azerty[128] =
    {
        0, 27, '&', 0, '"', '\'', '(', '-', 0, '_',    /* 9 */
        0, 0, ')', '=', '\b',                         /* Backspace */
        '\t',                                             /* Tab */
        'a', 'z', 'e', 'r',                               /* 19 */
        't', 'y', 'u', 'i', 'o', 'p', '0', '$', '\n',     /* Enter key */
        0,                                                /* 29   - Control */
        'q', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', /* 39 */
        '\'', '*', 0,                                     /* Left shift */
        '<', 'w', 'x', 'c', 'v', 'b', 'n',               /* 49 */
        ',', ';', ':', '!', 0,                            /* Right shift */
        '*',
        0,   /* Alt */
        ' ', /* Space bar */
        0,   /* Caps lock */
        0,   /* 59 - F1 key ... > */
        0, 0, 0, 0, 0, 0, 0, 0,
        0, /* < ... F10 */
        0, /* 69 - Num lock*/
        0, /* Scroll Lock */
        0, /* Home key */
        0, /* Up Arrow */
        0, /* Page Up */
        '-',
        0, /* Left Arrow */
        0,
        0, /* Right Arrow */
        '+',
        0, /* 79 - End key*/
        0, /* Down Arrow */
        0, /* Page Down */
        0, /* Insert Key */
        0, /* Delete Key */
        0, 0, 0,
        0, /* F11 Key */
        0, /* F12 Key */
        0, /* All other keys are undefined */
};

static const unsigned char keyboard_map_azertyUP[128] =
    {
        0, 27, '1', '2', '3', '4', '5', '6', '7', '8',    /* 9 */
        '9', '0', '-', '+', '\b',                         /* Backspace */
        '\t',                                             /* Tab */
        'A', 'Z', 'E', 'R',                               /* 19 */
        'T', 'Y', 'U', 'I', 'O', 'P', '0', '0', '\n',     /* Enter key */
        0,                                                /* 29   - Control */
        'Q', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', /* 39 */
        '%', 0, 0,                                     /* Left shift */
        '>', 'W', 'X', 'C', 'V', 'B', 'N',               /* 49 */
        '?', '.', '/', 0, 0,                            /* Right shift */
        '*',
        0,   /* Alt */
        ' ', /* Space bar */
        0,   /* Caps lock */
        0,   /* 59 - F1 key ... > */
        0, 0, 0, 0, 0, 0, 0, 0,
        0, /* < ... F10 */
        0, /* 69 - Num lock*/
        0, /* Scroll Lock */
        0, /* Home key */
        0, /* Up Arrow */
        0, /* Page Up */
        '-',
        0, /* Left Arrow */
        0,
        0, /* Right Arrow */
        '+',
        0, /* 79 - End key*/
        0, /* Down Arrow */
        0, /* Page Down */
        0, /* Insert Key */
        0, /* Delete Key */
        0, 0, 0,
        0, /* F11 Key */
        0, /* F12 Key */
        0, /* All other keys are undefined */
};


static const unsigned char keyboard_map_azertyALT[128] =
    {
        0, 27, 0, '~', '#', '{', '[', '|', '`', '\\',    /* 9 */
        0, '@', ']', '}', '\b',                         /* Backspace */
        '\t',                                             /* Tab */
        'a', 'z', 'e', 'r',                               /* 19 */
        't', 'y', 'u', 'i', 'o', 'p', '0', '$', '\n',     /* Enter key */
        0,                                                /* 29   - Control */
        'q', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', /* 39 */
        '\'', '`', 0,                                     /* Left shift */
        '\\', 'w', 'x', 'c', 'v', 'b', 'n',               /* 49 */
        ',', ';', ':', '!', 0,                            /* Right shift */
        '*',
        0,   /* Alt */
        ' ', /* Space bar */
        0,   /* Caps lock */
        0,   /* 59 - F1 key ... > */
        0, 0, 0, 0, 0, 0, 0, 0,
        0, /* < ... F10 */
        0, /* 69 - Num lock*/
        0, /* Scroll Lock */
        0, /* Home key */
        0, /* Up Arrow */
        0, /* Page Up */
        '-',
        0, /* Left Arrow */
        0,
        0, /* Right Arrow */
        '+',
        0, /* 79 - End key*/
        0, /* Down Arrow */
        0, /* Page Down */
        0, /* Insert Key */
        0, /* Delete Key */
        0, 0, 0,
        0, /* F11 Key */
        0, /* F12 Key */
        0, /* All other keys are undefined */
};


#endif