#include "syscalls.h"
#include "utils.h"
#include "malloc.h"

char* strcpy(char* src, char* dst){
    while(*(src)) *(dst++) = *(src++);
    return dst;
}

int main(){
    init_heap();
    
    char* lock = malloc(2);
    char* string = malloc(50);
    char* plouf = "Initial string.\n";
    char* to_set = "Set by other thread.\n";

    lock[0] = 1;
    lock[1] = 1;

    if(thread()){
        //the detached thrads pritns the computed data.
        strcpy(plouf, string);
        for(int i = 0; i < 5; i++){
            print(string);
        }
        lock[2] = 0;
        while(lock[1]);
        for(int i = 0; i < 5; i++){
            print(string);
        }
        return 0;
    } else {
        //the original thread computes the string.
        while(lock[2]);
        strcpy(to_set, string);
        lock[1] = 0;
        return 1;
    }

}