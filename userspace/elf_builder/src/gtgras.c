#include "../include/gtgras.h"
#include "../include/utils.h"
#include "../include/malloc.h"
#include "../include/vga_struct.h"
static size_t communication_port;

uint8_t GetColorIndex(uint8_t r, uint8_t g, uint8_t b)
{
    {
        if (r == 0x00 && g == 0x00 && b == 0x00)
            return 0x00; // black
        if (r == 0x00 && g == 0x00 && b == 0xA8)
            return 0x01; // blue
        if (r == 0x00 && g == 0xA8 && b == 0x00)
            return 0x02; // green
        if (r == 0xA8 && g == 0x00 && b == 0x00)
            return 0x04; // red
        if (r == 0xFF && g == 0xFF && b == 0xFF)
            return 0x3F; // white
        return 0x00;
    }
}

void RegisterWindows()
{
    communication_port = bind_port();
    WM_Request* buffer = malloc(sizeof(WM_Request));
    *buffer = (WM_Request){.code = WM_ADD_WINDOW, .message = communication_port};

  //  send_ipc(WINDOW_MANAGER_COMMUNICATION_PORT,buffer ,sizeof(WM_Request));
}

void initGTK(Widget *container)
{
    RegisterWindows();
}

void DrawBoxColor(Box box, uint8_t color)
{
    vga_message *buffer = malloc(sizeof(vga_message));
    // print("send_data");
    buffer->type = D_BOX;
    buffer->box = (vga_request_box){.color = color, .box = box};
    send_ipc(VGA_COMMUNICATION_PORT, buffer, sizeof(vga_message));
}

void DrawContainer(Widget *widget)
{

    GTK_Container *container = (GTK_Container *)widget->data;
    DrawBoxColor(container->box, container->color);
    if (widget->child)
        widget->child->function.draw(widget->child);
    if (widget->brother)
        widget->brother->function.draw(widget->brother);
}

Widget *CreateContainer(GTK_Container container)
{
    Widget *new = (Widget *)malloc(sizeof(Widget));
    GTK_Container *container_loc = (GTK_Container *)malloc(sizeof(GTK_Container));
    new->type = GTK_CONTAINER;
    new->data = container_loc;
    new->function = (Function){.draw = &DrawContainer, .on_click = NULL};
    return new;
}

int main()
{
    init_heap();
    DrawBoxColor((Box){.x=0, .y = 0, .w=320,.h=200}, GetColorIndex(0xA8, 0x00, 0x00));
   // DrawBoxColor((Box){.x=50, .y = 50, .w=320,.h=200}, GetColorIndex(0xA8, 0xA8, 0x00));
    return 0;
    Widget *main_window = CreateContainer((GTK_Container){
        .box = (Box){.x = 0, .y = 0, .w = 320, .h = 200},
        .color = GetColorIndex(0, 0, 0xA8)});
    print("nat");
    DrawContainer(main_window);

    return 123;
}