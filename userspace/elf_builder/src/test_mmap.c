#include "syscalls.h"
#include "utils.h"

int main(){
    mmap(0xceadb000, 0x1000-1, 1);
    print("Ici on a map une page donc on ne va pas segfault:\n");
    *((uint32_t*)0xceadb000) = 500;
    print("Ici on va segfault apres avoir demap la page:\n");
    munmap(0xceadb000, 0x1000);
    *((uint32_t*)0xceadb000) = 200;
}