#include "syscalls.h"
#include "utils.h"

// FORK: 0 pour le fils
//      PID pour le père

void callback(void* buffer, size_t written, pid_t sender){
    print((char*)buffer);
    __exit(1024);
}

int main(){
    // This port belong to the father
    size_t port = bind_port();
    char buffer[10];
    if(fork()){
        while (1)
        {
            receive_ipc(port, buffer,10, &callback);
        }
        
    } else {
        // Code du fils
        send_ipc(port, "Hello dad", 10);
    }
    return 0;
}