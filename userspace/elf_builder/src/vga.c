#include "syscalls.h"
#include "vga_struct.h"
#include "malloc.h"
#include "utils.h"

#define bool uint8_t
#define false 0
#define true 1

#define MISC_PORT 0x3c2
#define CRTC_INDEX_PORT 0x3d4
#define CRTC_DATA_PORT 0x3c5
#define SEQUENCER_INDEX_PORT 0X3C4
#define SEQUENCER_DATA_PORT 0X3C5
#define GRAPICS_CONTROLLER_INDEX_PORT 0X3CE
#define GRAPICS_CONTROLLER_DATA_PORT 0X3Cf

#define ATTRIBUTE_CONTROLLER_INDEX_PORT 0X3C0
#define ATTRIBUTE_CONTROLLER_READ_PORT 0X3C1
#define ATTRIBUTE_CONTROLLER_WRITE_PORT 0X3C0
#define ATTRIBUTE_CONTROLLER_RESET_PORT 0X3DA

#define VGA_AC_INDEX 0x3C0
#define VGA_AC_WRITE 0x3C0
#define VGA_AC_READ 0x3C1
#define VGA_MISC_WRITE 0x3C2
#define VGA_SEQ_INDEX 0x3C4
#define VGA_SEQ_DATA 0x3C5
#define VGA_DAC_READ_INDEX 0x3C7
#define VGA_DAC_WRITE_INDEX 0x3C8
#define VGA_DAC_DATA 0x3C9
#define VGA_MISC_READ 0x3CC
#define VGA_GC_INDEX 0x3CE
#define VGA_GC_DATA 0x3CF
/*			COLOR emulation		MONO emulation */
#define VGA_CRTC_INDEX 0x3D4 /* 0x3B4 */
#define VGA_CRTC_DATA 0x3D5  /* 0x3B5 */
#define VGA_INSTAT_READ 0x3DA

#define VGA_NUM_SEQ_REGS 5
#define VGA_NUM_CRTC_REGS 25
#define VGA_NUM_GC_REGS 9
#define VGA_NUM_AC_REGS 21
#define VGA_NUM_REGS (1 + VGA_NUM_SEQ_REGS + VGA_NUM_CRTC_REGS + \
                      VGA_NUM_GC_REGS + VGA_NUM_AC_REGS)

static inline uint8_t Port8Bit_read(uint16_t port_number) {
  uint8_t result;
  __asm__ volatile("inb %1, %0" : "=a"(result) : "Nd"(port_number));
  return result;
}
static inline uint16_t Port16Bit_read(uint16_t port_number) {
  uint16_t result;
  __asm__ volatile("inw %1, %0" : "=a"(result) : "Nd"(port_number));

  return result;
}
static inline uint32_t Port32Bit_read(uint16_t port_number) {
  uint32_t result;
  __asm__ volatile("inl %1, %0" : "=a"(result) : "Nd"(port_number));

  return result;
}

static inline void Port8Bit_write(uint16_t port_number, uint8_t _data){
    __asm__ volatile("outb %0, %1" : : "a"(_data), "Nd"(port_number));
}

static inline void Port16Bit_write(uint16_t port_number, uint16_t _data){
    __asm__ volatile("outw %0, %1" : : "a"(_data), "Nd"(port_number));
}

static inline void Port32Bit_write(uint16_t port_number, uint32_t _data){
    __asm__ volatile("outl %0, %1" : : "a"(_data), "Nd"(port_number));
}

uint8_t GetColorIndex(uint8_t r, uint8_t g, uint8_t b)
{
    {
        if (r == 0x00 && g == 0x00 && b == 0x00)
            return 0x00; // black
        if (r == 0x00 && g == 0x00 && b == 0xA8)
            return 0x01; // blue
        if (r == 0x00 && g == 0xA8 && b == 0x00)
            return 0x02; // green
        if (r == 0xA8 && g == 0x00 && b == 0x00)
            return 0x04; // red
        if (r == 0xFF && g == 0xFF && b == 0xFF)
            return 0x3F; // white
        return 0x00;
    }
}

void WriteRegisters2(uint8_t *regs)
{
    unsigned i;

    /* write MISCELLANEOUS reg */
    Port8Bit_write(VGA_MISC_WRITE, *regs);
    regs++;
    /* write SEQUENCER regs */
    for (i = 0; i < VGA_NUM_SEQ_REGS; i++)
    {
        Port8Bit_write(VGA_SEQ_INDEX, i);
        Port8Bit_write(VGA_SEQ_DATA, *regs);
        regs++;
    }
    /* unlock CRTC registers */
    Port8Bit_write(VGA_CRTC_INDEX, 0x03);
    Port8Bit_write(VGA_CRTC_DATA, Port8Bit_read(VGA_CRTC_DATA) | 0x80);
    Port8Bit_write(VGA_CRTC_INDEX, 0x11);
    Port8Bit_write(VGA_CRTC_DATA, Port8Bit_read(VGA_CRTC_DATA) & ~0x80);
    /* make sure they remain unlocked */
    regs[0x03] |= 0x80;
    regs[0x11] &= ~0x80;
    /* write CRTC regs */
    for (i = 0; i < VGA_NUM_CRTC_REGS; i++)
    {
        Port8Bit_write(VGA_CRTC_INDEX, i);
        Port8Bit_write(VGA_CRTC_DATA, *regs);
        regs++;
    }
    /* write GRAPHICS CONTROLLER regs */
    for (i = 0; i < VGA_NUM_GC_REGS; i++)
    {
        Port8Bit_write(VGA_GC_INDEX, i);
        Port8Bit_write(VGA_GC_DATA, *regs);
        regs++;
    }
    /* write ATTRIBUTE CONTROLLER regs */
    for (i = 0; i < VGA_NUM_AC_REGS; i++)
    {
        (void)Port8Bit_read(VGA_INSTAT_READ);
        Port8Bit_write(VGA_AC_INDEX, i);
        Port8Bit_write(VGA_AC_WRITE, *regs);
        regs++;
    }
    /* lock 16-color palette and unblank display */
    (void)Port8Bit_read(VGA_INSTAT_READ);
    Port8Bit_write(VGA_AC_INDEX, 0x20);
}

void WriteRegisters(uint8_t *registers)
{

    // Misc
    Port8Bit_write(MISC_PORT, *(registers++));

    // Sequencer

    for (uint8_t i = 0; i < 5; i++)
    {
        Port8Bit_write(SEQUENCER_INDEX_PORT, i);
        Port8Bit_write(SEQUENCER_DATA_PORT, *(registers++));
    }

    // Cathode ray tube controller
    Port8Bit_write(CRTC_INDEX_PORT, 0x03);
    Port8Bit_write(CRTC_DATA_PORT, Port8Bit_read(CRTC_DATA_PORT) | 0x80);
    Port8Bit_write(CRTC_INDEX_PORT, 0x11);
    Port8Bit_write(CRTC_DATA_PORT, Port8Bit_read(CRTC_DATA_PORT) & ~0x80);

    registers[0x03] = registers[0x03] | 0x80;
    registers[0x11] &= ~0x80;

    for (uint8_t i = 0; i < 25; i++)
    {
        Port8Bit_write(CRTC_INDEX_PORT, i);
        Port8Bit_write(CRTC_DATA_PORT, *(registers++));
    }

    // Grahpics controller
    for (uint8_t i = 0; i < 9; i++)
    {
        Port8Bit_write(GRAPICS_CONTROLLER_INDEX_PORT, i);
        Port8Bit_write(GRAPICS_CONTROLLER_DATA_PORT, *(registers++));
    }
    for (uint8_t i = 0; i < 21; i++)
    {
        Port8Bit_read(ATTRIBUTE_CONTROLLER_RESET_PORT);
        Port8Bit_write(ATTRIBUTE_CONTROLLER_INDEX_PORT, i);
        Port8Bit_write(ATTRIBUTE_CONTROLLER_WRITE_PORT, *(registers++));
    }

    Port8Bit_read(ATTRIBUTE_CONTROLLER_RESET_PORT);
    Port8Bit_write(ATTRIBUTE_CONTROLLER_INDEX_PORT, 0x20);
}

bool SupportMode(uint32_t width, uint32_t height, uint32_t colordepth)
{

    return width == 320 && height == 200 && colordepth == 8;
}

void pixel_fast(int x, int y, uint8_t r, uint8_t g, uint8_t b)
{
    unsigned char *VGA = (unsigned char *)0xA0000;
    uint8_t color = GetColorIndex(r, g, b);
    VGA[y * 320 + x] = color;
}

bool SetMode(uint32_t width, uint32_t height, uint32_t colordepth)
{

    if (!SupportMode(width, height, colordepth))
        return false;

    unsigned char g_320x200x256[] =
        {
            /* MISC */
            0x63,
            /* SEQ */
            0x03, 0x01, 0x0F, 0x00, 0x0E,
            /* CRTC */
            0x5F, 0x4F, 0x50, 0x82, 0x54, 0x80, 0xBF, 0x1F,
            0x00, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x9C, 0x0E, 0x8F, 0x28, 0x40, 0x96, 0xB9, 0xA3,
            0xFF,
            /* GC */
            0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x0F,
            0xFF,
            /* AC */
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
            0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
            0x41, 0x00, 0x0F, 0x00, 0x00};

    WriteRegisters2(g_320x200x256);

    return true;
}

void Unset(){
#if 0
    unsigned char g_80x25_text[] = {
    /* MISC */
        0x67,
    /* SEQ */
        0x03, 0x00, 0x03, 0x00, 0x02,
    /* CRTC */
        0x5F, 0x4F, 0x50, 0x82, 0x55, 0x81, 0xBF, 0x1F,
        0x00, 0x4F, 0x0D, 0x0E, 0x00, 0x00, 0x00, 0x50,
        0x9C, 0x0E, 0x8F, 0x28, 0x1F, 0x96, 0xB9, 0xA3,
        0xFF,
    /* GC */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x0E, 0x00,
        0xFF,
    /* AC */
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x14, 0x07,
        0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F,
        0x0C, 0x00, 0x0F, 0x08, 0x00
    };
#else

    unsigned char g_80x25_text[] =
    {
        /* MISC */
        0x67,
        /* SEQ */
        0x03, 0x00, 0x03, 0x00, 0x02,
        /* CRTC */
        0x5F, 0x4F, 0x50, 0x82, 0x55, 0x81, 0xBF, 0x1F,
        0x00, 0x4F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x9C, 0x8E, 0x8F, 0x28, 0x1F, 0x96, 0xB9, 0xA3,
        0xFF,
        /* GC */
        0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x0E, 0x0F,
        0xFF,
        /* AC */
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
        0x0C, 0x00, 0x0F, 0x08, 0x00};
#endif
    // uint16_t* txt_mode = get_phys_buff((void*)0xb8000, 80*25*2);

    WriteRegisters2(g_80x25_text);
}

uint8_t *GetFrameBufferSegment()
{
    static char first_run = 1;
    static uint8_t *value = 0;
    if (first_run)
    {
        first_run = 0;
        Port8Bit_write(GRAPICS_CONTROLLER_INDEX_PORT, 0x06);
        uint8_t segmentNumber = Port8Bit_read(GRAPICS_CONTROLLER_DATA_PORT) & (0x03 << 2);

        switch (segmentNumber)
        {
        case 0 << 2:
        {
            value = get_phys_buff((void*)0x00000, 320*200);
            return value;
        }
        case 1 << 2:
        {

            value = get_phys_buff((void*)0xA0000, 320*200);
            return (uint8_t *)value;
        }
        case 2 << 2:
        {

            value = get_phys_buff((void*)0xB0000, 320*200);
            return (uint8_t *)value;
        }
        case 3 << 2:
        {
            value = get_phys_buff((void*)0xB8000, 320*200);
            return (uint8_t *)value;
        }
        }
    } else{
        return value;
    }
}

void PutPixel(uint32_t x, uint32_t y, uint8_t r, uint8_t g, uint8_t b)
{

    uint8_t index = GetColorIndex(r, g, b);
    uint8_t *pixelAddress = GetFrameBufferSegment() + (320 * y) + x;
    *pixelAddress = index;
}
void PutPixelColor(uint32_t x, uint32_t y, uint8_t color)
{
    uint8_t *pixelAddress = GetFrameBufferSegment() + (320 * y) + x;
    *pixelAddress = color;
}

void DrawBoxColor(Box box, uint8_t color)
{
    for (size_t x = box.x; x < box.x + box.w; x++)
    {
        for (size_t y = box.y; y < box.y + box.h; y++)
        {
            PutPixelColor(x, y, color);
        }
    }
}

void DrawBoxRGB(Box box, uint8_t r, uint8_t g, uint8_t b)
{
    DrawBoxColor(box, GetColorIndex(r, g, b));
}

typedef struct{
    unsigned char mode;
    void (*crt0_def)(int);
    void (*userset)(int);
} signals;
extern signals lol[10];

void restart_on_error(){
    extern signal_t signal;
    extern void _start();
    for(uint8_t i = 0; i < 10; i++){
        lol[i].mode = 1;
        lol[i].userset = &_start;
    }
}

void unset_on_error(){
    extern signal_t signal;
    // extern void _start();
    for(uint8_t i = 0; i < 10; i++){
        lol[i].mode = 3;
        lol[i].userset = &Unset;
    }
}


void handle_command(size_t command_code){
    switch(command_code){
        case VGA_SET_3h:
        {
            Unset();
        } break;
        
        case VGA_SET_13h:
        {
            SetMode(320, 200, 8);
        } break;
        
    }
}

int main(int argc, char**argv)
{
    int port = VGA_COMMUNICATION_PORT; 
    bind_that_port(VGA_COMMUNICATION_PORT);
    request_io_permission();
    // SetMode(320, 200, 8);
    unset_on_error();
    // volatile int a = 5;
    // a /= 0;
    // Unset();
    // return 0;
    // DrawBoxRGB((Box){ .x=1, .y=1, .w=10,.h=10}, 0xA8, 0xA8, 0xA8);
    init_heap();
    vga_message* buffer  = malloc(sizeof(vga_message));
    char empty = 1;
    size_t len;
    while (1)
    {
        request(port, buffer, sizeof(vga_message));
        switch (buffer->type)
        {
        case D_BOX:{
            char* lel = 0;
            DrawBoxColor(buffer->body.box.box, buffer->body.box.color);
            send_ipc(buffer->port,&empty, 1);
        } break;
        case D_COMMAND:{
            handle_command(buffer->body.code);
            send_ipc(buffer->port,&empty, 1);
        }break;


        default:
            break;
        }
    }
    

    return 15;
}