#include "syscalls.h"
#include "../include/vga_struct.h"
#include "../include/keyboard_struct.h"
#include "../include/malloc.h"
#include "../include/utils.h"

#ifndef NULL
#define NULL (void *)0
#endif

#define TXT_MODE 0
#define GRAPHICS_MODE 1

size_t port;

void sendKeyboardCommand(size_t port_id, size_t command)
{
    IN_KEYBOARD_message *message = malloc(sizeof(IN_KEYBOARD_message));
    *message = (IN_KEYBOARD_message){.type = command, .content = port_id};
    send_ipc(KEYBOARD_COMMUNICATION_PORT, message, sizeof(IN_KEYBOARD_message));
    // Waiting for acknowledgment
    char empty;
    request(port_id, &empty, 1);
    free(message);
}

void SetVga(size_t port, size_t code)
{
    vga_message *message = malloc(sizeof(vga_message));
    *message = (vga_message){.port = port, .body.code = code, .type = D_COMMAND};
    send_ipc(VGA_COMMUNICATION_PORT, message, sizeof(vga_message));

    char empty;
    request(port, &empty, 1);
    free(message);
}

void handle_command(char *buff)
{
    if (buff[0] == 's')
    {
        SetVga(port, VGA_SET_13h);
        if (!fork())
        {
            _execve(1, &buff, &buff);
        }
    }
    if (buff[0] == 'k')
    {
        sendKeyboardCommand(port, KEYBOARD_STOP_RECEIVER);
    }
}

void handle_key(char *buffer, size_t len, pid_t sender)
{
    char key = *buffer;
    static char prev = 0;
    static char IS_MAJ_LOCK = 0;
    static char IS_ALT_LOCK = 0;
    static char buff[7];
    if (key == 58)
        IS_MAJ_LOCK = !IS_MAJ_LOCK;
    else if (key == 56)
        IS_ALT_LOCK = !IS_ALT_LOCK;
    else if (key == 0x1C)
    {
        // print("plouftamer");
        buff[1] = 0;
        print(buff);
        handle_command(buff);
    }
    else
    {
        buff[0] = keyboard_map_azerty[(unsigned int)key];
        if ((prev == 42 || prev == 54) || IS_MAJ_LOCK)
            buff[0] = keyboard_map_azertyUP[(unsigned int)key];
        else if (IS_ALT_LOCK)
            buff[0] = keyboard_map_azertyALT[(unsigned int)key];
        if (1)
        {
            buff[1] = 0;
            print(buff);
        }

        prev = key;
    }
}

int main()
{
    port = bind_port();
    init_heap();
    char *plouf = "pikalul";
    char *myvalue = malloc(50);
    for (size_t i = 0; plouf[i]; i++)
        myvalue[i] = plouf[i];

    // if (!thread())
    // {
    //     print(myvalue);
    //     char *truc = "\nausinetanrist\n";
    //     for (size_t i = 0; truc[i]; i++)
    //         myvalue[i] = truc[i];
    //     return 90;
    // }

    char buff[2] = {myvalue, 0};
    sendKeyboardCommand(port, KEYBOARD_SET_RECEIVER);
    print(myvalue);

    if (!fork())
        while (1);

    char keycode;
    while (1)
    {
        // receive_ipc(port, &keycode, 1, &handle_key);
        request(port, &keycode, 1);
        handle_key(&keycode, 1, 2);
    }

    return 0;
}