#include "syscalls.h"
#include "utils.h"

// FORK: 0 pour le fils
//      PID pour le père

int main(){
    // This port belong to the father
    size_t port = bind_port();
    char buffer[10];
    if(fork()){
        // Code du père
        print("hello i am the father my pid is: ");
        print(dec(getpid(),"   "));
        print("\n");

        size_t received_son_port;
        request(port, &received_son_port,sizeof(size_t));
        send_ipc(received_son_port, "Are ya winning son ?", 21);
    } else {
        // Code du fils
        print("hello i am the son my pid is: ");
        print(dec(getpid(),"   "));
        print("\n");
        // Seulement le fils connait se port
        size_t son_port = bind_port();
        send_ipc(port,&son_port,sizeof(size_t));

        char buffer[19];
        request(son_port, buffer, 21);
        print(buffer);
    }

    return 0;
}