#include "../include/window.h"
#include "../include/utils.h"
#include "../include/malloc.h"
#include "../include/vga_struct.h"
#define NULL 0

#define print(x)

char *dec(unsigned x, char *s)
{
    *--s = 0;
    if (!x)
        *--s = '0';
    for (; x; x /= 10)
        *--s = '0' + x % 10;
    return s;
}

void DrawBoxColor(Box box, uint8_t color)
{
   
    vga_message* buffer = malloc(sizeof(vga_message));
    buffer->type = 1;
    buffer->box = (vga_request_box){.color = color, .box = box};
    send_ipc(0, buffer, sizeof(vga_message));
}

uint8_t GetColorIndex(uint8_t r, uint8_t g, uint8_t b)
{
    {
        if (r == 0x00 && g == 0x00 && b == 0x00)
            return 0x00; // black
        if (r == 0x00 && g == 0x00 && b == 0xA8)
            return 0x01; // blue
        if (r == 0x00 && g == 0xA8 && b == 0x00)
            return 0x02; // green
        if (r == 0xA8 && g == 0x00 && b == 0x00)
            return 0x04; // red
        if (r == 0xFF && g == 0xFF && b == 0xFF)
            return 0x3F; // white
        return 0x00;
    }
}


void handle_message(WM_Request* buffer, size_t written, pid_t sender){
    switch(buffer->code){
        case WM_ADD_WINDOW:{
        }break;
    }
}

int main()
{

    init_heap();
    DrawBoxColor((Box){.x = 0, .y = 0, .w = 320, .h = 50}, GetColorIndex(0x00, 0xA8, 0x00));
    return 1;
}

