#include "../include/malloc.h"
#include "../include/vga_struct.h"
#include "../include/keyboard_struct.h"
#include "syscalls.h"
#include "utils.h"

#ifndef size_t
#define size_t unsigned int
#endif
#ifndef NULL
#define NULL ((void *)0)
#endif

#define SIGTERM 1
#define SIGSEGV 2
#define SIGABRT 3
#define SIGCHILD 4
#define SIGARITH 5
#define SIGPERM 6

static inline uint8_t Port8Bit_read(uint16_t port_number)
{
    uint8_t result;
    __asm__ volatile("inb %1, %0"
                     : "=a"(result)
                     : "Nd"(port_number));
    return result;
}

void sendKeyboardCommand(size_t port, size_t command)
{
    if (fork())
        return;
    IN_KEYBOARD_message *message = malloc(sizeof(IN_KEYBOARD_message));
    *message = (IN_KEYBOARD_message){.type = command, .content = port};
    send_ipc(KEYBOARD_COMMUNICATION_PORT, message, sizeof(IN_KEYBOARD_message));
    // Waiting for acknowledgment
    char empty;
    request(port, &empty, 1);
    free(message);
    __exit(0);
}

void SetVga(size_t port, size_t code)
{
    if (thread())
        return;
    
    vga_message *message = malloc(sizeof(vga_message));
    *message = (vga_message){.port = port, .body.code = code, .type = D_COMMAND};
    send_ipc(VGA_COMMUNICATION_PORT, message, sizeof(vga_message));

    char empty;
    request(port, &empty, 1);
    free(message);
    __exit(0);
}

typedef struct body_part
{
    size_t x;
    size_t y;
    struct body_part *next;
} body_part;

#define QEMU_SPEED 1500
#define HP_SPEED 200000
#define SPEED QEMU_SPEED
#define SNAKE_STEP 5

#define NORTH 0
#define EAST 1
#define SOUTH 2
#define WEST 3

#define MAX_CONSUMMABLE 3

#define VGA_COMMUNICATION_PORT 1

size_t port_bis;

void DrawBoxColor(Box box, uint8_t color)
{
    vga_message *buffer = malloc(sizeof(vga_message));
    buffer->type = D_BOX;
    buffer->body.box = (vga_request_box){.color = color, .box = box};
    buffer->port = port_bis;
    send_ipc(VGA_COMMUNICATION_PORT, buffer, sizeof(vga_message));
    char empty[1];
    request(port_bis, empty, 1);
    free(buffer);
}

body_part *push_part(size_t x, size_t y, body_part *snake)
{
    body_part *new_elemt = malloc(sizeof(body_part));
    new_elemt->next = snake;
    new_elemt->x = x;
    new_elemt->y = y;
    return new_elemt;
}

void append_part(size_t x, size_t y, body_part *part)
{
    if (!part)
        return;
    while (part->next)
    {
        part = part->next;
    }

    part->next = malloc(sizeof(body_part));
    part->next->x = x;
    part->next->y = y;
}

body_part *PopSnake(body_part *snake_head)
{
    body_part *current = snake_head;
    for (current = snake_head; current && current->next && current->next->next;)
        current = current->next;

    if (!(current || current->next))
        return NULL;

    body_part *r = current->next;
    current->next = NULL;
    return r;
}

char direction = NORTH;
char newdirection = NORTH;
void ChangeDirection(char dir)
{
    
    if (dir == 75 && direction != EAST)
        newdirection = WEST;
    else if (dir == 77 && direction != WEST)
         newdirection = EAST;
    else if (dir == 80 && direction != NORTH)
         newdirection = SOUTH;
    else if (dir == 72 && direction != SOUTH)
         newdirection = NORTH;
}

void kallbak(char *buff, size_t written, pid_t sender)
{
    unsigned char status;
    char keycode;

    status = Port8Bit_read(0x64);
    if (status & 0x01)
    {
        keycode = Port8Bit_read(0x60);
        if (keycode < 0)
            return;
        ChangeDirection(keycode);
    }
}

void DrawSnake(body_part *snake_head)
{
    while (snake_head)
    {
        DrawBoxColor((Box){.x = snake_head->x, .y = snake_head->y, .w = 5, .h = 5}, 0x04);
        snake_head = snake_head->next;
    }
}

char Intersect(size_t x1, size_t y1, size_t x2, size_t y2)
{
    // return (x1 == x2 && y1 == y2);
    return (x2 >= x1 && x2 <= x1 + 10) &&
           (y2 >= y1 && y2 <= y1 + 10);
}

char CheckHasEaten(body_part *snake_head, body_part *consummable)
{
    if (!consummable)
        return 0;
    if (!consummable->next)
        return 0;
    char to_ret = 0;
    while (consummable && consummable->next && !to_ret)
    {
        if (Intersect(consummable->next->x, consummable->next->y, snake_head->x, snake_head->y))
        {
            to_ret = 1;
            body_part *temp = consummable->next->next;
            free(consummable->next);
            consummable->next = temp;
        }
        consummable = consummable->next;
    }

    return to_ret;
}

char snake_is_valid(body_part *snake_head)
{
    if (snake_head->x > 320 || snake_head->y > 200)
        return 0;

    body_part *current = snake_head->next;
    while (current && !(current->x == snake_head->x && current->y == snake_head->y))
    {
        current = current->next;
    }

    return current == (void *)0;
}

int main()
{

    init_heap();
    request_io_permission();
    port_bis = bind_port();
   
    char *buff = malloc(5);
    pid_t listener_pid = thread();
    if (!listener_pid)
    {
        uint32_t port = bind_port();
        sendKeyboardCommand(port, KEYBOARD_PAUSE_RECEIVER);
        if (!receive_irqs_on(port, 1))
        {
            print("can't receive ipc on this port");
        }

        while (1)
        {
            // receive_ipc(port, buff, 1, &kallbak);
            request(port, buff, 1);
            kallbak(buff, 5, 10);
        }
    }

    if (Port8Bit_read(0x64) & 0x01)
        Port8Bit_read(0x60);
    size_t seed = 10;
    char buffer[5];
    size_t framecount = 0;
    size_t draw_counter = 0;
    int consummable_count = 0;

    body_part *snake_head = malloc(sizeof(body_part));
    *snake_head = (body_part){.x = 190, .y = 190, .next = NULL};

    body_part *consummable = (malloc(sizeof(body_part)));
    *consummable = (body_part){.x = 20, .y = 20, .next = NULL};

    while (1)
    {
        framecount++;
        if (framecount % SPEED == 0)
        {
            direction = newdirection;
            switch (direction)
            {
            case NORTH:

                snake_head = push_part(snake_head->x, snake_head->y - SNAKE_STEP, snake_head);
                break;
            case SOUTH:
                snake_head = push_part(snake_head->x, (snake_head->y + SNAKE_STEP) % 200, snake_head);
                break;
            case EAST:
                snake_head = push_part((snake_head->x + SNAKE_STEP) % 320, snake_head->y, snake_head);
                break;
            case WEST:
                snake_head = push_part((snake_head->x - SNAKE_STEP <= 0) ? 320 - SNAKE_STEP : snake_head->x - SNAKE_STEP, snake_head->y, snake_head);
                break;
            }
            
            if (!snake_is_valid(snake_head))
            {
                SetVga(port_bis, VGA_SET_3h);
                sendKeyboardCommand(port_bis, KEYBOARD_RESUME_RECEIVER);
                send_signal(SIGTERM, listener_pid);
                char *lel = 0;
                *lel = 5;
                return 68;
            }
            DrawBoxColor((Box){.x = snake_head->x, .y = snake_head->y, .w = 5, .h = 5}, 0x3F);

            if (!(draw_counter % 3) && consummable_count < MAX_CONSUMMABLE)
            {
                append_part(5 * (rand(&seed) % (320 / 5)), 5 * (rand(&seed) % (200 / 5)), consummable);
                consummable_count++;
                draw_counter = 0;
            }

            if (!CheckHasEaten(snake_head, consummable))
            {
                body_part *temp = PopSnake(snake_head);
                if (temp)
                {
                    DrawBoxColor((Box){.x = temp->x, .y = temp->y, .w = 5, .h = 5}, 0);
                    free(temp);
                    draw_counter++;
                }
            }
            else
            {
                consummable_count--;
            }
            DrawSnake(consummable->next);
            framecount = 0;
        }
    }
    return 0;
}