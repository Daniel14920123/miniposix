#include "syscalls.h"
#include "../include/stdarg.h"

#include "utils.h"

#ifndef NULL
#define NULL (void *)0x0
#endif

#define bool char

typedef struct
{
    uint32_t portBase;
    uint32_t interrupt;

    uint16_t bus;
    uint16_t device;
    uint16_t function;

    uint16_t vendor_id;
    uint16_t device_id;

    uint8_t class_id;
    uint8_t subclass_id;
    uint8_t interface_id;

    uint8_t revision;

} PCI_Device_descriptor;

void pciCheckDevice(PCI_Device_descriptor descriptor);

static inline uint8_t Port8Bit_read(uint16_t port_number)
{
    uint8_t result;
    __asm__ volatile("inb %1, %0"
                     : "=a"(result)
                     : "Nd"(port_number));
    return result;
}
static inline uint16_t Port16Bit_read(uint16_t port_number)
{
    uint16_t result;
    __asm__ volatile("inw %1, %0"
                     : "=a"(result)
                     : "Nd"(port_number));

    return result;
}
static inline uint32_t Port32Bit_read(uint16_t port_number)
{
    uint32_t result;
    __asm__ volatile("inl %1, %0"
                     : "=a"(result)
                     : "Nd"(port_number));

    return result;
}

static inline void Port8Bit_write(uint16_t port_number, uint8_t _data)
{
    __asm__ volatile("outb %0, %1"
                     :
                     : "a"(_data), "Nd"(port_number));
}

static inline void Port16Bit_write(uint16_t port_number, uint16_t _data)
{
    __asm__ volatile("outw %0, %1"
                     :
                     : "a"(_data), "Nd"(port_number));
}

static inline void Port32Bit_write(uint16_t port_number, uint32_t _data)
{
    __asm__ volatile("outl %0, %1"
                     :
                     : "a"(_data), "Nd"(port_number));
}
#define PCI_CONFIG_ADDRESS 0xCF8
#define PCI_CONFIG_DATA 0xCFC

enum
{

    PCI_Unclassified = 0x00,
    PCI_MassStorageController = 0x01,
    PCI_NetworkController = 0x02,
    PCI_DisplayController = 0x03,
    PCI_MultimediaController = 0x04,
    PCI_MemoryController = 0x05,
    PCI_BridgeDevice = 0x06,
    PCI_SimpleCommunicationController = 0x07,
    PCI_BaseSystemPeripheral = 0x08,
    PCI_InputDeviceController = 0x09,
    PCI_DockingStation = 0x0A,
    PCI_Processor = 0x0B,
    PCI_Serial_Bus_Controller = 0x0C
};

/**
 * @brief Get the address for a device
 * 
 * @return uint32_t the adress
 */
uint32_t get_address(uint8_t bus_number, uint8_t device_number, uint8_t function_number, uint8_t register_offset)
{
    uint32_t lbus = ((uint32_t)bus_number) << 16;
    uint32_t lslot = ((uint32_t)device_number) << 11;
    uint32_t lfunc = ((uint32_t)function_number) << 8;
    // register_offset need to point consecutive DWORD so his bit 0 and 1 must be 0.
    return lbus | lslot | lfunc | (register_offset & 0xFC) | ((uint32_t)0x80000000);
}

/**
 * @brief Read the pci and return the specified register content. 
 *
 * @return uint32_t register content.
 */
uint32_t pciConfigRead(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset)
{
    uint32_t address = get_address(bus, slot, func, offset);
    Port32Bit_write(PCI_CONFIG_ADDRESS, address);
    uint32_t rep = Port32Bit_read(PCI_CONFIG_DATA);

    // the two offset LSB represent the byte offset off the register. while the other bit represent
    // the start of offset of the register from the adress space
    return rep >> ((offset & 0b11) * 8);
}
/**
 * @brief Read the pci and return the specified register content. 
 *
 * @return uint32_t register content.
 */
uint32_t pciConfigWrite(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset, uint32_t data)
{
    uint32_t address = get_address(bus, slot, func, offset);
    Port32Bit_write(PCI_CONFIG_ADDRESS, address);
    Port32Bit_write(PCI_CONFIG_DATA, data);
}

/**
 * @brief to detect a multifunction device is bit 7 of the header type field. If it is set (value = 0x80)
 * 
 * @return true If device is multifunction
 * @return false If device is not multifunction
 */
bool IsDeviceMultifunction(uint16_t bus, uint16_t device)
{
    return pciConfigRead(bus, device, 0, 0x00E) & (1 << 7);
}

/**
 * @brief Get the descriptor for a device
 * 
 * @param bus The device bus
 * @param device position of the device in the bus
 * @param function Function to get the descriptor of
 */
PCI_Device_descriptor GetDescriptor(uint16_t bus, uint16_t device, uint16_t function)
{

    PCI_Device_descriptor result;
    result.bus = bus;
    result.device = device;
    result.function = function;

    result.vendor_id = (uint16_t)pciConfigRead(bus, device, function, 0x00);
    result.device_id = (uint16_t)pciConfigRead(bus, device, function, 0x02);

    result.class_id = (uint16_t)pciConfigRead(bus, device, function, 0x0b);
    result.subclass_id = (uint16_t)pciConfigRead(bus, device, function, 0x0a);
    result.interface_id = (uint16_t)pciConfigRead(bus, device, function, 0x09);

    result.revision = pciConfigRead(bus, device, function, 0x08);
    result.interrupt = pciConfigRead(bus, device, function, 0x3c);

    return result;
}

/**
 * @brief Bruteforce and stupid loop on all pci buses and devices.
 * 
 */
void SelectDrivers()
{

    for (size_t bus = 0; bus < 256; bus++)
    {
        for (size_t device = 0; device < 32; device++)
        {
            int numFunctions = IsDeviceMultifunction(bus, device) ? 8 : 1;
            for (size_t function = 0; function < numFunctions; function++)
            {
                PCI_Device_descriptor dev = GetDescriptor(bus, device, function);
                pciCheckDevice(dev);
            }
        }
    }
}

void pciCheckUSB(PCI_Device_descriptor descriptor)
{
    if (descriptor.class_id != PCI_Serial_Bus_Controller)
        return;

    switch (descriptor.subclass_id)
    {
    case 0x03:
    {
        printf("USB controller \n");
        switch (descriptor.interface_id)
        {
        case 0x00:{

            printf("UHCI controller\n");
            uint32_t base = pciConfigRead(descriptor.bus, descriptor.device, descriptor.function, 0x20);
            pciConfigWrite(descriptor.bus, descriptor.device, descriptor.function, 0x20, 0xFFFFFFFF);
            uint32_t size = pciConfigRead(descriptor.bus, descriptor.device, descriptor.function, 0x20);
            pciConfigWrite(descriptor.bus, descriptor.device, descriptor.function, 0x20, base);

            if (base & 1)
            {
                printf("IO address is:%x\n", base & ~0x1);
                uint16_t range = (uint16_t)((~(size & ~0x1)));
                printf("IO range is:%x [%x]\n", (base & ~1), (base & ~1) + range);
            }
            else
                printf("Memomry address is:%d\n", base & ~0x1);
        }

            break;
        case 0x10:
            printf("OHCI controller\n");
            break;
        case 0x20:
            printf("EHCI controller\n");
            break;
        case 0x30:
            printf("XHCI controller\n");
            break;
        default:
            break;
        }
    }
    break;

    default:
        break;
    }
}

void pciCheckDevice(PCI_Device_descriptor descriptor)
{

    if (descriptor.vendor_id == 0x0000 || descriptor.vendor_id == 0xffff)
        return;
    char *text;
    switch (descriptor.class_id)
    {
    case PCI_Unclassified:
        text = "detected unclassifed";
        break;
    case PCI_MassStorageController:
        text = "MassStorageController";
        break;
    case PCI_NetworkController:
        text = "detected Network Controller";
        break;
    case PCI_DisplayController:
        text = "detected Display Controller";
        break;
    case PCI_MultimediaController:
        text = "detected Multimedia Controller";
        break;
    case PCI_MemoryController:
        text = "detected Memory Controller";
        break;
    case PCI_BridgeDevice:
        text = "detected Bridge Device";
        break;
    case PCI_SimpleCommunicationController:
        text = "detected Simple communication Controller";
        break;
    case PCI_BaseSystemPeripheral:
        text = "detected Base system peripheral";
        break;
    case PCI_InputDeviceController:
        text = "detected Input Device Controller";
        break;
    case PCI_Serial_Bus_Controller:
        text = "Serial Bus Controller";
        pciCheckUSB(descriptor);
        break;
    default:
        text = "not yet implemented";
        break;
    }
    printf("%s at bus:%d device:%d function:%d\n", text, descriptor.bus, descriptor.device, descriptor.function);
}

int main()
{
    request_io_permission();
    // char buffer[150];
    // printf("%d:%d\n,%s", 158, 25,"coucou");
    //   print(buffer);

    SelectDrivers();
    pid_t ppp = 1; //fork();
    // print("out performed");
    return 0;
}