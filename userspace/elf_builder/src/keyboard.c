#include "syscalls.h"
#include "malloc.h"
#include "utils.h"
#include "keyboard_struct.h"

#define SIGTERM 1
#define SIGSEGV 2
#define SIGABRT 3
#define SIGCHILD 4
#define SIGARITH 5
#define SIGPERM 6

#define request_ipc request

#define uint8_t unsigned char
#define uint16_t unsigned short

static inline uint8_t Port8Bit_read(uint16_t port_number)
{
    uint8_t result;
    __asm__ volatile("inb %1, %0"
                     : "=a"(result)
                     : "Nd"(port_number));
    return result;
}

char isCapture = 1;
void startInput()
{
    isCapture = 1;
}

void stopInput()
{
    isCapture = 0;
}

static void handle_key(char key)
{
    static char prev = 0;
    static char IS_MAJ_LOCK = 0;
    static char IS_ALT_LOCK = 0;
    char buff[7];
    //    if(key == 75)
    //       print((get_current_term()-1) % get_term_number());
    //    else if(key == 77)
    //       print((get_current_term()+1) % get_term_number());
    if (key == 58)
        IS_MAJ_LOCK = !IS_MAJ_LOCK;
    else if (key == 56)
        IS_ALT_LOCK = !IS_ALT_LOCK;
    else
    {
        buff[0] = keyboard_map_azerty[(unsigned int)key];
        if ((prev == 42 || prev == 54) || IS_MAJ_LOCK)
            buff[0] = keyboard_map_azertyUP[(unsigned int)key];
        else if (IS_ALT_LOCK)
            buff[0] = keyboard_map_azertyALT[(unsigned int)key];
        if (1)
        {
            buff[1] = 0;
            print(buff);
        }

        prev = key;
    }
}

void kallbak(char *keycode, size_t receiver_port)
{
    unsigned char status;

    status = Port8Bit_read(0x64);
    if (status & 0x01)
    {
        *keycode = Port8Bit_read(0x60);
        if (*keycode < 0)
            return;

        send_ipc(receiver_port, keycode, 1);
        // handle_key(keycode);
    }
}

void handle_command(IN_KEYBOARD_message *buff, size_t written, pid_t sender)
{

    switch (buff->type)
    {
    case KEYBOARD_SET_RECEIVER:
        //port_bis = buff->content;
        // char empty;
        // send_ipc(port_bis, &empty, 1);
        break;

    default:
        break;
    }
}

size_t start_routine(size_t receiver_port)
{
    size_t child_pid = fork();
    if (!child_pid)
    {
        size_t irq_port = bind_port();
        receive_irqs_on(irq_port, 1);
        if (Port8Bit_read(0x64) & 0x01)
        {
            print("freeing\n");
            Port8Bit_read(0x60);
        }

        char keycode;
        while (1)
        {
            request(irq_port, &keycode, 1);
            kallbak(&keycode, receiver_port);
        }
    }

    return child_pid;
}

typedef struct port_history
{
    size_t port;
    struct port_history *next;
} port_history;

void append_part(size_t port, port_history *list)
{
    if (!list)
    {
        return;
    }
    while (list->next)
    {
        list = list->next;
    }

    list->next = malloc(sizeof(port_history));
    print("append port: ");
    print(dec(port, "    "));
    print("\n");
    list->next->port = port;
    list->next->next = (void *)0;
}

port_history *pop(port_history *list)
{
    if (!list)
    {
        return;
    }
    while (list->next->next)
    {
        list = list->next;
    }

    print("pop port: ");
    char buff[20];
    print(dec(list->next->port,buff+19 ));
    print("\n");
    free(list->next);
    list->next = (void *)0;
    return list;
}

int main()
{
    size_t communication_port = KEYBOARD_COMMUNICATION_PORT;
    bind_that_port(KEYBOARD_COMMUNICATION_PORT);
    init_heap();
    request_io_permission();
    // Set which port should receive the message
    IN_KEYBOARD_message *buffer = malloc(sizeof(IN_KEYBOARD_message));
    port_history *history = malloc(sizeof(port_history));
    char empty;
    size_t irq_port = 0;
    size_t child_pid = 0;
    size_t receiver_port = 0;

    if(!fork()) while(1);

    while (1)
    {

        request(communication_port, buffer, sizeof(IN_KEYBOARD_message));
        switch (buffer->type)
        {
        case KEYBOARD_SET_RECEIVER:
            if (!irq_port)
                irq_port = bind_port();
            if (child_pid)
                send_signal(SIGTERM, child_pid);
            print("set");
            receiver_port = buffer->content;
            append_part(receiver_port, history);
            child_pid = start_routine(receiver_port);
            send_ipc(receiver_port, &empty, 1);
            break;
        case KEYBOARD_STOP_RECEIVER:
        {
            if (child_pid)
            {
                send_signal(SIGTERM, child_pid);
                child_pid = 0;
            }
            pop(history);
            send_ipc(receiver_port, &empty, 1);
        }
        break;
        case KEYBOARD_RESTAURE_RECEIVER:
        {
            if (child_pid)
            {
                send_signal(SIGTERM, child_pid);
                child_pid = 0;
            }
            send_ipc(receiver_port, &empty, 1);
            receiver_port = pop(history)->port;
            print("restauring port: ");
            print(dec(receiver_port, "     "));
            child_pid = start_routine(receiver_port);
        }
        break;
        case KEYBOARD_PAUSE_RECEIVER:
        {
            if (child_pid)
            {
                send_signal(SIGTERM, child_pid);
                child_pid = 0;
            }
            send_ipc(buffer->content, &empty, 1);
        }
        break;
        case KEYBOARD_RESUME_RECEIVER:
        {
            if (!child_pid)
            {
                print("resuming");
                child_pid = start_routine(receiver_port);
            }
            send_ipc(buffer->content, &empty, 1);
        }
        break;
        }
    }

    return 69;
}