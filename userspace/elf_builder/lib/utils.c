#include "utils.h"
#include "syscalls.h"

#ifndef NULL
#define NULL ((void*)0)
#endif

#ifndef uint64_t
#define uint64_t unsigned long long
#endif

#ifndef size_t
#define size_t unsigned int
#endif

#ifndef uint32_t
#define uint32_t unsigned int
#endif

#ifndef size_t
#define size_t unsigned int
#endif

#ifndef uint16_t
#define uint16_t unsigned short
#endif

#ifndef uint8_t
#define uint8_t unsigned char
#endif

char *dec(unsigned x, char *s)
{
    *--s = 0;
    if (!x)
        *--s = '0';
    for (; x; x /= 10)
        *--s = '0' + x % 10;
    return s;
}
char *decX(unsigned x, char *s)
{
    char list[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    *--s = 0;
    if (!x)
        *--s = '0';
    for (; x; x /= 16)
        *--s = list[x % 16];
    return s;
}

void sprintf(char *format, char *buff, va_list args)
{
    // va_list args;
    //    va_start(args, format);

    while (*format != NULL)
    {
        if (*format == '%')
        {
            format++;
            switch (*format)
            {
            case 'd':
            {
                char buffer[30];
                size_t d = va_arg(args, size_t);
                char *newbuff = dec(d, buffer + 29);
                while (*newbuff != NULL)
                {
                    *(buff++) = *(newbuff++);
                }
            }
            break;
            case 'x':
            {
                char buffer[30];
                size_t d = va_arg(args, size_t);
                *(buff++) = '0';
                *(buff++) = 'x';

                char *newbuff = decX(d, buffer + 29);
                while (*newbuff != NULL)
                {
                    *(buff++) = *(newbuff++);
                }
            }
            break;
            case 's':
            {
                char *str = va_arg(args, char *);
                while (*str != NULL)
                {
                    *(buff++) = *(str++);
                }
            }
            break;
            default:
                break;
            }
        }
        else
        {
            *buff = *format;
            buff++;
        }
        format++;
    }
    // va_end(args);
    *buff = 0;
}

void printf(char *format, ...)
{
    char buffer[500];
    va_list args;
    va_start(args, format);

    sprintf(format, buffer, args);
    va_end(args);

    print(buffer);
}

char strcmp(char* s, char* cmp){
    while(*(s) && *(s++) == *(cmp++));
    return !(*(s) == *(cmp) && *(s) == 0);
}