#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv){
    if(argc <= 2) 
        return 1;
    FILE* header = fopen(argv[1], "w");
    //fprintf(header, "#ifndef kPACKED_FILESYSTEM\n#define kPACKED_FILESYSTEM\n\n");
    fprintf(header, "#include \"imported/defs.h\"");
    fprintf(header, "\nconst uint16_t imported_file_n = %d;\n\n", argc - 2);
    //fprintf(header, "typedef struct {\n\tchar* filename;\n\tchar* str;\n\tunsigned long size;\n} Ramdisk_file;\n\n");
    fprintf(header, "Ramdisk_file imported_files[%d] = {\n", argc - 2);
    for(size_t i = 2; i < argc; i++){
        FILE *fp = fopen(argv[i], "rb");
        if(fp) {
            fprintf(header, "\t(Ramdisk_file){\n\t\t.filename = \"%s\",\n\t\t.str = \"", (argv[i] + 7));
            unsigned char buffer[4096];
            unsigned long acc = 0;
            size_t sz = 0;
            while((sz = fread(buffer, 1, sizeof(buffer), fp)) > 0){
                acc += sz;
                for(int i = 0; i < sz; i++)
                    fprintf(header, "\\x%x", buffer[i]);
            }
            fprintf(header, "\",\n\t\t.size = %lu\n\t}", acc);
            if(i < argc - 1)
                fprintf(header, ",\n");
            else 
                fprintf(header, "\n");
        } else 
            return 1;
    }
    fprintf(header, "};\n");
    //fprintf(header, "#endif");
    return 0;
}