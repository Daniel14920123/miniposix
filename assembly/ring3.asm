global jump_usermode
global jump_usermode_global
extern test_user_function

jump_usermode:
    nop
    push eax
    mov eax,[esp+8]
    mov [jump_addr],eax
    pop eax
    mov ax, (4*8)|3 ; ring 3 data with bottom 2 bits set for ring 3
	mov ds, ax
	mov es, ax 
	mov fs, ax 
	mov gs, ax ; SS is handled by iret
	; set up the stack frame iret expects
	mov eax, esp
	push (4*8)|3 ; data selector
	push eax ; current esp
	pushf ; eflags
	push (3*8)|3 ; code selector (ring 3 code with bottom 2 bits set for ring 3)
    mov eax,[jump_addr]
	push eax ; instruction address to return to
	iret ;sans commentaire...

jump_usermode_global:
    nop
	;add esp, 64
    ;pop eax
    mov ax, (4*8)|3 ; ring 3 data with bottom 2 bits set for ring 3
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax ; SS is handled by iret
	; set up the stack frame iret expects
	mov eax, [temp_esp]
	push (4*8)|3 ; data selector
	push eax ; current esp
	pushf ; eflags
	or DWORD [esp],1<<9
	push (3*8)|3 ; code selector (ring 3 code with bottom 2 bits set for ring 3)
    mov eax,[jump_addr]
	push eax ; instruction address to return to
	sti
	iret ;sans commentaire...

[GLOBAL tss_flush]    ; Allows our C code to call tss_flush().
tss_flush:
   mov ax, 0x2B      ; Load the index of our TSS structure - The index is
                     ; 0x28, as it is the 5th selector and each is 8 bytes
                     ; long, but we set the bottom two bits (making 0x2B)
                     ; so that it has an RPL of 3, not zero.
   ltr ax            ; Load 0x2B into the task state register.
   ret 

;global set_esp
;set_esp:
;	mov esp, [esp+4]
;1	ret