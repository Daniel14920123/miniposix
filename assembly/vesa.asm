global vbe_info
vbe_info:
    .signature:
        db "VESA"
    .data:
        resb 508

global fill_vbe_info
fill_vbe_info:
    sti
    push es
    push ax
    push di     
    mov ax, 0x4F00  
    mov di, vbe_info
    cli
    int 0x10
    sti
    cmp ax, 0x4F
	jne .error
    pop di
    pop ax
    pop es
    ret

.error:
    cli
    call k_main