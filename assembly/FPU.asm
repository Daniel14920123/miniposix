extern has_fpu
extern no_fpu
extern has_sse
extern no_sse
global test_fpu
global init_sse

test_fpu:
    mov edx, cr0                            ; Start probe, get CR0
    and edx, 0xFFFFFFF3                     ; clear EM and TS to force fpu access
    mov cr0, edx                            ; store control word
    fninit                                  ; load defaults to FPU
    fnstsw [.testword]                      ; store status word
    cmp word [.testword], 0                 ; compare the written status with the expected FPU state
    jne .nofpu                              ; jump if the FPU hasn't written anything (i.e. it's not there)
    ; has fpu
    mov edx, cr0                            ; Start probe, get CR0
    and edx, 0xFFFFFFDB                     ; clear EM and NE to force fpu access
    or  edx, 0x2                            ; set MP
    mov cr0, edx                            ; store control word

    fldcw [.allmask]                       ; writes 0x37a, both division by zero and invalid operands cause exceptions.
    call has_fpu
    ret
.nofpu:
    call no_fpu
    ret
.value_37A:	dw 0x037a
.allmask:	dw 0x037F
.value_3FA:	dw 0x03fa
.testword: DW 0x55AA                    ; store garbage to be able to detect a change

init_sse:
    ; NEED TO set SSE handleler
    
    mov eax, 0x1                        ; Check if SSE is support
    cpuid
    test edx, 1<<25
    jz .noSSE                           ; Can't do SSE
    ; SSE is available
    test edx, 1<<24                     ; Check if FXSR is here
    jz .noSSE
    ; support SSE
    mov eax, cr0
    and eax, 0xFFFB		;clear coprocessor emulation CR0.EM
    or eax, 0x40002		;set coprocessor monitoring  CR0.MP AND OSXSAVE CR0.OSXSAVE
    mov cr0, eax
    mov eax, cr4
    or ax, 3 << 9		;set CR4.OSFXSR and CR4.OSXMMEXCPT at the same time
    ;os support for fxsave and fxrstor instruction
    ;os support for unmasked simd floating point exceptions
    mov cr4, eax
    
    call has_sse
    ret
.noSSE:
    call no_sse
    ret