bits 32

global jump_addr
global temp_esp
global kernel_esp

section .bss
end_of_stack:
resb 8192
stack_space:

jump_addr:
    resb 4

temp_esp:
    resb 4

kernel_esp:
    resb 4

section .multiboot
        ;multiboot spec
        align 4
        dd 0x1BADB002              ;magic
        dd 0x03                    ;flags
        dd - (0x1BADB002 + 0x03)   ;checksum. m+f+c should be zero

section .text
global start

extern k_main

start:
	cli 	
	mov esp, stack_space
    push ebx
    xor ebp, ebp
	call k_main
	hlt

%include 'assembly/gdtandidt.asm'
%include 'assembly/FPU.asm'
%include 'assembly/ring3.asm'
;%include 'assembly/manual_probing.asm'
;%include 'assembly/vesa.asm'
;%include 'assembly/apaging.asm'

; forever:
;	hlt 				;halt the CPU
;	jmp forever

;global end_of_kernel
;end_of_kernel:
