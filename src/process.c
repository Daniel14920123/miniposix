#include "process/process.h"
#include "paging/paging.h"
#include "memorymanager/memorymanager.h"
#include "convert.h"
#include "usermode/usermode.h"
#include "macros/macros.h"

size_t new_pid(){
	static size_t next_pid = 0;
    return next_pid++;
}

process new_process(pid_t parent, ptime_t time){
	process_break_info pbi;
	pbi.page_directory = (uint32_t*)set_up_directory();
	pbi.pde = 10;
	pbi.next_pte = 0;
	pbi.allocated_in_current_frame = 0;
	pbi.program_break = (uint32_t)craft_addr(10, 0, 0);

	process proc;
	proc.pbi = pbi;
	proc.parent = parent;
	proc.time = time;
	proc.next = 0;
	proc.pid = new_pid();
	proc.has_been_launched = false;
    proc.is_paused = false;
    
    proc.signal_queue = 0;
    proc.signal_handler = 0;
    proc.message_sent = 0;
    proc.should_exit = false;

    memset(&(proc.regs), 0, sizeof(registers_t));

	return proc;
}

void enableProcessFirst(process* to_enable, registers_t* regs){
    signal_t sig;
    to_enable->regs.cs = (3*8) | 3;
    to_enable->regs.ds = (4*8) | 3;
    to_enable->regs.ss = (4*8) | 3;
    //to_enable->regs. = (4*8) | 3;
    to_enable->regs.eflags |= (1<<9); 
    to_enable->regs.useresp = to_enable->regs.esp;
    *regs = to_enable->regs;
	process temp = *to_enable;
    if(temp.signal_queue)
        sig = temp.signal_queue->sigcode;
	disablePaging();
    loadPageDirectory(temp.pbi.page_directory);
	jump_addr = (void*)temp.regs.eip;
	enablePaging();
    if(temp.signal_queue)
        *temp.sigswap = sig;
	//extern uint32_t temp_esp;
	//temp_esp = temp.regs.esp;
	//jump_usermode_global();
}

void enableProcess(process* to_enable, registers_t* regs){
	process temp = *to_enable;
	registers_t regsk = *regs;
    if(to_enable->signal_queue) {
        signal_t* swap = to_enable->sigswap;
        signal_t sig = to_enable->signal_queue->sigcode;
        loadPageDirectory(temp.pbi.page_directory);
        *swap = sig;
    } else{
        loadPageDirectory(temp.pbi.page_directory);
    }
	extern uint32_t temp_esp;
	return;
}

process_break_info fork_pbi(process_break_info info){
    disablePaging();
    process_break_info to_return;
    to_return = info;
    to_return.page_directory = (uint32_t*)set_up_directory();
    for(size_t i = 1; i < 1023; i++){
        //if(!(info.page_directory[i]%2)){
            to_return.page_directory[i] = duplicate_frame(info.page_directory[i]);
        //}
    }
    enablePaging();
    return to_return;
}

process_break_info map_pbi(process_break_info info){
    disablePaging();
    process_break_info to_return;
    to_return = info;
    to_return.page_directory = (uint32_t*)set_up_directory();
    for(size_t i = 1022; i > 0; i--){
        //if(!(info.page_directory[i]%2)){
            to_return.page_directory[i] = map_pte(info.page_directory[i]);
        //}
    }
    // to_return.page_directory[1023] = (uint32_t)to_return.page_directory
    enablePaging();
    return to_return;

    // parcourir 1022 (ni premier ni dernier) entrée de page directory (en reverse order)
    // premier: kernel / dernier début
    // tant que le bit (9) est set allouer une nouvelel page et recopier le contenu de la page table


}




process fork(process* proc){
    process to_return;
	to_return.pbi = fork_pbi(proc->pbi);
	to_return.parent = proc->pid;
	to_return.time = proc->time;
	to_return.next = 0;
	to_return.pid = new_pid();
	to_return.has_been_launched = true;
    to_return.regs = proc->regs;

	return to_return;
}
process thread(process* proc){
    process to_return = *proc;
	to_return.pbi = map_pbi(proc->pbi);
	// to_return.parent = proc->pid;
	// to_return.time = proc->time;
	to_return.next = 0;
	to_return.pid = new_pid();
	// to_return.has_been_launched = true;
    // to_return.regs = proc->regs;

	return to_return;
}


void stopProcess(process* pid){
    // Retirer du scheduler
    // Envoyer un message au parent.
    // Éventuellement plus tard (beaucoup plus tard) indiquer un pid libre et libérer les pages quand on aura un vrai pmm
}

/**
 * @brief Adds page in Process Break Info
 * @warning should rewrite the algorithm to add page in the middle,\
 *  or at least check if page is free
 * @param pbi The process break info
 * @return true Page added successfully
 * @return false Aborted.
 */
static bool add_page_in_pbi(process_break_info* pbi, int flags, bool force){
    uint32_t* page_table = 0;

    if((force && !((uint32_t)pbi->page_directory[pbi->pde] & 0x1)) || pbi->next_pte == 1025 || (!pbi->next_pte && pbi->pde == 10)){
        if(pbi->next_pte == 1025) 
            pbi->pde++;
        if(!force)
            pbi->next_pte = 0;

        uint32_t* table_to_push = (uint32_t*)get_new_page();
        // for(uint32_t i = 0; i < 1024; i++)
        //     table_to_push[i] = 0;
        
        pbi->page_directory[pbi->pde] = (uint32_t)table_to_push | PG_USERLAND | PG_WRITABLE | PG_PRESENT;
        page_table = table_to_push;
    } else{
        page_table = UNPACK_ADDR(pbi->page_directory, pbi->pde);
    }

    if(!force || (force && !((uint32_t)page_table[pbi->next_pte] & 0x1)))
        page_table[pbi->next_pte] = (uint32_t)get_new_page() | flags | PG_PRESENT;
    else if(force && ((uint32_t)page_table[pbi->next_pte] & 0x1))
        page_table[pbi->next_pte] = (page_table[pbi->next_pte] & ~((uint32_t)0xFFF)) | flags | PG_PRESENT;
    pbi->next_pte++;

    return true;
}

//Will bug if we start with negative increment
void* sbrk(process_break_info* proc, int32_t to_add){
    disablePaging();
    void* old_limit = (void*)proc->program_break;
    size_t new_break = proc->program_break + to_add; //same
    int32_t sav_to_add = to_add;
    if(!proc->allocated_in_current_frame)
        add_page_in_pbi(proc, PG_WRITABLE | PG_USERLAND, false);
    while((int32_t)(0x1000 - proc->allocated_in_current_frame) < to_add && to_add > 0){
        add_page_in_pbi(proc, PG_WRITABLE | PG_USERLAND, false);
        to_add-=0x1000;
    }
    //TODO : add frame freeing with to_add < 0
    proc->program_break = (size_t)old_limit + sav_to_add;
    proc->allocated_in_current_frame = proc->program_break % 4096;
    // if (proc->allocated_in_current_frame == 0)
        // add_page_in_pbi(proc, PG_WRITABLE | PG_USERLAND, false);
    enablePaging();
    return old_limit;
}

void* add_space(process_break_info* proc, void* hintaddr, size_t length, int flags){
    disablePaging();
    process_break_info temp_info = (process_break_info){
        .allocated_in_current_frame = (uint32_t)hintaddr & 0xFFF,
        .next_pte = (uint16_t)(((uint32_t)hintaddr & 0x3FF000) >> 12)
    };

    //Had to do this in order to keep intellisense away from madness.
    //100 0000 0000 0000 0000 0000
    temp_info.pde = (uint16_t)(((uint32_t)hintaddr) >> 22);
    temp_info.program_break = (uint32_t)hintaddr;
    temp_info.page_directory = proc->page_directory;
    
    length += temp_info.allocated_in_current_frame;

    for(size_t i = 0; i < (length/4096)+1; i++)
        add_page_in_pbi(&temp_info, flags, true);

    enablePaging(); 
    return hintaddr;
}

void* set_then_apply(process_break_info* proc, void* addr, char* mem, size_t len, int flags){
    process_break_info temp  = *proc;
    process_break_info temp2 = *proc;
    char* chunk = (char*)add_space(&temp, addr, len, PG_WRITABLE);
    for(size_t i = 0; i < len; i++)
        chunk[i] = mem[i];
    return add_space(&temp2, chunk, len, flags);
}