#include "setup/setup.h"
#include "pit/pit.h"

void setup_heap(){
	frame_init(0);
	init_kheap();
	use_kernel_mapping();
	enablePaging();
	init_heap();
}

void setup_services(){
	k_clear_screen();
	init_descriptor_tables();
	attach_errors();
	// pit_set_mode(2);
	// set_pit_count(0xF0F0);
	// timer_phase(500);
	loadKDrivers();
	setup_scheduler();
	//initrd = initrd_init();
	/*node* lol = new_file_in(initrd);
	kstrcpy("lolilol.txt", lol->filename);
	test((path(initrd->fd, "") == initrd->fd), "pikalul bomboozled");
	test((path(initrd->fd, "/") == initrd->fd), "pikalul bomboozled");
	test((path(initrd->fd, "/votai .test") == initrd->link_sub->fd), "pikalul bomboozled");*/
}

void init_terms(){
	//multiprint(0, ">>> ");
	multiflush();
}