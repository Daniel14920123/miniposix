#include "textmodeshell/textmodeshell.h"
#include "tmterm/tmterm.h"
#include "kstring.h"

void print_prompt(char* username){
    multiprint(get_current_term(), username);
    multiprint(get_current_term(), "# ");
}

void handle_command(char* command){
    if(kstrequal(command, "lol"))
        multiprint(get_current_term(), "lolilol");
}

void input(){
    char buffer[255];
}