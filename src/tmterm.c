#include "tmterm/tmterm.h"

static uint8_t term_number = 1;

char multiterms[MAX_TERM_NUMBER][VGA_COLUMN * VGA_ROW * 2];
int pos[MAX_TERM_NUMBER];
uint8_t current_term = 0;

__attribute__((unused))
static void k_clear_screen(uint8_t num)
{
	char *vidmem = multiterms[num];
	unsigned int i = 0;
	while (i < (80 * 25 * 2))
	{
		vidmem[i] = ' ';
		i++;
		vidmem[i] = WHITE_TXT;
		i++;
	};
};

__attribute__((unused))
static unsigned int k_printf(uint8_t num, char *message, unsigned int line, int *n)
{
	char *vidmem = multiterms[num];
	unsigned int i = 0;

	i = (line * 80 * 2);

	while (*message != 0)
	{
		if (*message == '\n') // check for a new line
		{
			line++;
			i = (line * 80 * 2);
			message++;
			(*n)++;
		}
		else
		{
			vidmem[i] = *message;
			message++;
			i++;
			vidmem[i] = WHITE_TXT;
			i++;
		};
	};

	return (1);
}
#if 0
static unsigned int scroll_printf(uint8_t num, char *message)
{
	static int count = 0;
	// count++;
	if (count == VGA_ROW)
	{
		char *vidmem = multiterms[num];
		for (int i = 0; i < 2 * VGA_ROW * (VGA_COLUMN - 1); i += 2)
		{
			vidmem[i] = vidmem[i + VGA_COLUMN * 2];
		}
		k_printf(num, message, VGA_ROW - 1, &count);
		count--;
	}
	else
	{
		k_printf(num, message, count, &count);
	}

	count += kstrlen(message) / VGA_COLUMN;
	return 0;
}
#endif
static unsigned int print_message(uint8_t num, char *message, int16_t* pos)
{
	char *vidmem = multiterms[num];
	
	while (*message != 0)
	{
		if (*message == '\n') // check for a new line
		{
			//(*pos) = ((*pos)%VGA_COLUMN + 1) * VGA_ROW;
			(*pos) += (VGA_COLUMN*2)-((*pos)%(VGA_COLUMN*2)) ;
			message++;
		} else if (*message == '\r') // check for a new line
		{
			//(*pos) = ((*pos)%VGA_COLUMN + 1) * VGA_ROW;
			(*pos) += (VGA_COLUMN*2)-((*pos)%(VGA_COLUMN*2)) ;
			message++;
		} else if(*message == '\b'){
			(*pos)--;
			vidmem[--(*pos)] = ' ';
			message++;
            /*char* pos2 = (char*)pos;
            //(*pos2)--;
            vidmem[--(*pos2)] = ' ';
            *message++;
            pos = (uint16_t*)pos2;*/
        } else if(*message == '\t'){
			
			(*pos)+=4;
			message++;
		} else{
			vidmem[*pos] = *message;
			message++;
			(*pos)++;
			vidmem[*pos] = WHITE_TXT;
			(*pos)++;
		};
	};

	return (1);
}


static void kprintf(uint8_t num, char *message)
{
	char *vidmem = multiterms[num];

	if (pos[num] >= (VGA_ROW * VGA_COLUMN*2))
	{
		for (int i = 0; i < 2 * VGA_ROW * (VGA_COLUMN - 1); i ++)
		{
			vidmem[i] = vidmem[i + VGA_COLUMN * 2];
		}
		pos[num] -= (VGA_COLUMN*2);
		print_message(num, message, (int16_t*)&pos[num]);
	} else {
		print_message(num, message, (int16_t*)&pos[num]);
	}
}

#if 0
static void printfHex(uint8_t num, uint16_t nombre)
{
	char *foo = "00";
	char *hex = "0123456789ABCDEF";
	foo[0] = hex[(nombre >> 4) & 0xF];
	foo[1] = hex[(nombre & 0xF)];
	kprintf(num, foo);
}
#endif

void multiprint(uint8_t term_num, char* message){
    kprintf(term_num, message);
}

void multiswitch(uint8_t num){
    char* vidmem = (char *)0xb8000;
    for(int i = VGA_COLUMN * VGA_ROW * 2 - 1; i >= 0; i--){
        vidmem[i] = multiterms[num][i];
    }
    current_term = num;
}

uint8_t get_current_term(){
    return current_term;
}

int8_t add_term(){
    if(term_number >= MAX_TERM_NUMBER)
		return 0;
    term_number++;
    for(int i = VGA_COLUMN * VGA_ROW - 1; i >= 0; i--){
        multiterms[term_number-1][i] = 0;
    }
	return 1;
}

void remove_term(uint8_t index){
    if(term_number == 1 || index >= term_number)
        return;
    term_number--;
    for(int i = index; i < term_number; i++){
        for(int y = VGA_COLUMN * VGA_ROW - 1; y >= 0; y--){
            multiterms[i][y] = multiterms[i+1][y];
        }
    }
}

uint8_t get_term_number(){
	return term_number;
}