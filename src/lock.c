#include "helpers/lock.h"
#include "macros/macros.h"

static unsigned int lock_counter = 0;

void lock(){
    DISABLE_INTERRUPTS();
    lock_counter++;
}

void unlock(){
    if(!lock)
        return;
    lock_counter--;
    if(!lock_counter)
        ENABLE_INTERRUPTS();
}