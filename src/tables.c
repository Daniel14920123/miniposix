#include "casm/inlineassembly.h"
#include "textmode.h"
#include "tables/tables.h"

gdt_entry_t gdt_entries[6];
gdt_ptr_t gdt_ptr;
idt_entry_t idt_entries[256];
idt_ptr_t idt_ptr;

static void init_gdt();
static void init_idt();

struct tss_entry_struct {
	uint32_t prev_tss; // The previous TSS - with hardware task switching these form a kind of backward linked list.
	uint32_t esp0;     // The stack pointer to load when changing to kernel mode.
	uint32_t ss0;      // The stack segment to load when changing to kernel mode.
	// Everything below here is unused.
	uint32_t esp1; // esp and ss 1 and 2 would be used when switching to rings 1 or 2.
	uint32_t ss1;
	uint32_t esp2;
	uint32_t ss2;
	uint32_t cr3;
	uint32_t eip;
	uint32_t eflags;
	uint32_t eax;
	uint32_t ecx;
	uint32_t edx;
	uint32_t ebx;
	uint32_t esp;
	uint32_t ebp;
	uint32_t esi;
	uint32_t edi;
	uint32_t es;
	uint32_t cs;
	uint32_t ss;
	uint32_t ds;
	uint32_t fs;
	uint32_t gs;
	uint32_t ldt;
	uint16_t trap;
	uint16_t iomap_base;
} __attribute__((packed));
 
typedef struct tss_entry_struct tss_entry_t;

tss_entry_t tss_entry;

/**
 * @brief Set gdt gate mode.
 * 
 */
static void gdt_set_gate(int32_t, uint32_t, uint32_t,uint8_t, uint8_t);

/**
 * @brief Set idt gate mode.
 * 
 */
void idt_set_gate(uint8_t, uint32_t, uint16_t,uint8_t);

void init_descriptor_tables(){
	init_idt();
	init_gdt();
}

void write_tss(gdt_entry_t *e);

static void init_gdt(){
	gdt_ptr.limit = (sizeof(gdt_entry_t)* 6)-1;
	gdt_ptr.base = (uint32_t)&gdt_entries;

	gdt_set_gate(0,0,0,0,0); // Null segment
	gdt_set_gate(1,0,0xFFFFFFFF,0x9A,0xCF); // Code Segment
	gdt_set_gate(2,0,0xFFFFFFFF,0x92,0xCF); // Data Segment
	gdt_set_gate(3,0,0xFFFFFFFF,0xFA,0xCF); // User Code Segment
	gdt_set_gate(4,0,0xFFFFFFFF,0xF2,0xCF); // User Code Segment
	write_tss(gdt_entries + 5);

	gdt_flush((uint32_t)&gdt_ptr);
	extern void tss_flush();
	tss_flush();
}

static void gdt_set_gate(int32_t num, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran){
	gdt_entries[num].base_low = (base & 0xFFFF);
	gdt_entries[num].base_middle = (base >> 16)& 0xFF;	
	gdt_entries[num].base_high = (base >> 24)& 0xFF;

	gdt_entries[num].limit_low = (limit & 0xFFFF);
	gdt_entries[num].granularity = (limit & 0xFFFF);


	gdt_entries[num].granularity |= (gran & 0xF0);
	gdt_entries[num].access = access;
}

void write_tss(gdt_entry_t *e) {

	typedef struct {
		unsigned int limit_low: 16;
		unsigned int base_low: 24;
		unsigned int accessed: 1;
		unsigned int read_write: 1; // readable for code, writable for data
		unsigned int conforming_expand_down: 1; // conforming for code, expand down for data
		unsigned int code: 1; // 1 for code, 0 for data
		unsigned int code_data_segment: 1; // should be 1 for everything but TSS and LDT
		unsigned int DPL: 2; // privilege level
		unsigned int present: 1;
		unsigned int limit_high: 4;
		unsigned int available: 1; // only used in software; has no effect on hardware
		unsigned int long_mode: 1;
		unsigned int big: 1; // 32-bit opcodes for code, uint32_t stack for data
		unsigned int gran: 1; // 1 to use 4k page addressing, 0 for byte addressing
		unsigned int base_high: 8;
	} __attribute__((packed)) gdt_entry_bits;

	gdt_entry_bits* g = (gdt_entry_bits*)((void*)e);

	// Compute the base and limit of the TSS for use in the GDT entry.
	uint32_t base = (uint32_t) &tss_entry;
	uint32_t limit = sizeof(tss_entry);
 
	// Add a TSS descriptor to the GDT.
	g->limit_low = limit;
	g->base_low = base;
	g->accessed = 1; // With a system entry (`code_data_segment` = 0), 1 indicates TSS and 0 indicates LDT
	g->read_write = 0; // For a TSS, indicates busy (1) or not busy (0).
	g->conforming_expand_down = 0; // always 0 for TSS
	g->code = 1; // For a TSS, 1 indicates 32-bit (1) or 16-bit (0).
	g->code_data_segment=0; // indicates TSS/LDT (see also `accessed`)
	g->DPL = 3; // user mode
	g->present = 1;
	g->limit_high = (limit & (0xf << 16)) >> 16; // isolate top nibble
	g->available = 0; // 0 for a TSS
	g->long_mode = 0;
	g->big = 0; // should leave zero according to manuals.
	g->gran = 0; // limit is in bytes, not pages
	g->base_high = (base & (0xff << 24)) >> 24; //isolate top byte
 
	// Ensure the TSS is initially zero'd.
	memset(&tss_entry, 0, sizeof(tss_entry));
 
	tss_entry.ss0 = 0x10;  // Set the kernel stack segment.

	//tss_entry.esp0 = REPLACE_KERNEL_STACK_ADDRESS; // Set the kernel stack pointer.
	asm volatile("mov %%esp,%0" :"=r"(tss_entry.esp0):);
	//note that CS is loaded from the IDT entry and should be the regular kernel code segment
}


#define INIT_POSIX_IDT() idt_set_gate(128, (uint32_t)isr128, 0x08, 0x8E);

static void init_idt(){


	idt_ptr.limit = (sizeof(idt_entry_t)*256)-1;
	idt_ptr.base = (uint32_t)&idt_entries;
	memset((uint8_t *)&idt_entries, 0, sizeof(idt_entry_t)*256);


	Port8Bit_write(0x20, 0x11);
	Port8Bit_write(0xA0, 0x11);
	Port8Bit_write(0x21, 0x20);
	Port8Bit_write(0xA1, 0x28);
	Port8Bit_write(0x21, 0x04);
	Port8Bit_write(0xA1, 0x02);
	Port8Bit_write(0x21, 0x01);
	Port8Bit_write(0xA1, 0x01);
	Port8Bit_write(0x21, 0x0);
	Port8Bit_write(0xA1, 0x0);

	idt_set_gate( 0,  (uint32_t)isr0, 0x08, 0x8E);
	idt_set_gate( 1,  (uint32_t)isr1, 0x08, 0x8E);
	idt_set_gate( 2,  (uint32_t)isr2, 0x08, 0x8E);
	idt_set_gate( 3,  (uint32_t)isr3, 0x08, 0x8E);
	idt_set_gate( 4,  (uint32_t)isr4, 0x08, 0x8E);
	idt_set_gate( 5,  (uint32_t)isr5, 0x08, 0x8E);
	idt_set_gate( 6,  (uint32_t)isr6, 0x08, 0x8E);
	idt_set_gate( 7,  (uint32_t)isr7, 0x08, 0x8E);
	idt_set_gate( 8,  (uint32_t)isr8, 0x08, 0x8E);
	idt_set_gate( 9,  (uint32_t)isr9, 0x08, 0x8E);
	idt_set_gate(10, (uint32_t)isr10, 0x08, 0x8E);
	idt_set_gate(11, (uint32_t)isr11, 0x08, 0x8E);
	idt_set_gate(12, (uint32_t)isr12, 0x08, 0x8E);
	idt_set_gate(13, (uint32_t)isr13, 0x08, 0x8E);
	idt_set_gate(14, (uint32_t)isr14, 0x08, 0x8E);
	idt_set_gate(15, (uint32_t)isr15, 0x08, 0x8E);
	idt_set_gate(16, (uint32_t)isr16, 0x08, 0x8E);
	idt_set_gate(17, (uint32_t)isr17, 0x08, 0x8E);
	idt_set_gate(18, (uint32_t)isr18, 0x08, 0x8E);
	idt_set_gate(19, (uint32_t)isr19, 0x08, 0x8E);
	idt_set_gate(20, (uint32_t)isr20, 0x08, 0x8E);
	idt_set_gate(21, (uint32_t)isr21, 0x08, 0x8E);
	idt_set_gate(22, (uint32_t)isr22, 0x08, 0x8E);
	idt_set_gate(23, (uint32_t)isr23, 0x08, 0x8E);
	idt_set_gate(24, (uint32_t)isr24, 0x08, 0x8E);
	idt_set_gate(25, (uint32_t)isr25, 0x08, 0x8E);
	idt_set_gate(26, (uint32_t)isr26, 0x08, 0x8E);
	idt_set_gate(27, (uint32_t)isr27, 0x08, 0x8E);
	idt_set_gate(28, (uint32_t)isr28, 0x08, 0x8E);
	idt_set_gate(29, (uint32_t)isr29, 0x08, 0x8E);
	idt_set_gate(30, (uint32_t)isr30, 0x08, 0x8E);
	idt_set_gate(31, (uint32_t)isr31, 0x08, 0x8E);
	idt_set_gate(32,  (uint32_t)irq0, 0x08, 0x8E);
	idt_set_gate(33,  (uint32_t)irq1, 0x08, 0x8E);
	idt_set_gate(34,  (uint32_t)irq2, 0x08, 0x8E);
	idt_set_gate(35,  (uint32_t)irq3, 0x08, 0x8E);
	idt_set_gate(36,  (uint32_t)irq4, 0x08, 0x8E);
	idt_set_gate(37,  (uint32_t)irq5, 0x08, 0x8E);
	idt_set_gate(38,  (uint32_t)irq6, 0x08, 0x8E);
	idt_set_gate(39,  (uint32_t)irq7, 0x08, 0x8E);
	idt_set_gate(40,  (uint32_t)irq8, 0x08, 0x8E);
	idt_set_gate(41,  (uint32_t)irq9, 0x08, 0x8E);
	idt_set_gate(42, (uint32_t)irq10, 0x08, 0x8E);
	idt_set_gate(43, (uint32_t)irq11, 0x08, 0x8E);
	idt_set_gate(44, (uint32_t)irq12, 0x08, 0x8E);
	idt_set_gate(45, (uint32_t)irq13, 0x08, 0x8E);
	idt_set_gate(46, (uint32_t)irq14, 0x08, 0x8E);
	idt_set_gate(47, (uint32_t)irq15, 0x08, 0x8E);

	INIT_POSIX_IDT()

	idt_flush((uint32_t)&idt_ptr);
}



void idt_set_gate(uint8_t n, uint32_t base, uint16_t sel, uint8_t flags)
{

	idt_entries[n].ie_base_lo = base & 0xFFFF;
	idt_entries[n].ie_base_hi = (base >> 16) & 0xFFFF;

	idt_entries[n].ie_sel     = sel;
	idt_entries[n].ie_always0 = 0;
	/* We must uncomment the OR below when we get to using user-mode. It
		sets the interrupt gate's privilege level to 3. */
	idt_entries[n].ie_flags   = flags  | 0x60 ;
}

isr_t interrupt_handlers[256];

void register_interrupt_handler(uint8_t n, isr_t handler)
{
	interrupt_handlers[n] = handler;
} 

void irq_handler(volatile registers_t regs)
{
	// Send an EOI (end of interrupt) signal to the PICs.
	// If this interrupt involved the slave.
	if(regs.int_no == 33){
		volatile int a = 5;
		a += 2;
	}
	if (regs.int_no >= 40)
	{
		// Send reset signal to slave.
		Port8Bit_write(0xA0, 0x20);
	}
	// Send reset signal to master. (As well as slave, if necessary).
	Port8Bit_write(0x20, 0x20);

	if (interrupt_handlers[regs.int_no] != 0)
	{
		isr_t handler = interrupt_handlers[regs.int_no];
		handler(&regs);
	}
} 

void isr_handler(volatile registers_t regs)
{
	if (interrupt_handlers[regs.int_no] != 0){
		isr_t handler = interrupt_handlers[regs.int_no];
		handler(&regs);
	}
}

GateDescriptor interuptDescriptorTable[256];
void SetInterruptDescriptorTableEntry(
		uint8_t interruptNumber,
		uint16_t codeSegmentSelectorOffset,
		void (*handler)(),
		uint8_t DescriptorPrivilege,
		uint8_t DescriptorType
		){
	interuptDescriptorTable[interruptNumber].handlerAddressLowBits = ((uint32_t)handler)&0xFFFF;
	interuptDescriptorTable[interruptNumber].handlerAddressHighBits = ((uint32_t)handler >> 16)&0xFFFF;
	interuptDescriptorTable[interruptNumber].gdt_codeSegmentSelector = codeSegmentSelectorOffset;
	interuptDescriptorTable[interruptNumber].access = IDT_DESC_PRESENT|DescriptorPrivilege| (DescriptorType&3 << 5);
	interuptDescriptorTable[interruptNumber].reserved = 0;
}


