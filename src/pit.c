#include "casm/inlineassembly.h"
#include "std.h"
#include "pit/pit.h"
#define PIT_COMMAND_PORT 0x43

size_t reload_value;
#define FREQUENCY (1193182 / reload_value)

typedef struct
{
    int32_t channel : 2;
    int32_t access_mode : 2;
    int32_t op_mode : 3;
    bool BCD : 1;
} __attribute__((packed)) pit_message;

typedef union
{
    pit_message msg;
    uint8_t as_unsigned;
} pit_wrapper;

void pit_send_message(int32_t channel, int32_t access_mode, int32_t op_mode, bool BCD)
{
    uint8_t truc = ((channel & 0b11) << 6) | ((access_mode & 0b11) << 4) | ((op_mode & 0b111) << 1) | (BCD & 0b1);
    Port8Bit_write(0x43, truc);
}

void pit_set_mode(int32_t mode)
{
    pit_send_message(0, 0b11, mode, 0);
}

// 00 11 010 0

uint16_t read_value()
{
    uint16_t count;

    Port8Bit_write(PIT_COMMAND_PORT, 0b0000000);

    count = Port8Bit_read(0x40); // Low byte
    count |= Port8Bit_read(0x40) << 8;
}

void set_pit_count(unsigned count)
{
   // count = count / 1193182;
    // Set low byte
    Port8Bit_write(0x40, count & 0xFF);          // Low byte
    Port8Bit_write(0x40, (count & 0xFF00) >> 8); // High byte

    reload_value = count;
}

size_t ms_to_tick(size_t delay)
{
    return delay * FREQUENCY;
}

void timer_phase(int hz)
{
    int divisor = 1193180 / hz;       /* Calculate our divisor */
    Port8Bit_write(0x43, 0x36);             /* Set our command byte 0x36 */
    Port8Bit_write(0x40, divisor & 0xFF);   /* Set low byte of divisor */
    Port8Bit_write(0x40, divisor >> 8);     /* Set high byte of divisor */
}