#include "convert.h"
#include "std.h"

int digit_count(int number){
    int i = 0;
    do
    {
        number /= 10;
        i++;

    } while (number != 0);
    return i;
}

int abs_i(int number){
    return number < 0 ? -1*number : number;
}

void itoa(int number, char ret[]){

    int nb_digit = digit_count(number);
    size_t i = 0;
    char is_negative = number < 0;
    if(is_negative) i = 1;
    number = abs_i(number);
    if(is_negative)
        ret[0] = '-';
    do
    {
        if(!is_negative)
            ret[nb_digit - i - 1] = (char)((number % 10) + (char)'0');
        else
            ret[nb_digit - i + 1] = (char)((number % 10) + (char)'0');
        number /= 10;
        i++;

    } while (number != 0);
    //char y = i - i + 1;
    for(; i < 7 ; i++){
        ret[i] = (char)0x0;
    }

}

void sttoa(uint32_t number, char ret[]){
    int nb_digit = digit_count(number);
    size_t i = 0;
    do
    {
        ret[nb_digit - i - 1] = (char)((number % 10) + (char)'0');
        number /= 10;
        i++;

    } while (number != 0);
    //char y = i - i + 1;
    for(; i < 11 ; i++){
        ret[i] = (char)0x0;
    }
}

int32_t atoi(char* str){
    int32_t i = 0;
    int32_t r = 0;
    while(str[i]){
        r = r * 10 + (str[i] - '0');
        i++;
    }
    return r;
}
