#include "setup/setup.h"
#include "tests/tests.h"
#include "thirdparty/thirdparty.h"

void k_main(multiboot_info_t* mbd) {
	ON_FIRST_RUN(
		setup_services();
	)

	
	setup_heap();

	launch_tests();

	init_terms();

	ENABLE_INTERRUPTS();
	WAIT_INTERRUPTS();
}
