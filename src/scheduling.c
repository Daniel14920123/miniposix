#include "process/scheduling.h"
#include "paging/paging.h"
#include "syscalls/syscalls.h"
#include "tables/isr.h"
#include "memory.h"
#include "process/process.h"
#include "helpers/kmalloc.h"
#include "macros/macros.h"

#define false 0
#define true 1

process *pqueue;
process *current_proc;
static int32_t tick = 100;

size_t number_of_task_switchs = 0;

static bool scheduler_enabled = false;

bool are_tasks_scheduled(){
    return (bool)pqueue;
}

process copy_current_proc(){
    use_kernel_mapping();
    process to_return = *current_proc;
    //loadPageDirectory(current_proc);
    return to_return;
}

void setup_scheduler(){
    pqueue = 0;
    current_proc = 0;
    tick = 100;
    scheduler_enabled = false;
}

void Init_scheduler(process *first_process)
{
    pqueue = first_process;
    current_proc = first_process;
    tick = PROCESS_TICK;
    scheduler_enabled = true;
}

void Add_process_to_schedule(process *process_to_add)
{
    DISABLE_INTERRUPTS();
    static bool first_call = true;
    if(first_call || !pqueue){
        Init_scheduler(process_to_add);
        first_call = false;
        //ENABLE_INTERRUPTS();
        return;
    }
    process *queue = pqueue;
    while (queue->next != 0)
    {
        queue = queue->next;
    }
    queue->next = process_to_add;
}

void remove_current_proc(){
    //process* tmp = current_proc;
    Remove_process_to_schedule(current_proc);
    //my_free(current_proc);
}

// NEED TO FREE PROCESS AFTER CALLING THIS FUCNTION
void Remove_process_to_schedule(process *process_to_remove)
{
    use_kernel_mapping();
    if (pqueue->pid == process_to_remove->pid)
    {
        if (pqueue->next == 0){
            current_proc = current_proc->next;
            pqueue = (void*)0;
            return;
        }
        pqueue = process_to_remove->next;
        return;
    }
    process *old = pqueue;
    process *queue = pqueue->next;
    while (queue->pid != process_to_remove->pid)
    {
        old = queue;
        queue = queue->next;
    }
    if (queue->pid == process_to_remove->pid)
    {
        old->next = queue->next;
    }
}


void switch_task(registers_t *regs)
{
    // q_print("lel");
    if(!pqueue) return;
    number_of_task_switchs++;
    // extern uint32_t temp_esp;
    // regs->esp = temp_esp;
    static bool first_time = true;
    //current_proc->regs.useresp = regs->esp;

    if(current_proc->has_been_launched){
        memcpy(&(current_proc->regs), regs, sizeof(registers_t));
    }


    do{
        if(current_proc->should_exit && !current_proc->message_sent){
            process* next = (current_proc->next ? current_proc->next : pqueue);
            remove_current_proc();
            current_proc = next;
            continue;
        }
        if (current_proc->next != 0 && !first_time)
            current_proc = current_proc->next;
        else
            current_proc = pqueue;
    } while(current_proc->is_paused || (current_proc->should_exit));
    
    
    /*if(!current_proc->has_been_launched){
        current_proc->regs.ds = (4*8)|3;
        current_proc->regs.cs = (4*8)|3;
        current_proc->regs.useresp = current_proc->regs.esp;
        current_proc->regs.eax = 0;
        current_proc->regs.ebx = 0;
        current_proc->regs.ecx = 0;
        current_proc->regs.edx = 0;
    }*/
    
    first_time = false;
    
    if(current_proc->has_been_launched){
        //regs->ds = 4*8|3;
        
        /*regs->esp = current_proc->regs.esp;
        regs->eip = current_proc->regs.eip;
        regs->eax = current_proc->regs.eax;
        regs->ebx = current_proc->regs.ebx;
        regs->ecx = current_proc->regs.ecx;
        regs->edx = current_proc->regs.edx;*/
        // current_proc->regs.eflags;
        uint32_t eflags = regs->eflags;
        memcpy(regs, &(current_proc->regs), sizeof(registers_t));
        // regs->eflags = eflags;
        // regs->ds = (4*8)|3;
        // regs->cs = (3*8)|3;
        // regs->ss = (4*8)|3;
        // regs->eflags = 0;
    }
    
    if(!current_proc->has_been_launched){
        current_proc->has_been_launched = true;
        enableProcessFirst(current_proc, regs);
    } else{
        //extern uint32_t temp_esp;
        uint32_t pesp = current_proc->regs.esp - sizeof(registers_t);
        enableProcess(current_proc, regs);

    }

    return;
    //memcpy(regs, &current_proc->regs, sizeof(registers_t));
    //loadPageDirectory(current_proc->pbi.page_directory);
}

// Call by IRQ0
void schedule_tick(registers_t *regs)
{
    if(scheduler_enabled && pqueue){
    DISABLE_INTERRUPTS();
        tick = (tick - 1)%PROCESS_TICK;
        if (tick == 0)
        {
            use_kernel_mapping();
            switch_task(regs);

        }
    //ENABLE_INTERRUPTS();
    }
}