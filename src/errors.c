#include "errors/errors.h"
#include "std.h"
#include "macros/macros.h"
#include "tmterm/tmterm.h"
#include "paging/paging.h"
#include "process/process.h"
#include "helpers/kmalloc.h"
#include "defines/defines.h"

#define ABRT_CUR_PROC(code) \
    {extern process* current_proc;\
    if(current_proc){\
        signal_the_process(regs, code);\
        return;\
    }}

void signal_the_process(registers_t* regs, signal_t sig){
    extern process* current_proc;
        process* receiver = current_proc;
        use_kernel_mapping();
        // while(receiver && receiver->pid != sig.proc)
        //     receiver = receiver->next;
        if(receiver){
            signal* to_append = receiver->signal_queue;
            while(to_append && to_append->next)
                to_append = to_append->next;
            if(!to_append){
                receiver->signal_queue = my_malloc(sizeof(signal));
                to_append = receiver->signal_queue;
            }
            else{
                to_append->next = my_malloc(sizeof(signal));
                to_append = to_append->next;
            }
            to_append->sigcode = sig;
            
            //hack the registers of the receiver
            if(receiver->signal_handler){
                receiver->regs_vault_in_case_of_signal = *regs;
                receiver->regs = *regs;
                receiver->regs.eip = receiver->signal_handler;
                receiver->is_paused = false;
                *regs = receiver->regs;
            }
        }
        //loadPageDirectory(current_proc->pbi.page_directory);
        switch_task(regs);
}

static void double_fault_handler(registers_t* regs __attribute__((unused))){
    disablePaging();
    PANIC("FATAL : Double Fault occured");
}

static void div_by_zero_handler(registers_t* regs __attribute__((unused))){
#ifndef PANIC_MODE
    ABRT_CUR_PROC(SIGARITH);
#endif
    PANIC("FATAL : Division by 0 occured");
}

static void page_fault_handler(registers_t* regs){
    //disablePaging();
    uint16_t code = regs->err_code;

#ifndef PANIC_MODE
    if(code | 4){
        signal_the_process(regs, SIGSEGV);
        return;
    }
#endif

    if((code % 2)){
        q_print("Page fault caused by page protection violation :\n");
    } else{
        q_print("Page fault caused by non present page :\n");
    }
    
    code >>= 1;
    if((code % 2)){
        q_print("Page fault caused by write access :\n");
    } else{
        q_print("Page fault caused by read access :\n");
    }
    
    code >>= 1;
    if((code % 2))
        q_print("Page Fault in userspace :\n");
    
    code >>= 1;
    if((code % 2))
        q_print("Error on reserved write :\n");

    code >>= 1;
    if((code % 2))
        q_print("Error on instruction Fetch :\n");

    // extern void use_kernel_mapping();
    // use_kernel_mapping();

    PANIC("FATAL : page fault");
}

static void bound_error_handler(registers_t* regs){
    disablePaging();
    uint32_t faulty_instruction = regs->err_code;
    q_print("Instruction at ");
    q_print_size_t(faulty_instruction);
    q_print(" triggered bound error.");
    PANIC("FATAL : bound range exceeded");
}

static void invalid_opcode_handler(registers_t* regs __attribute__((unused))){
    PANIC("FATAL : invalid opcode");
}

static void coprocessor_handler(registers_t* regs __attribute__((unused))){
    PANIC("Fatal : coprocessor segment overrun");
}

static void device_not_available_handler(registers_t* regs __attribute__((unused))){
    PANIC("Fatal : device not available");
}

static void x87_floating_point_handler(registers_t* regs __attribute__((unused))){
    PANIC("Fatal : x87_floating point exception");
}

static void simd_floating_point_handler(registers_t* regs __attribute__((unused))){
    PANIC("Fatal : SIMD floating point execption");
}

static void virtualization(registers_t* regs __attribute__((unused))){
    PANIC("Fatal : Virtualization exception");
}

static void debug(registers_t* regs){
    PANIC("Fatal : debug");
}

static void alignment_check_handler(registers_t* regs){
    PANIC("Fatal : error alignment");
}

static void gpf_handler(registers_t* regs){
#ifdef PANIC_MODE
    PANIC("Fatal : General protection Fault.");
#else
    ABRT_CUR_PROC(SIGPERM);
#endif
}

static void invalid_tss_handler(registers_t* regs){
    disablePaging();
    q_print("Segment at ");
    q_print_size_t(regs->err_code);
    q_print(" caused invalid tss.\n");
    PANIC("FATAL : invalid tss.");
}

void attach_errors(void){
    register_interrupt_handler(0x0, &div_by_zero_handler);
    register_interrupt_handler(0x5, &bound_error_handler);
    register_interrupt_handler(0x6, &invalid_opcode_handler);
    register_interrupt_handler(0x7, &device_not_available_handler);
    register_interrupt_handler(0x8, &double_fault_handler);
    register_interrupt_handler(0x9, &coprocessor_handler);
    register_interrupt_handler(0xD, &gpf_handler);
    register_interrupt_handler(0xE, &page_fault_handler);
    register_interrupt_handler(0x10, &x87_floating_point_handler);
    register_interrupt_handler(0x13, &simd_floating_point_handler);
    register_interrupt_handler(0x14, &virtualization);
}