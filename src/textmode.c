#include "textmode.h"

void k_clear_screen()
{
	char *vidmem = (char *)0xb8000;
	unsigned int i = 0;
	while (i < (80 * 25 * 2))
	{
		vidmem[i] = ' ';
		i++;
		vidmem[i] = WHITE_TXT;
		i++;
	};
};

unsigned int k_printf(char *message, unsigned int line, int *n)
{
	char *vidmem = (char *)0xb8000;
	unsigned int i = 0;

	i = (line * 80 * 2);

	while (*message != 0)
	{
		if (*message == '\n') // check for a new line
		{
			line++;
			i = (line * 80 * 2);
			message++;
			(*n)++;
		}
		else
		{
			vidmem[i] = *message;
			message++;
			i++;
			vidmem[i] = WHITE_TXT;
			i++;
		};
	};

	return (1);
}

unsigned int scroll_printf(char *message)
{
	static int count = 0;
	// count++;
	if (count == VGA_ROW)
	{
		char *vidmem = (char *)0xb8000;
		for (int i = 0; i < 2 * VGA_ROW * (VGA_COLUMN - 1); i += 2)
		{
			vidmem[i] = vidmem[i + VGA_COLUMN * 2];
		}
		k_printf(message, VGA_ROW - 1, &count);
		count--;
	}
	else
	{
		k_printf(message, count, &count);
	}

	count += kstrlen(message) / VGA_COLUMN;
	return 0;
}




unsigned int print_message(char *message, size_t* pos)
{
	char *vidmem = (char *)0xb8000;

	while (*message != 0)
	{
		if (*message == '\n') // check for a new line
		{
			(*pos) += (VGA_COLUMN*2)-((*pos)%(VGA_COLUMN*2));
			message++;
		}
		else
		{
			vidmem[*pos] = *message;
			message++;
			(*pos)++;
			vidmem[*pos] = WHITE_TXT;
			(*pos)++;
		};
	};

	return (1);
}


void kprintf(char *message)
{
	static size_t pos = 0;
	char *vidmem = (char *)0xb8000;

	if (pos >= (VGA_ROW * VGA_COLUMN*2))
	{
		for (size_t i = 0; i < 2 * VGA_ROW * (VGA_COLUMN - 1); i += 2)
		{
			vidmem[i] = vidmem[i + VGA_COLUMN * 2];
		}
		pos -= (VGA_COLUMN*2);
		print_message(message,&pos);
	} else {
		print_message(message,&pos);
	}
}

void printfHex(uint16_t nombre)
{
	char *foo = "000";
	char *hex = "0123456789ABCDEF";
	foo[0] = hex[(nombre >> 4) & 0xF];
	foo[1] = hex[(nombre & 0xF)];
	kprintf(foo);
}