#include "kdrivers/kdrivers.h"
#include "textmode.h"
#include "convert.h"
#include "tables/tables.h"
#include "tmterm/tmterm.h"
#include "metaprog/yield.h"
#include "kstring.h"
#include "macros/macros.h"
#include "syscalls/syscalls.h"
#include "process/scheduling.h"
#include "IPC/IPC.h"
#include "helpers/kmalloc.h"

#define DEFAULTINPUTSIZE 1024
#define DELIMITERLIMIT 15

//static uint32_t tick = 0;
static bool isCapture = true;
static bool isDisplay = true;
char basicInputBuff_b[DEFAULTINPUTSIZE];
char returnInputBuff_b[DEFAULTINPUTSIZE];
char* basicInputBuff = basicInputBuff_b;
char* returnInputBuff = returnInputBuff_b;
static size_t num_of_writes = 0;
char del[DELIMITERLIMIT] = {"\n"};
#if 0

char* getInput(){
   for(size_t i = 0; i < DEFAULTINPUTSIZE && basicInputBuff[i]; i++)
      returnInputBuff[i] = basicInputBuff[i];
   for(size_t i = 0; i < DEFAULTINPUTSIZE && basicInputBuff[i]; i++)
      basicInputBuff[i] = 0;
   return returnInputBuff;
}


void Inputn(char* buffer, size_t n, char* delimiters){
   startInput();
   kstrcpy(delimiters, del);
   num_of_writes = 0;
}
#endif

void startInput(){
   isCapture = true;
}

void stopInput(){
   isCapture = false;
}

static void handle_key(char key){
   static char prev = 0;
   static char IS_MAJ_LOCK = 0;
   static char IS_ALT_LOCK = 0;
   char buff[7];
   if(key == 75)
      multiswitch((get_current_term()-1) % get_term_number());
   else if(key == 77)
      multiswitch((get_current_term()+1) % get_term_number());
   else if(key == 58)
      IS_MAJ_LOCK = !IS_MAJ_LOCK;
   else if(key == 56)
      IS_ALT_LOCK = !IS_ALT_LOCK;
   else{
      buff[0] = keyboard_map_azerty[(unsigned int)key];
      if((prev == 42 || prev == 54) || IS_MAJ_LOCK)
         buff[0] = keyboard_map_azertyUP[(unsigned int)key];
      else if(IS_ALT_LOCK)
         buff[0] = keyboard_map_azertyALT[(unsigned int)key];
      if(isDisplay){
         buff[1] = 0;
         multiprint(get_current_term(), buff);
         multiswitch(get_current_term());
      }
      if(isCapture){
         bool is_delimiter = false;
         for(size_t i = 0; i < DELIMITERLIMIT && del[i]; i++)
            if(del[i] == buff[0])
               is_delimiter = true;
         if(is_delimiter){
            stopInput();
            for(size_t i = 0; i < DEFAULTINPUTSIZE /*&& basicInputBuff[i]*/; i++)
               returnInputBuff[i] = basicInputBuff[i];
            //returnInputBuff[num_of_writes] = 0;
            ___SYSCALL(1, HANDLE_KRISBOOL_INPUT, returnInputBuff);
            q_print(">>> ");
            num_of_writes = 0;
            for(size_t i = 0; i < DEFAULTINPUTSIZE /*&& basicInputBuff[i]*/; i++)
               basicInputBuff[i] = 0;
            startInput();
         } else if(buff[0] == '\b')
            num_of_writes--;
         else if(buff[0])
            basicInputBuff[num_of_writes++] = buff[0];
      }
      prev = key;
   }
}

#define MAX_TIMER_FUNC 32
static void(*functions_timer[MAX_TIMER_FUNC])(registers_t*) = {0};
static void timer_callback(registers_t* regs){
   // registers_t registers;
   // memcpy(&registers, regs, sizeof(registers_t));
   for(uint8_t i = 0; i < MAX_TIMER_FUNC; i++){
      if(functions_timer[i])
         functions_timer[i](regs);
   }
   // ENABLE_INTERRUPTS();
}

bool register_timer_func(void(*function_timer)(registers_t*)){
   uint8_t i = 0;
   while(functions_timer[i] && i < MAX_TIMER_FUNC)
      i++;
   if(i >= MAX_TIMER_FUNC)
      return false;
   functions_timer[i] = function_timer;
   return true;
}

void init_timer(uint32_t frequency){
   uint32_t divisor = 1193182 / frequency;
   Port8Bit_write(0x43, 0x36);
   Port8Bit_write(0x40, (uint8_t)(divisor & 0xFF));
   Port8Bit_write(0x40, (uint8_t)( (divisor >> 8) & 0xFF ));
} 

static void keyboard_call_back(registers_t* reg __attribute__((unused)))
{
   unsigned char status;
   char keycode;

   status = Port8Bit_read(0x64);
   if (status & 0x01)
   {
      keycode = Port8Bit_read(0x60);
      if (keycode < 0)
         return;

      handle_key(keycode);
   }
}

uint32_t attached_to_irq[16];

void irq_common_callback(registers_t* regs){
   if(!attached_to_irq[regs->int_no - IRQ0])
      return;
   size_t old_cr3 = 0;
   asm volatile ("mov %%cr3, %0":"=r"(old_cr3):);
   use_kernel_mapping();
   // message msg = {.signal = 0, .data_size = sizeof(int32_t), .data = my_malloc(sizeof(uint32_t))};
   int32_t empty;
   message msg = {.signal = 0, .data_size = sizeof(int32_t), .data = &empty};
   // ((uint32_t*)msg.data)[0] = regs->int_no;
   send(attached_to_irq[regs->int_no - IRQ0], get_k_task(), msg);
   extern process* current_proc;
   current_proc->regs = *regs;
   process* target = GetPortWithId(attached_to_irq[regs->int_no - IRQ0])->linked;
   if(target->is_paused != attached_to_irq[regs->int_no - IRQ0]){
      asm volatile("mov %0, %%cr3"::"r"(old_cr3));
      return;
   }
   *regs = target->regs;
   enableProcess(target, regs);
}

void loadKDrivers(void){
   register_interrupt_handler(0x80, &syscall_callback);

   register_timer_func(&schedule_tick);
   register_interrupt_handler(IRQ0, &timer_callback);

   for (int i = 1; i < 16; i++)
      attached_to_irq[i] = 0;
   
   attached_to_irq[1] = 0;
   attached_to_irq[2] = 0;
   attached_to_irq[3] = 0;
   attached_to_irq[4] = 0;
   attached_to_irq[5] = 0;
   attached_to_irq[6] = 0;
   attached_to_irq[7] = 0;
   attached_to_irq[8] = 0;
   attached_to_irq[9] = 0;
   attached_to_irq[10] = 0;
   attached_to_irq[11] = 0;
   attached_to_irq[12] = 0;
   attached_to_irq[13] = 0;
   attached_to_irq[14] = 0;
   attached_to_irq[15] = 0;

   register_interrupt_handler(IRQ1,  &irq_common_callback);
   register_interrupt_handler(IRQ2,  &irq_common_callback);
   register_interrupt_handler(IRQ3,  &irq_common_callback);
   register_interrupt_handler(IRQ4,  &irq_common_callback);
   register_interrupt_handler(IRQ5,  &irq_common_callback);
   register_interrupt_handler(IRQ6,  &irq_common_callback);
   register_interrupt_handler(IRQ7,  &irq_common_callback);
   register_interrupt_handler(IRQ8,  &irq_common_callback);
   register_interrupt_handler(IRQ9,  &irq_common_callback);
   register_interrupt_handler(IRQ10, &irq_common_callback);
   register_interrupt_handler(IRQ11, &irq_common_callback);
   register_interrupt_handler(IRQ12, &irq_common_callback);
   register_interrupt_handler(IRQ13, &irq_common_callback);
   register_interrupt_handler(IRQ14, &irq_common_callback);
   register_interrupt_handler(IRQ15, &irq_common_callback);

   /*typedef struct {
      char isr;
      void(*callback)(registers_t* regs);
      bool success;
   } driver;

   driver to_push = (driver){
      IRQ1,
      &keyboard_call_back,
      false
   };*/
   // register_interrupt_handler(IRQ1, &keyboard_call_back);

   // ___SYSCALL(1, ADD_DRIVER_LEGACY, &to_push);
   // to_push = (driver){IRQ0, &timer_callback, false};
   // ___SYSCALL(1, ADD_DRIVER_LEGACY, &to_push);


}

// process* process_attached_to_isr[15];

// void attach_current_process_to_interrupt(size_t interrupt_number){
//    extern process* current_proc;
//    process_attached_to_isr[interrupt_number] = current_proc;
// }

// switch_task_to_process(registers_t* regs){

// }

// void enable_process_based_on_interrupt(registers_t* regs){
//    switch_task_to_process(process_attached_to_isr[regs->int_no]);
// }

