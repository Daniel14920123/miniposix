#include "IPC/IPC.h"
#include "helpers/kmalloc.h"
#include "paging/paging.h"

#define HOW_MANY_PORT_CAN_HAVE_A_DEFINED_PORT_ID 30

port *port_list = NULL;

/**
 * @brief Set ipc. Must be called
 * 
 */
void setup_ipc()
{
    port_list = my_malloc(sizeof(port));
    *port_list = (port){
        .linked = NULL,
        .port_name = 30,
        .next = NULL,
        .msg_queue = NULL,
        .white_list = NULL,
        .white_list_size = 0};
}

size_t bind_a_port(process* proc){
    if(port_list == NULL){
        setup_ipc();
        port_list->linked = proc;
        return port_list->port_name;
    }

    port *ports = port_list;
    while (ports->next != NULL)
        ports = ports->next;
    
    port *newproc = my_malloc(sizeof(port));
    //extern process* current_proc;
    newproc->linked = proc;
    if(ports->port_name <= 30)
        newproc->port_name = 31;
    else
        newproc->port_name = ports->port_name + 1;

    newproc->white_list_size = 0;
    newproc->white_list = NULL;
    newproc->msg_queue = NULL;
    newproc->next = NULL;

    ports->next = newproc;
    return newproc->port_name;
}
bool bind_that_port(process* proc, size_t port_id){
    if(port_list == NULL){

        port_list = my_malloc(sizeof(port));
        *port_list = (port){.linked =proc,
        .port_name = port_id,
        .next = NULL,
        .msg_queue = NULL,
        .white_list = NULL,
        .white_list_size = 0};

        return true;
    }

    port *ports = port_list;
    while (ports->next != NULL && ports->next->port_name < port_id)
        ports = ports->next;
    
    // va y avoir un bug car si jamais c le premiertruc ou je sais pas si jaais port_pid < à port_pid du premier

    if(ports->port_name == port_id){
        // ou alors jarter le port de ces morts.
        return false;
    }

    port *newproc = my_malloc(sizeof(port));
    newproc->linked = proc;
    newproc->port_name = port_id;
    newproc->white_list_size = 0;
    newproc->white_list = NULL;
    newproc->msg_queue = NULL;
    newproc->next = NULL;

    ports->next = newproc;
    return true;
}


// synchronous
void request(size_t port)
{
}

/**
 * @brief Search a port id in a list.
 * @param list The in which the port will be searched.
 * @param pid The pid of the port
 * @return port* NULL if the port isn't found otherwise return the port.
 */
port *GetPortWithId(size_t pid)
{

    port *curr = port_list;
    while (curr != NULL && curr->port_name != pid)
        curr = curr->next;

    return curr;
}
/**
 * @brief Check if the sender pid belong to the receiver white_list
 * @param receiver The port which will receive the message
 * @param sender_pid The pid of the sender
 * @return true The sender is in the white_list
 * @return false 
 */
bool IsPortAccepted(port *receiver, size_t sender_pid)
{
    if (receiver->white_list == NULL)
        return true;
    size_t *current_element = receiver->white_list;
    for (size_t i = 0; i < receiver->white_list_size; i++)    
        if (receiver->white_list[i] == sender_pid)
            return true;

    return false;
}
typedef union{
    struct {
        unsigned int pdindex : 10;
        unsigned int ptindex : 10;
        unsigned int offset : 12;
    } vaddr_t;
    void* as_ptr;
} vaddr;

/*static void* get_physical(void* vaddr, uint32_t* page_directory){
    unsigned long pdindex = (unsigned long)vaddr >> 22;
    unsigned long ptindex = (unsigned long)vaddr >> 12 & 0x03FF;
    uint32_t* page_table = page_directory[pdindex] & ~0xFFF;
    uint32_t* frame = page_table[ptindex] & ~0xFFF;
    return (void*)((char*)frame + ((uint32_t)vaddr & 0xFFF));
}*/

/**
 * @brief Receives a message asynchronously
 * @param port_pid The pid of the port which will receive the message
 * @param addr virtual address in the receiver page, Where the message should be sent.
 * @param len The lenght of addr buffer.
 * @param sender the pid of the sender proc.
 * @return size_t the number of byte sent.
*/
size_t receive(size_t port_pid, void* addr, size_t len, pid_t* sender){
    port *receiver = GetPortWithId(port_pid);
    
    if (!receiver || receiver->msg_queue == NULL)
        return 0;

    // RECUP MESSAGE
    mailbox proc_mail = *(receiver->msg_queue);
    *sender = proc_mail.sender->pid;
    proc_mail.sender->message_sent--;

    uint32_t sender_pd   = (uint32_t)(proc_mail.sender->pbi.page_directory);
    uint32_t receiver_pd = (uint32_t)(receiver->linked->pbi.page_directory);
    
    disablePaging();

    char* sender_physical = get_physical(proc_mail.data.data, (uint32_t*)sender_pd);
    char* receiver_physical = get_physical(addr, (uint32_t*)receiver_pd);

    for(size_t i = 0; i < len && i < proc_mail.data.data_size; i++)
        receiver_physical[i] = sender_physical[i]; 
    enablePaging();
    
    // POP
    size_t written = (((int32_t)proc_mail.data.data_size - (int32_t)len ) <= 0 ? (size_t)proc_mail.data.data_size : ((size_t)proc_mail.data.data_size - (size_t)len ));
    mailbox * temp = receiver->msg_queue->next;
    my_free(receiver->msg_queue);
    receiver->msg_queue = temp;
    return written;
}

/*
 * @brief 
 * 
 * @param port_pid The port which will receive the message
 * @param sender_proc The process of the sender
 * @param msg The message to add
 * @return true The sender is in the white_list
 */
void send(size_t port_pid, process *sender_proc, message msg){

    port* receiver_port = GetPortWithId(port_pid);
    if (receiver_port == NULL)
        return;


    // Test is not whitelisted
    if (!IsPortAccepted(receiver_port, sender_proc->pid))
        return;

    sender_proc->message_sent++;

    if(receiver_port->linked->is_paused && receiver_port->linked->is_paused == port_pid)
        receiver_port->linked->is_paused = 0;

        
    // message *malloc_msg = my_malloc(sizeof(message));
    // malloc_msg->data = msg.data;
    // malloc_msg->signal = msg.signal;
    // malloc_msg->data_size = msg.data_size;

    mailbox *proc_mail = receiver_port->msg_queue;

    // Create a new mail
    mailbox *mail = my_malloc(sizeof(mailbox));
    mail->data = msg;
    mail->sender = sender_proc;
    mail->next = NULL;

    // If mailbox empty
    if (proc_mail == NULL){
        receiver_port->msg_queue = mail;
        return;
    }

    // Search the last mail
    while (proc_mail->next != NULL)
        proc_mail = proc_mail->next;

    // Add it
    proc_mail->next = mail;
}
