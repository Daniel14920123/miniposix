#include "memorymanager/mapping.h"
#include "memorymanager/memorymanager.h"
#include "casm/inlineassembly.h"
#include "macros/macros.h"
#include "paging/paging.h"
#include "memory.h"

#define FRAME_NUMBER (4096)
#define PAGE_SIZE 
extern char end_of_kernel;

static uint32_t number_of_frames;

static bool frames[FRAME_NUMBER] = {true};
static uint32_t addresses[FRAME_NUMBER] = {0};

//static uint32_t x[MAX_PAGE_DIRECTORY_NUM][1024] __attribute__((aligned(4096))) = {0};

static void get_true_frame_number(size_t limit){
	for(uint32_t i = 1; i < limit+1; i++){
		uint32_t* frame = (uint32_t*)get_new_page();
		set_frame(frame, i);
		for(uint32_t y = 0; y < 1024; y++){
			if((uint32_t)(frame[y]) != i){
				number_of_frames = i-1;
				return;
			}
		}
	}
	number_of_frames = limit;
	return;
}

void frame_init(size_t limit){
	if(limit)
		get_true_frame_number(limit);
	else
		number_of_frames = FRAME_NUMBER;
	for(uint32_t i = 0; i < number_of_frames; i++){
		addresses[i] = i * 4096 + (uint32_t)BEGINNING_ADDR();
		frames[i] = true;
	}
}

/*
memory_map new_memory_map(){
	memory_map to_return;
	for(uint32_t i = 0; i < 4096; i++){
		to_return.directory[i] = (uint32_t)(to_return.tables + i) * 4096 | 3;
		for(uint32_t y = 0; y < 4096; i++){
			to_return.tables[i][y] = 2;
		}
	}
	NEW_TABLE(lel, 3);
	ASSIGN_PRESENT_ADDR(to_return.directory, 0, lel);
	to_return.directory[1023] = (uint32_t)to_return.directory * 4096 | 3;
	return to_return;
}
*/

void new_empty_directory(uint32_t* to_set){
	static size_t last = 0;
	for(unsigned int to_returni = 0; to_returni < 1024; to_returni ++){ 
		to_set[to_returni] = 2; 
	} 
	to_set[1023] = (uint32_t)to_set | 3;
	NEW_TABLE(lel, 3);
	ASSIGN_PRESENT_ADDR(to_set, 0, lel);
	last++;
}

void* get_new_page(){
	disablePaging();
	size_t y = 0;
	char* to_return = 0;
	for(size_t i = BEGINNING_ADDR(); i < END_ADDR(); i += 4096, y++){
		if(frames[y]){
			frames[y] = !frames[y];
			to_return = (void*)addresses[y];
			break;
		}
	}
//	if(!to_return)
//		PANIC("No more frames available.");
	for(size_t y = 0; y < 0x1000; y++)
		to_return[y] = 0;
	return to_return;
}

/*
int32_t add_page_to(uint32_t* page_directory){
	for(size_t i = 0; i < 1024; i++)
		if(page_directory[i] & 1){
			uint32_t* page_table = UNPACK_ADDR(page_directory, i);
			for(size_t y = 0; y < 1024; y++){
				if(page_table[i] % 2){
					page_table[i] = (uint32_t)get_new_page();
					return page_table[i];
				}
			}
			NEW_EMPTY_TABLE(temp);
			temp[0] = get_new_page();
			ASSIGN_PRESENT_ADDR(page_directory, i+1, temp);
			return temp[0];
			///create new page directory entry
		}
	
	failure:
		PANIC("Unable to add page to process. Aborting.");
}
*/
#if 0
#define ON_FRAMES_ITERATOR(addrvar, entry, modifydir, beginning, condition) \
{  \
	for(uint32_t )
}
#endif

#define HAS_TABLE(x) (!((x) % 2))

#if 0
void identity_map_from(uint32_t* directory, uint32_t start_addr, uint32_t end_addr){
	uint32_t beginning = start_addr - (start_addr % 4096);
	uint32_t directory_entry = beginning / (1024*4096);
	uint32_t table_entry = (beginning - (directory_entry * 1024 * 4096)) / 4096;
	for(uint32_t i = 0; beginning + (i*4096) < start_addr; i++){
		if(HAS_TABLE(((uint32_t)UNPACK_ADDR(directory, directory_entry)))){

		} else{
			directory[directory_entry] |= (0x1 | 0x8);
		}

	}
}
#endif