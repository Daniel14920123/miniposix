#include "syscalls/syscalls.h"
#include "tables/tables.h"
#include "macros/macros.h"
#include "tmterm/tmterm.h"
#include "kstring.h"
#include "convert.h"
#include "process/process.h"
#include "helpers/kmalloc.h"
#include "imported/imported.h"
#include "elf/elf.h"
#include "paging/paging.h"
#include "IPC/IPC.h"
#include "process/sbrk.h"

size_t number_of_syscalls = 0;
size_t number_of_syscalls_done = 0;

#define GET_AS(type_no_ptr) (type_no_ptr*)(data[1])

char* beginswith(char* str, char* prefix){
    size_t i;
    for(i = 0; prefix[i]; i++){
        if(str[i] != prefix[i]) return 0;
    }
    return str + i;
}

#define EXECUTE_FOR(string, ...) if(kstrequal(cmd, #string)) { __VA_ARGS__ ; return 1; }
#define EXECUTE_FORv(var, string, ...) if(kstrequal(var, #string)) { __VA_ARGS__ ; return 1; }
#define EXECUTE_PREF(string, ...) {char* argument = beginswith(cmd, #string); if(argument && *argument) {argument++; __VA_ARGS__; return 1;}}
int32_t handle_command(char* cmd, registers_t* regs){
    if(kstrequal(cmd, "")) return 1;
    EXECUTE_FOR(help,
        q_print("+ hello - greetings\n");
        q_print("+ ls - list files\n");
        q_print("+ echo [str] - prints str\n");
        q_print("+ credits - show credits\n");
        q_print("+ panic [str] - crashes the kernel\n");
        q_print("+ restart - restart auxiliary services and reset kernel stack\n");
        q_print("+ spawn term - spawn new term. Switch using arrows\n");
        q_print("+ remove term [index] - removes the [index] terminal\n");
        q_print("Environment vaariables :\n");
        q_print("+ curterm : current terminal number.\n");
    )
    EXECUTE_FOR(hello, 
        q_print("Hello Mister Boullay!!\n"); 
    )
    EXECUTE_FOR(ls,
        q_print("[CURRENT FILES]\n");
        q_print("+ ./\n");
        q_print("+ kernel.bin\n");
        q_print("+ isr33.segment\n");
        q_print("+ isr32.segment\n");
    )
    EXECUTE_FOR(credits,
        q_print("Credits :\n");
        q_print("+ Daniel FREDERIC\n"); 
        q_print("+ David Chemaly\n");
        q_print("+ Raphael Moresein\n");
        q_print("+ Aurelien xxx\n");
    )
    EXECUTE_PREF(panic,
        PANIC(argument);
    )
    EXECUTE_FOR(curdir, 
        q_print("Current directory: / (virtual)\n");
    )
    EXECUTE_PREF(echo,
        EXECUTE_FORv(argument, $(curterm),
            q_print("Terminal ");
            q_print_int(get_current_term()+1);
            q_print(" of ");
            q_print_int(get_term_number());
            q_print(".\n");
        )
        q_print(argument);
        q_print("\n");
    )
    EXECUTE_FOR(spawn term,
        multiprint(get_term_number(), ">>> ");
        if(!add_term()){
            q_print("Unable to add terminal (current number :");
            q_print_int(get_term_number());
            q_print(")\n");
        }
    )
    /*EXECUTE_PREF(run,
        int lul = atoi(argument);
        if(lul < imported_file_n){
            elf_load_file(RAMDISK_TO_FILE(lul));
            //switch_task(regs);
        }
        else
            q_print("Invalid index.\n")
    )*/
    {char* argument = beginswith(cmd, "run"); 
    if(argument && *argument) {
        argument++; int lul = atoi(argument); 
        if(lul < imported_file_n){ 
            elf_load_file(((FILE){.position = 0, .string = imported_files[lul].str, .strlength = imported_files[lul].size, .flags = 0})); 
        } else {
            multiprint(get_current_term(), "Invalid index.\n"); multiswitch(get_current_term());
        }; return 1;
    }}
    EXECUTE_PREF(remove term,
        int32_t id = atoi(argument);
        if(id == get_current_term()){
            q_print("Unable to remove the current terminal.\n");
        }
        else
            remove_term(id);
    )
    return 0;
}
#undef EXECUTE_FOR

void syscall_callback(registers_t* regs){
    DISABLE_INTERRUPTS();
    // extern bool scheduler_enabled;
	// scheduler_enabled = false;
    uint32_t* data = (uint32_t*)(regs->eax);
    uint32_t code = data[0];
    switch(code) {

#ifdef ADD_DRIVER_LEGACY
    case ADD_DRIVER_LEGACY:
    {
        typedef struct {
            char isr;
            void(*callback)(registers_t* regs);
            bool success;
        } driver;
        driver* to_add = GET_AS(driver);
        register_interrupt_handler(to_add->isr, to_add->callback);
        q_print("DRIVER LOADED FOR ISR ");
        q_print_int((int)(to_add->isr));
        q_print(".\n");
        to_add->success = true;
    } break;
#endif

#ifdef PRINT
    case PRINT:
    {
        char* value = GET_AS(char);
        q_print(value);
        multiflush();
    } break;
#endif

#ifdef HANDLE_KRISBOOL_INPUT
    case HANDLE_KRISBOOL_INPUT:
    {
        char* input = GET_AS(char);
        if(!handle_command(input, regs)){
            q_print("Error command `");
            q_print(input);
            q_print("` not found.\n");
        }
    } break;
#endif

#ifdef CREATE_FILE
    case CREATE_FILE:
    {
        typedef struct{

        } input_struct;
    } break;
#endif

#ifdef RANDOM
static uint32_t random_next_seed = 1;
#else 
#ifdef SRANDOM
static uint32_t random_next_seed = 1;
#endif
#endif

#ifdef RANDOM
    case RANDOM:
    {
        uint32_t *gen_rd = GET_AS(uint32_t);
        /* RAND_MAX assumed to be 32767 */
        random_next_seed = random_next_seed * 1103515245 + 12345;
        *gen_rd = ((unsigned)(random_next_seed/65536) % 32768);
    } break;
#endif

#ifdef SRANDOM
    case SRANDOM:
    {
        uint32_t *seed = GET_AS(uint32_t);
        random_next_seed = *seed;
    } break;
#endif

#ifdef EXIT
    case EXIT:
    {
        uint32_t* return_value = GET_AS(uint32_t);
        q_print("\n");
        q_print("Exiting subtask with code :");
        q_print_size_t(*return_value);
        q_print("\n");
        use_kernel_mapping();
        extern process* current_proc;
        if(current_proc->message_sent){
                current_proc->should_exit = true;
                current_proc->is_paused = true;
           // switch_task(regs);
            // loadPageDirectory(current_proc->pbi.page_directory);
            //return;
        } else {
            remove_current_proc();
        }
        if(are_tasks_scheduled())
            switch_task(regs);
        else{
            ENABLE_INTERRUPTS();
            WAIT_INTERRUPTS();
        }
        // ENABLE_INTERRUPTS();
        // while(1);
    } break;
#endif

#ifdef FORK
    case FORK:
    {
        use_kernel_mapping();
        process current_proc = copy_current_proc();
        process forked = fork(&current_proc);
        forked.regs = *regs;
        use_kernel_mapping();
        process* in_heap = my_malloc(sizeof(process));
        Add_process_to_schedule(memcpy(in_heap, &forked, sizeof(process)));
        loadPageDirectory(current_proc.pbi.page_directory);
        uint32_t* child_pid = GET_AS(uint32_t);
        *child_pid = forked.pid;
        loadPageDirectory(forked.pbi.page_directory);
        *child_pid = 0;
        loadPageDirectory(current_proc.pbi.page_directory);
    } break;
#endif

#ifdef GETPID
    case GETPID:
    {
        uint32_t* input = GET_AS(uint32_t);
        use_kernel_mapping();
        process current_proc = copy_current_proc();
        loadPageDirectory(current_proc.pbi.page_directory);
        *input = current_proc.pid;
    } break;
#endif

#ifdef GETPPID
    case GETPPID:
    {
        uint32_t* input = GET_AS(uint32_t);
        use_kernel_mapping();
        process current_proc = copy_current_proc();
        loadPageDirectory(current_proc.pbi.page_directory);
        *input = current_proc.parent;
    } break;
#endif

#ifdef SEND
    case SEND:
    {
        typedef struct
        {
            size_t port_pid;
            message msg;
        } send_ipc_params;
        send_ipc_params params = *GET_AS(send_ipc_params);
        // q_print(params.msg.data);
        use_kernel_mapping();
        
        extern process* current_proc;
        send(params.port_pid, current_proc, params.msg);
        loadPageDirectory(current_proc->pbi.page_directory);
    } break;
#endif

#ifdef RECEIVE
    case RECEIVE:
    {
        typedef struct{
            size_t port_pid;
            void* buffer;
            size_t len;
            size_t written;
            pid_t sender;
        } send_ipc_params;
            send_ipc_params* params = GET_AS(send_ipc_params);
        send_ipc_params on_stack = *params;
        use_kernel_mapping();
        process current_proc = copy_current_proc();
        size_t lul = receive(on_stack.port_pid, on_stack.buffer, on_stack.len, &(on_stack.sender));
        loadPageDirectory(current_proc.pbi.page_directory);
        params->written = lul;
        params->sender = on_stack.sender;

    } break;
#endif
#ifdef BIND_PORT
    case BIND_PORT:
    {

        use_kernel_mapping();
        extern process* current_proc;
        uint32_t port = bind_a_port(current_proc);
        loadPageDirectory(current_proc->pbi.page_directory);
        *GET_AS(uint32_t) = port;


    }break;
#endif

#ifdef REQUEST
    case REQUEST:
    {
         typedef struct{
            size_t port_pid;
            void* buffer;
            size_t len;
            size_t written;
            pid_t sender;
        } send_ipc_params;
        send_ipc_params* params = GET_AS(send_ipc_params);
        send_ipc_params on_stack = *params;
        use_kernel_mapping();
        extern process* current_proc;
        bool status = GetPortWithId(on_stack.port_pid)->msg_queue == NULL;
        if(status)
            current_proc->is_paused = on_stack.port_pid;

        number_of_syscalls_done++;
        switch_task(regs);
        // process current_proc_stack = copy_current_proc();
        // size_t lul = receive(on_stack.port_pid, on_stack.buffer, on_stack.len);
        //loadPageDirectory(current_proc_stack.pbi.page_directory);
        //params->written = lul;

    }break;

#endif

#ifdef SBRK
    case SBRK:
    {
        typedef struct{
            void* addr;
            size_t len;
        } brk_info;
        brk_info* info = GET_AS(brk_info);
        size_t len = info->len;
        use_kernel_mapping();
        extern process* current_proc;
        uint32_t* page_directory = current_proc->pbi.page_directory;
        process_break_info pbi = current_proc->pbi;
        void* addr = sbrk(&pbi, (int32_t)len);
        current_proc->pbi = pbi;
        loadPageDirectory(pbi.page_directory);
        info->addr = addr;
    }break;
#endif

#ifdef SEND_SIGNAL
    case SEND_SIGNAL:
    {
        extern process* pqueue;
        typedef struct{
            pid_t proc;
            signal_t sig;
        } signal_info;
        signal_info sig = *GET_AS(signal_info);
        use_kernel_mapping();
        extern process* current_proc;
        process* receiver = pqueue;
        while(receiver && receiver->pid != sig.proc)
            receiver = receiver->next;
        if(receiver){
            signal* to_append = receiver->signal_queue;
            while(to_append && to_append->next)
                to_append = to_append->next;
            if(!to_append){
                receiver->signal_queue = my_malloc(sizeof(signal));
                to_append = receiver->signal_queue;
            }
            else{
                to_append->next = my_malloc(sizeof(signal));
                to_append = to_append->next;
            }
            to_append->sigcode = sig.sig;
            
            //hack the registers of the receiver
            if(receiver->signal_handler){
                receiver->regs_vault_in_case_of_signal = receiver->regs;
                receiver->regs.eip = receiver->signal_handler;
                receiver->is_paused = false;
            }
            // receiver->regs.esp -= 4;
            //
            
            loadPageDirectory(current_proc->pbi.page_directory);
        } else{
            loadPageDirectory(current_proc->pbi.page_directory);
            signal_info* lol = GET_AS(signal_info);
            lol->proc = (pid_t)-1;
            lol->sig = (signal_t)-1;
        }

    }break;
#endif
#ifdef SET_SIGNAL_HANDLER
    case SET_SIGNAL_HANDLER:
    {
        typedef struct{
            uint32_t sighandler;
            signal_t* sigdef;
        } sighandler_info;
        sighandler_info addr = *GET_AS(sighandler_info);
        use_kernel_mapping();
        extern process* current_proc;
        current_proc->signal_handler = addr.sighandler;
        current_proc->sigswap = addr.sigdef;
        loadPageDirectory(current_proc->pbi.page_directory);
    }break;
#endif
#ifdef SIGHANDLER_RETURN 
    case SIGHANDLER_RETURN:
    {
        use_kernel_mapping();
        extern process* current_proc;
        if(current_proc->signal_queue){
            current_proc->regs = current_proc->regs_vault_in_case_of_signal;
            current_proc->signal_queue = current_proc->signal_queue->next;
            memcpy(regs, &(current_proc->regs), sizeof(registers_t));
            switch_task(regs);
        }else
            loadPageDirectory(current_proc->pbi.page_directory);

    }break;
#endif
#ifdef EXECVE
    case EXECVE:
    {

        void* allocate_in_disabled(process* proc, size_t x){
            void* r = sbrk(&(proc->pbi), x);
            disablePaging();
            return get_physical(r, proc->pbi.page_directory);
        }
        
        typedef struct {
            char* path;
            char** argv;
            char** environ;
        } execve_struct;

        execve_struct info = *GET_AS(execve_struct);

        size_t argv_size = 0;
        size_t argv_number = 0;
        size_t env_size = 0;
        size_t env_number = 0;
        size_t temp = 0;
        size_t i = 0;

        use_kernel_mapping();
        disablePaging();
        process* new_process = elf_load_file(RAMDISK_TO_FILE(((size_t)info.path)));
        enablePaging();

        extern process* current_proc;
        uint32_t* new_pd = new_process->pbi.page_directory;
        uint32_t* old_pd = current_proc->pbi.page_directory;
        uint32_t addr_env = 0;
        uint32_t addr_argv = 0;

        process proc = *new_process;
        disablePaging();
        for(;*(char**)get_physical((info.environ+env_size),old_pd);env_size++);
        // loadPageDirectory(new_pd);
        char** env_virtual = sbrk(&(proc.pbi),env_size*sizeof(void*));
        disablePaging();
        char** env_physical_tanguy = get_physical(env_virtual, new_pd);
        char** physical_environ = get_physical(info.environ, old_pd);
        
        for(uint32_t i = 0; i < env_size; i++){
            char* temp = get_physical(physical_environ[i], old_pd);
            char* dest = sbrk(&(proc.pbi), kstrlen(temp) + 1);
            disablePaging();
            kstrcpy(((char*)get_physical(physical_environ[i], old_pd)), (char*)get_physical(dest,new_pd));
            env_physical_tanguy[i] = dest;
        }
        enablePaging();

        *new_process = proc;
        new_process->regs.eax = (uint32_t)env_virtual;
        new_process->regs.ebx = 0;//argv_virtual;
        new_process->signal_queue = current_proc->signal_queue;
        new_process->pid = current_proc->pid;
        new_process->time = current_proc->time;
        new_process->parent = current_proc->parent;
        new_process->has_been_launched = false;
        new_process->is_paused = false;
        *regs = current_proc->regs;
        remove_current_proc();
        switch_task(regs);
        return;
    }break;
#endif

#ifdef RQST_IO_PERMS
    case RQST_IO_PERMS:
    {
        regs->eflags |= ((1 << 12) | (1 << 13));
    }break;
#endif

#ifdef GET_PHYS_BUFF
    case GET_PHYS_BUFF:
    {
        //TODO: security
        //TODO: when not page aligned
        typedef struct{
            uint32_t addr;
            uint32_t len;
            void* mapped_addr;
        } phys_buff_info;
        phys_buff_info* info   = GET_AS(phys_buff_info);
        phys_buff_info info_st = *info;
        size_t offset = info_st.addr % 4096;
        use_kernel_mapping();
        extern process* current_proc;
        process_break_info pbi = current_proc->pbi;
        if(pbi.program_break % 4096)
            sbrk(&(pbi), 4096 - pbi.allocated_in_current_frame);
        size_t target_addr = pbi.program_break;
        loadPageDirectory(pbi.page_directory);
        info->mapped_addr = sbrk(&pbi, info_st.len);
        
        uint32_t* tablo = craft_addr(1023, target_addr >> 22, ((target_addr << 10) >> 22)*4);
        for(size_t i = 0; i < info->len/4096+1; i++)
            tablo[i] = (tablo[i] & 0xFFF) | (info_st.addr + i * 4096);
        
        sbrk(&pbi, 1);
        
        ASM_I(
            push %eax;
            movl	%cr3,%eax;
            movl	%eax,%cr3;
            pop %eax;
        );

        use_kernel_mapping();
        current_proc->pbi = pbi;
        loadPageDirectory(pbi.page_directory);


        // *(info.mapped_addr >> 12 | (0xFF)<<22)=l'adressephysick ;
        // assert(!(pbi.program_break % 4096), "bruh WTF not page aligned");
        // assert((uint32_t)craft_addr(pbi.pde, pbi.next_pte, pbi.allocated_in_current_frame) == pbi.program_break, "NANI bruh WTF craft_addr error");
        // disablePaging();
        //         
        // enablePaging();




    } break;
#endif
#undef BIND_TO_ISR
#ifdef BIND_TO_ISR
    case BIND_TO_ISR:
    {
        typedef struct{
            bool has_succeded;
            uint16_t interrupt_number;
        } 

    }break;

#endif

#ifdef RECEIVE_IRQS_ON
    case RECEIVE_IRQS_ON:
    {
        typedef struct{
            uint32_t port_number;
            uint16_t irq_number;
            bool has_succeded;
        } broadcast_struct;
        broadcast_struct* x = GET_AS(broadcast_struct);
        extern uint32_t attached_to_irq[16];
        if(x->irq_number >= 16){
            x->has_succeded = false;
            return;
        }
        attached_to_irq[x->irq_number] = x->port_number;
        x->has_succeded = true;
    }break;
#endif
#ifdef THREAD
    case THREAD:
    {
        use_kernel_mapping();
        process current_proc = copy_current_proc();
        process threaded = thread(&current_proc);
        threaded.regs = *regs;
        use_kernel_mapping();
        process *in_heap = my_malloc(sizeof(process));
        Add_process_to_schedule(memcpy(in_heap, &threaded, sizeof(process)));
        loadPageDirectory(current_proc.pbi.page_directory);
        uint32_t *child_pid = GET_AS(uint32_t);
        *child_pid = threaded.pid;
        loadPageDirectory(threaded.pbi.page_directory);
        *child_pid = 0;
        loadPageDirectory(current_proc.pbi.page_directory);
    }
    break;
#endif
#ifdef BIND_THAT_PORT
    case BIND_THAT_PORT:
    {
        
        typedef struct{
            uint32_t port;
            bool status;
        } bind_port_msg;

        bind_port_msg port_info = *GET_AS(bind_port_msg);
        use_kernel_mapping();
        extern process* current_proc;
        port_info.status = bind_that_port(current_proc,port_info.port);
        loadPageDirectory(current_proc->pbi.page_directory);
        (GET_AS(bind_port_msg))->status = port_info.status;

    }
    break;


#endif
#ifdef MMAP
    case MMAP:
    {
        typedef struct{
            uint32_t addr; //below 1M
            uint32_t length;
            uint8_t success;
            uint8_t flags; // bool
        } mmap_info;
        mmap_info *info = GET_AS(mmap_info);
        mmap_info on_stack = *GET_AS(mmap_info);
        if(on_stack.addr >= 0x100000) {
            use_kernel_mapping();
            extern process* current_proc;
            process_break_info pbi = current_proc->pbi;
            add_space(&pbi, (void*)on_stack.addr, on_stack.length, PG_USERLAND | ((on_stack.flags & 1) ? PG_WRITABLE : 0) | PG_PRESENT);
            loadPageDirectory(pbi.page_directory);
            info->success = 1;
        } else {
            info->success = 0;
        }
    }
    break;
#endif
#ifdef MUNMAP
    case MUNMAP:
    {
        typedef struct{
            uint32_t addr; //below 1M
            uint32_t length;
            uint8_t success;
        } mmap_info;
        mmap_info *info = GET_AS(mmap_info);
        if(info->addr >= 0x100000 && info->length >= 4096){
            if(info->addr % 4096){
                uint32_t remaining = (4096 - (info->addr % 4096));
                info->addr += remaining;
                info->length -= remaining;
            }
            
            while (info->length >= 4096) {
                if( !(*((uint32_t*)craft_addr(1023, 1023, (info->addr >> 22)*4)) & PG_PRESENT)){
                    info->length -= 4096;
                    continue;
                }

                *(uint32_t*)craft_addr(1023, info->addr >> 22, (info->addr >> 12)*4) = 0;
                info->length -= 0x1000;
            }

            ASM_I(
                push %eax;
                movl	%cr3,%eax;
                movl	%eax,%cr3;
                pop %eax;
            );
        } else {
            info->success = 0;
        }
    }
    break;
#endif

    default:
        PANIC("Error syscall not supported");
        break;
    }

    number_of_syscalls_done++;
}