#include "vfs/vfs.h"
#include "kstring.h"

static node nodedir[MAX_NODES];

file_descriptor get_actual_file(file_descriptor fd){
	node* ptr = nodedir + fd;
	while(ptr->symlink){
		ptr = ptr->symlink;
	}
	return ptr->fd;
}


node* new_file(){
	node* ptr = NULL;
	for(uint32_t i = 1; i < MAX_NODES; i++){
		if(!nodedir[i].fd){
			nodedir[i].fd = i;
			ptr = nodedir + i;
			break;
		}
	}
	return ptr;
}

node* new_file_in(node* folder){
	node* new = new_file();
	while(folder->link_sub){
		folder = folder->link_sub;
	}
	folder->link_sub = new;
	return new;
}

bool free_file(file_descriptor fd){
	node* ptr = nodedir + fd;
	if(!ptr->fd)
		return false;
	if(!ptr->is_folder){
		do {
			for(uint32_t i = 0; i < DATA_MAX; i++){
				ptr->data[i] = 0;
			}
			node* to_erase = ptr;
			ptr = ptr->link;
			to_erase->fd = 0;
		} while (ptr->link);
	} else{
		do {
			node* to_erase = ptr;
			ptr = ptr->link_sub;
			if(!free_file(ptr->fd)) 
				return false;
			to_erase->fd = 0;
			to_erase->dataeof = 0;
		} while (ptr->link_sub);
	}
	return true;
}

bool get_text(file_descriptor fd, char* buffer, uint32_t buffer_size){
	node* ptr = nodedir + fd;
	if(!ptr->fd)
		return false;
	while(ptr->fd){
		for(size_t i = 0; i < DATA_MAX; i++){

			// A file AT LEAST contains the character \0, => dataeof > 0
			if(ptr->dataeof && i == ptr->dataeof)
				return true;
			
			(buffer++)[0] = ptr->data[i];
			buffer_size--;

			if(!buffer_size)
				return false;
		}
		ptr = ptr->link;

		if(ptr->fd && ptr->is_folder) 
			return false;
	}
	return true;
}

bool store_text(file_descriptor fd, char* buffer){
	node* ptr = nodedir + fd;
	if(!ptr->fd || ptr->is_folder)
		return false;
	for(size_t i = 0; buffer[i]; i++){
		ptr->dataeof++;
		ptr->data[i%DATA_MAX] = buffer[i];
		if(ptr->dataeof == DATA_MAX){
			ptr->dataeof = 0;
			ptr->link = new_file();
			//no more available entries
			if(!ptr->link) 
				return false;
			ptr = ptr->link;
		}
	}
	return true;
}
#if 0
bool get_subfiles(file_descriptor folder, file_descriptor* list_of_descriptors, uint32_t list_length){
	node* ptr = get_actual_file(folder);
	if(!ptr->is_folder) 
		return false;
	while(ptr->link_sub){
		ptr = ptr->link_sub;
	}
}
#endif

file_descriptor path(file_descriptor root, char* p){
	//add check for folder maybe
	if(p[0] == 0)
		return get_actual_file(root);	//root of the filesystem

	node* ptr = nodedir + get_actual_file(root);
	char buff[256] = {0};
	//for(size_t i = 0; p[i]; i++){
	//Parse the string
	uint32_t y = 0, i = 0;
	for(; p[i] && p[i] != FOLDER_PATH_SEPARATOR; y++, i++){
		buff[y] = p[i];
	}

	//Add end of string
	buff[++i] = 0;

	//iterate on subfiles and subfolders
	while(ptr->link_sub){
		ptr = ptr->link_sub;
		if(kstrequal(buff, ptr->filename))
			return path(ptr->fd, p+i); //maybe bug here
	}

	return path(root, p+1);
	//}
}

node* initrd_init(){
	node* ptr = new_file();
	ptr->is_folder = true;
	node* test = new_file_in(ptr);
	kstrcpy("votai .test", test->filename);
	store_text(test->fd, "Bruh ca marche fuck le monde !!");
	return ptr;
}