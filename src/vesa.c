#include "vesa/vesa.h"

#include "tmterm/tmterm.h"

vbe_mode_info_structure vbe_mode_info;
extern vbe_info_structure vbe_info;
uint32_t vbe_info_addr;

extern void fill_vbe_info();

void init_vesa(){
    fill_vbe_info();
    char stored_string[5];
    for(size_t i = 0; i < 4; i++)
        stored_string[i] = vbe_info.signature[i];
    stored_string[4] = '\0';
    multiprint(get_current_term(), stored_string);
    multiflush();
}