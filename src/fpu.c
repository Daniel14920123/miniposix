#include "fpu/fpu.h"

void no_fpu(){
	q_print("FPU DISABLE\n");
}
void has_fpu(){
	q_print("FPU ENABLED\n");
}

void no_sse(){
	q_print("SSE IS NOT AVAILABLE\n");
}
void has_sse(){
	q_print("SSE ENABLED\n");
}

void fpu_load_control_word(const uint16_t control)
{
    asm volatile("fldcw %0;"::"m"(control)); 
}

uint16_t fpu_get_control_word()
{
	uint16_t control;
    asm volatile("fstcw %0":"=m"(control):);
	return control;
}

uint16_t fpu_get_status_word()
{
	uint16_t status;
    asm volatile("fstsw %0":"=m"(status):);
	return status;
}