#include "elf/v2.h"
#include "process/mapping.h"
#include "helpers/kmalloc.h"
#include "paging/flags.h"
#include "tmterm/tmterm.h"
#include "convert.h"
#include "usermode/usermode.h"
#include "process/process.h"
#include "paging/paging.h"
#include "helpers/kmalloc.h"
#include "paging/flags.h"

typedef struct{
    size_t position;
    char* string;
    size_t strlength;
    uint8_t flags;
} FILE;

#define SEEK_SET 1
#define SEEK_CUR 2
#define SEEK_END 3

void fseek(FILE* file, long off, char mode){
    if(file->flags % 2)
        return;
    switch(mode){
        case SEEK_CUR:{
            file->position += off;
            break;
        }
        case SEEK_SET:{
            file->position = off;
            break;
        }
        case SEEK_END:{
            file->position += off;
            break;
        }
        default:
        break;
    }
    if(file->position >= file->strlength)
        file->flags |= 1;
}



size_t fread( void * buffer, size_t blocSize, size_t blocCount, FILE *stream )
{
    if(stream->flags%2)
        return 0;
    char *dest = (char*)buffer; 
    size_t l = stream->strlength - stream->position;
    if(l>blocSize*blocCount)
        l=blocSize*blocCount;    
    for(size_t i = 0; i<l;i++)
       dest[i]=stream->string[stream->position+i];
    return l;
}

bool elf_check_file(Elf32_Ehdr *hdr) {
	if(!hdr) return false;
	if(hdr->e_ident[EI_MAG0] != ELFMAG0) {
		ERROR("ELF Header EI_MAG0 incorrect.\n");
		return false;
	}
	if(hdr->e_ident[EI_MAG1] != ELFMAG1) {
		ERROR("ELF Header EI_MAG1 incorrect.\n");
		return false;
	}
	if(hdr->e_ident[EI_MAG2] != ELFMAG2) {
		ERROR("ELF Header EI_MAG2 incorrect.\n");
		return false;
	}
	if(hdr->e_ident[EI_MAG3] != ELFMAG3) {
		ERROR("ELF Header EI_MAG3 incorrect.\n");
		return false;
	}
	return true;
}


bool elf_check_supported(Elf32_Ehdr *hdr) {
	if(!elf_check_file(hdr)) {
		ERROR("Invalid ELF File.\n");
		return false;
	}
	if(hdr->e_ident[EI_CLASS] != ELFCLASS32) {
		ERROR("Unsupported ELF File Class.\n");
		return false;
	}
	if(hdr->e_ident[EI_DATA] != ELFDATA2LSB) {
		ERROR("Unsupported ELF File byte order.\n");
		return false;
	}
	if(hdr->e_machine != EM_386) {
		ERROR("Unsupported ELF File target.\n");
		return false;
	}
	if(hdr->e_ident[EI_VERSION] != EV_CURRENT) {
		ERROR("Unsupported ELF File version.\n");
		return false;
	}
	if(hdr->e_type != ET_REL && hdr->e_type != ET_EXEC) {
		ERROR("Unsupported ELF File type.\n");
		return true;
	}
	return true;
}

Elf32_Phdr *elf_program(Elf32_Ehdr *hdr)
{
    return (Elf32_Phdr *)((int)hdr + hdr->e_phoff);
}


process* read_program_headers(Elf32_Ehdr *hdr, FILE* file)
{
    Elf32_Phdr *phdr = elf_program(hdr);

    //use_kernel_mapping();
    process new = new_process(0, 5);
    loadPageDirectory(new.pbi.page_directory);
    enablePaging();
    for (int i = 0; i < hdr->e_phnum; i++)
    {
        Elf32_Phdr *program = &phdr[i];
        switch (program->p_type)
        {
        case PT_LOAD:
            /*
                Allocate virtual memory for each segment, at the address specified by the p_vaddr member in the program header.
                The size of the segment in memory is specified by the p_memsz member.
                Copy the segment data from the file offset specified by the p_offset member to the virtual memory address specified by the p_vaddr member.
                The size of the segment in the file is contained in the p_filesz member. This can be zero.
            */
           {
               
               set_then_apply(&new.pbi, (void*)program->p_vaddr, file->string + program->p_offset, program->p_memsz, PG_WRITABLE | PG_USERLAND);
           }
            break;

        default:
            break;
        }
       // q_print("reading\n");
    }
    void* pikalul = add_space(&new.pbi, (void*)0xffadbeef, 8192, PG_USERLAND | PG_WRITABLE | PG_STACK);
    void* heap = sbrk(&new.pbi, 1);
	pikalul += 8180;
	new.regs.esp = (uint32_t)(pikalul);
    //void* kstack=add_space(&new.pbi, (void*)0xffffd000, 4096, PG_WRITABLE);
    use_kernel_mapping();
    process* in_heap = my_malloc(sizeof(process));
    memcpy(in_heap, &new, sizeof(process));
    Add_process_to_schedule(in_heap);
    return in_heap;
}

static inline Elf32_Shdr *elf_sheader(Elf32_Ehdr *hdr, FILE* file)
{
    return (Elf32_Shdr *)((int)hdr + hdr->e_shoff);
	//Elf32_Shdr * test = (Elf32_Ehdr *)my_malloc(sizeof(Elf32_Shdr));
	// fseek(file, hdr->e_shoff, SEEK_SET);
	// fread(test, sizeof(Elf32_Shdr), 1, file);
	// return test;
}

// Return the specific section header
static inline Elf32_Shdr *elf_section(Elf32_Ehdr *hdr, int idx, FILE* file)
{

	return &elf_sheader(hdr, NULL)[idx];
	// Elf32_Shdr * test = (Elf32_Ehdr *)my_malloc(sizeof(Elf32_Shdr));
	// fseek(file, hdr->e_shoff + (idx*sizeof(Elf32_Shdr)), SEEK_SET);
	// fread(test, sizeof(Elf32_Shdr), 1, file);
	// return test;

}

int elf_load_stage1(Elf32_Ehdr *hdr, FILE* file)
{
    Elf32_Shdr *shdr = elf_sheader(hdr, file);

    unsigned int i;
    // Iterate over section headers
    for (i = 0; i < hdr->e_shnum; i++)
    {
        Elf32_Shdr *section = elf_section(hdr, i, file);

        // If the section isn't present in the file
        if (section->sh_type == SHT_NOBITS)
        {
            // Skip if it the section is empty
            if (!section->sh_size)
                continue;
            // If the section should appear in memory
            if (section->sh_flags & SHF_ALLOC)
            {
                // Allocate and zero some memory
                void *mem = my_malloc(section->sh_size);
                memset(mem, 0, section->sh_size);

                // Assign the memory offset to the section offset
                section->sh_offset = (int)mem - (int)hdr;
                //q_print("Allocated memory for a section.\n");
                //q_print_size_t(section->sh_size);
                //q_print("\n");
            }
        }
    }
    return 0;
}
static inline void *elf_load_rel(Elf32_Ehdr *hdr, FILE* file, process* proc)
{
    int result;
    result = elf_load_stage1(hdr, file);
    if (result == ELF_RELOC_ERR)
    {
        ERROR("Unable to load ELF file.\n");
        return NULL;
    }
    //  result = elf_load_stage2(hdr);
    if (result == ELF_RELOC_ERR)
    {
        ERROR("Unable to load ELF file.\n");
        return NULL;
    }
    // TODO : Parse the program header (if present)
    jump_addr = (void*)hdr->e_entry;
    proc->regs.eip = hdr->e_entry;
    return (void *)hdr->e_entry;
}

static inline char *elf_str_table(Elf32_Ehdr *hdr, FILE* file)
{
    if (hdr->e_shstrndx == SHN_UNDEF)
        return NULL;
    
   return (char *)hdr + elf_section(hdr, hdr->e_shstrndx, NULL)->sh_offset;
}

static inline char *elf_lookup_string(Elf32_Ehdr *hdr, int offset)
{
    char *strtab = elf_str_table(hdr, NULL);
    if (strtab == NULL)
        return NULL;
    return strtab + offset;
}

void list_section_name(Elf32_Ehdr *hdr, FILE* file)
{
    Elf32_Shdr *shdr = elf_sheader(hdr, file);

    for (int i = 0; i < hdr->e_shnum; i++)
    {
        Elf32_Shdr *section = &shdr[i];

        // If the section isn't present in the file
        if (section->sh_type != SHT_NOBITS)
        {
            // Skip if it the section is empty

            char *result = elf_lookup_string(hdr, section->sh_name);
            char *plouf = result;
            // if(result != )
            // printf("%s\n", result);
        }
    }
}

void *elf_load_file(FILE f)
{
    FILE* file = &f;
    // Elf32_Ehdr* hdr = (Elf32_Ehdr *)my_malloc(sizeof(Elf32_Ehdr));
    // fread(hdr, sizeof(Elf32_Ehdr), 1, file);
     // fseek(file, 0, SEEK_SET);
    Elf32_Ehdr *hdr = (Elf32_Ehdr *)(file->string);
    // elf_load_stage1(hdr, file);
    list_section_name(hdr, file);
    process* prog = read_program_headers(hdr, file);
    //prog->regs.eip = 0x4000000;
    if (!elf_check_supported(hdr))
    {
        ERROR("ELF File cannot be loaded.\n");
        return NULL;
    }
    switch (hdr->e_type)
    {
    case ET_EXEC:
        // TODO : Implement
        elf_load_rel(hdr, file, prog);//NULL;
    case ET_REL:
        elf_load_rel(hdr, file, prog);
    }
    return prog;
}

void *elf_lookup_symbol(const char *name)
{

    return NULL;
}

static int elf_get_symval(Elf32_Ehdr *hdr, int table, unsigned int idx)
{
    if (table == SHN_UNDEF || idx == SHN_UNDEF)
        return 0;
    Elf32_Shdr *symtab = elf_section(hdr, table, NULL);

    uint32_t symtab_entries = symtab->sh_size / symtab->sh_entsize;
    if (idx >= symtab_entries)
    {
        //ERROR("Symbol Index out of Range (%d:%u).\n", table, idx);
        return ELF_RELOC_ERR;
    }

    int symaddr = (int)hdr + symtab->sh_offset;
    Elf32_Sym *symbol = &((Elf32_Sym *)symaddr)[idx];
    if (symbol->st_shndx == SHN_UNDEF)
    {
        // External symbol, lookup value
        Elf32_Shdr *strtab = elf_section(hdr, symtab->sh_link, NULL);
        const char *name = (const char *)hdr + strtab->sh_offset + symbol->st_name;

        void *target = elf_lookup_symbol(name);

        if (target == NULL)
        {
            // Extern symbol not found
            if (ELF32_ST_BIND(symbol->st_info) & STB_WEAK)
            {
                // Weak symbol initialized as 0
                return 0;
            }
            else
            {
                //ERROR("Undefined External Symbol : %s.\n", name);
                return ELF_RELOC_ERR;
            }
        }
        else
        {
            return (int)target;
        }
    }
    else if (symbol->st_shndx == SHN_ABS)
    {
        // Absolute symbol
        return symbol->st_value;
    }
    else
    {
        // Internally defined symbol
        Elf32_Shdr *target = elf_section(hdr, symbol->st_shndx, NULL);
        return (int)hdr + symbol->st_value + target->sh_offset;
    }
}
