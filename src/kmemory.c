#include "helpers/kmalloc.h"
#include "process/sbrk.h"
#include "paging/paging.h"

process_break_info info;
process k_task;

size_t new_pid();

void init_kheap(){
	info.page_directory = set_up_directory();
	info.pde = 10;
	info.next_pte = 0;
	info.allocated_in_current_frame = 0;
	info.program_break = (uint32_t)craft_addr(10, 0, 0);

	k_task.pbi = info;
	k_task.is_paused = true;
	k_task.message_sent = 0;
	k_task.parent = 0;
	k_task.pbi = info;
	k_task.pid = new_pid();
	k_task.should_exit = false;
	k_task.signal_handler = 0;
	k_task.signal_queue = NULL;
	k_task.sigswap = NULL;
	k_task.time = 0;
}

void* ksbrk(int32_t len){
    return sbrk(&info, len);
}

void use_kernel_mapping(){
    loadPageDirectory(info.page_directory);
}

process* get_k_task(){
	return &k_task;
};