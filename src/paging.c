#include "paging/paging.h"
#include "macros/macros.h"
#include "memorymanager/memorymanager.h"

bool is_paging_enabled = false;

void loadPageDirectory(unsigned int* table ){
	asm volatile(" \
		mov %0, %%eax; \
		mov %%eax, %%cr3; \
	"::"r"(table));
}

void enablePaging(){
	if(is_paging_enabled)
		return;
	ASM_I(
		push %eax;
		push %ebx;
		mov %cr3, %eax;
		mov $0, %ebx;
		cmp %eax, %ebx;
		je .carre;
		mov %cr0, %eax;
		or $0x80010000, %eax;
		mov %eax, %cr0;
	.carre:
		pop %ebx;
		pop %eax;
	);
	is_paging_enabled = true;
	//q_print("PAGING ENABLED SUCCESSFULLY.\n");
}

void disablePaging(){
	if(!is_paging_enabled)
		return;
	ASM_I(
		push %eax;
		mov %cr0, %eax;
		and $0x7FFEFFFF, %eax;
		mov %eax, %cr0;
		pop %eax;
	);
	is_paging_enabled = false;
	//q_print("PAGING DISABLED SUCCESSFULLY.\n");
}

void set_frame(void* frame, uint32_t value){
	uint32_t* lul = frame;
	for(uint32_t i = 0; i < 1024; i++){
		lul[i] = value;
	}
}
void identity_map_table(void* frame, size_t pde_entry){
	extern char end_of_kernel;
	size_t i = pde_entry * 0x1000;
	uint32_t* table = frame;
	for(; i * 0x1000 <= (uint32_t)(&end_of_kernel) || i * 0x1000 <= (uint32_t)0xb8000; i++){
		table[i] = (i * 0x1000) | 3;
	}
}
void identity_map_directory(void* frame){
	uint32_t* first_page_table = get_new_page();
	uint32_t* second_page_table = get_new_page();
	set_frame(first_page_table, 0);
	set_frame(second_page_table, 0);
	identity_map_table(first_page_table, 0);
	identity_map_table(second_page_table, 1);
	((uint32_t*)frame)[0] = ((uint32_t)first_page_table) | 3;
	((uint32_t*)frame)[1] = ((uint32_t)second_page_table) | 3;
}

void* set_up_directory(){
	disablePaging();
	uint32_t* page_directory = get_new_page();
	set_frame(page_directory, 2);
	identity_map_directory(page_directory);
	page_directory[1023] = ((uint32_t)page_directory)| 3;
	return page_directory;
}

void* craft_addr(uint16_t pde, uint16_t pte, uint16_t offset){
	return (void*)(pde << 22 | pte <<12 | offset);
}

void* get_physaddr(void* virtualaddr){
	uint32_t pdindex = (uint32_t)virtualaddr >> 22;
	uint32_t ptindex = (uint32_t)virtualaddr >> 12 & 0x03FF;
	uint32_t * pd = (uint32_t *)0xFFFFF000;
	uint32_t * pt = ((uint32_t *)0xFFC00000) + (0x400 * pdindex);
	return (void *)((pt[ptindex] & ~0xFFF) + ((uint32_t)virtualaddr & 0xFFF));
}

void map_page(void * physaddr, void * virtualaddr, unsigned int flags){
	unsigned long pdindex = (unsigned long)virtualaddr >> 22;
	unsigned long ptindex = (unsigned long)virtualaddr >> 12 & 0x03FF;
	unsigned long * pd = (unsigned long *)0xFFFFF000;
	unsigned long * pt = ((unsigned long *)0xFFC00000) + (0x400 * pdindex);
	pt[ptindex] = ((unsigned long)physaddr) | (flags & 0xFFF) | 0x01;
}

uint32_t copy_frame(uint32_t entry){
	if(!(entry & 1))
		return 0;
	uint32_t* frame = get_new_page();
	uint32_t* addr = (void*)((uint32_t) entry & ~0xFFF);
	for(uint32_t i = 0; i < 1024; i++)
		frame[i] = addr[i];
	return ((uint32_t)frame) | (entry & 0xFFF);
}

uint32_t duplicate_frame(uint32_t entry){
	if(!(entry & 1))
		return 0;
	uint32_t* frame = get_new_page();
	uint32_t* addr = (void*)((uint32_t) entry & ~0xFFF);
	for(uint32_t i = 0; i < 1024; i++){
		frame[i] = copy_frame(addr[i]);
	}
	return ((uint32_t)frame) | (entry & 0xFFF);
}

// au niveau de la page table entry
uint32_t map_pte(uint32_t entry){

	// Si le bit de stack est set rajouter une page table
	uint32_t* addr = (void*)((uint32_t) entry & ~0xFFF);
	if(addr == 0)
		return 0;
	uint32_t* page_table_entry = get_new_page();
	for (size_t i = 0; i < 1024; i++)
	{
		if(addr[i] & PG_STACK)
			page_table_entry[i] = copy_frame(addr[i]);
		else
			page_table_entry[i] = addr[i];

	}
	return ((uint32_t)page_table_entry)  | (entry & 0xFFF);
}

void* get_physical(void* vaddr, uint32_t* page_directory){
    unsigned long pdindex = (unsigned long)vaddr >> 22;
    unsigned long ptindex = (unsigned long)vaddr >> 12 & 0x03FF;
    uint32_t* page_table = (uint32_t*)(page_directory[pdindex] & ~0xFFF);
    uint32_t* frame = (uint32_t*)(page_table[ptindex] & ~0xFFF);
    return (void*)((char*)frame + ((uint32_t)vaddr & 0xFFF));
}