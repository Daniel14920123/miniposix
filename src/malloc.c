#include "helpers/kmalloc.h"
#include "memory.h"

const size_t HSIZE = sizeof(header);
const size_t YES = 1;
const size_t NO = 0;
#ifndef NULL
#define NULL 0
#endif

void* get_data(header* h)
{
    return h + 1;
}

header* get_header_from_data(void* data)
{
    return (header *)data - 1;
}

size_t get_total(header* h)
{
    size_t total = (char *)h->next - (char *)get_data(h);
    return total;
}

header* get_sentinel()
{
    static header *sentinel = 0;

    if (sentinel == 0)
        sentinel = ksbrk(0);

    return sentinel;
}

void init_heap()
{
    header *sentinel = get_sentinel();
    ksbrk(HSIZE);

    sentinel->prev = NULL;
    sentinel->next = ksbrk(0);
    sentinel->size = 0;
    sentinel->free = NO;
}

header* expand_heap(header* last_header, size_t size)
{
    header *current = ksbrk(0);
    int *r = ksbrk(HSIZE + size);
    if (*r == -1)
        return NULL;

    current->prev = last_header;
    current->next = ksbrk(0);

    return current;
}

/**
 * Returns YES if the chunk is free or large enough for the specified size.
 */
static int is_valid(header* chunk, size_t size)
{
    return chunk->free && get_total(chunk) >= size;
}

header* find_free_chunk(size_t size)
{
    struct header *end = ksbrk(0);

    header *h;
    for (h = get_sentinel(); h->next != end; h = h->next)
    {
        if (is_valid(h->next, size))
            return h->next;
    }

    return h;
}

void* my_malloc(size_t size)
{
    if (!size)
        return NULL;
    
    size_t data_size = size;
    if (size % 8)
        size = (size / 8 + 1) * 8;
    
    header *chunk = find_free_chunk(size);
    if (!is_valid(chunk, size))
        chunk = expand_heap(chunk, size);
    
    if (chunk == NULL)
        return NULL;

    chunk->size = data_size;
    chunk->free = NO;

    size_t total_size = get_total(chunk);
    if (total_size - size > HSIZE)
    {
        header *new = (void *)((char *)get_data(chunk) + size);
        new->prev = chunk;
        new->next = chunk->next;
        chunk->next = new;
        new->size = total_size - size - HSIZE;
        new->free = YES;
    }

    return memset(get_data(chunk), 0, size);
}

void* my_calloc(size_t nmemb, size_t size)
{
    size_t result;
    if (__builtin_mul_overflow(nmemb, size, &result))
        return NULL;

    size *= nmemb;
    void *data = my_malloc(result);
    if (data == NULL)
        return NULL;

    memset(data, 0, result);
    return data;
}

void my_free(void* ptr)
{
    if (ptr == NULL)
        return;
    
    header *chunk = get_header_from_data(ptr);
    chunk->free = YES;
    memset(ptr, 0, chunk->size);

    void *prg_brk = ksbrk(0);

    if (chunk->prev->free)
    {
        chunk->prev->next = chunk->next;
        chunk->next->prev = chunk->prev;
        chunk = chunk->prev;
    }
    if (chunk->next != prg_brk && chunk->next->free)
    {
        chunk->next = chunk->next->next;
        chunk->next->prev = chunk;
    }

    if (chunk->next == prg_brk)
    {
        int offset = (char *)chunk->next - (char *)chunk;
        ksbrk(-offset);
    }
}

void* my_realloc(void* ptr, size_t size)
{
    if (ptr == NULL)
        return my_malloc(size);

    if (size == 0)
    {
        my_free(ptr);
        return NULL;
    }
    
    header *chunk = get_header_from_data(ptr);

    if (get_total(chunk) >= size)
    {
        chunk->size = size;
        return ptr;
    }

    void *new_data = my_malloc(size);
    if (new_data == NULL)
        return NULL;

    memcpy(new_data, ptr, chunk->size);
    my_free(ptr);

    return new_data;
}