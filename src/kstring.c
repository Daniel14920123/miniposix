#include "string.h"

int kstrlen(char* str) {
    int i = 0;
    for(; str[i]; i++){}
    return i;
}

void kstrcpy(char* src, char* dest){
    size_t i = 0;
    for(; src[i]; i++)
        dest[i] = src[i];
    dest[i] = 0;
}

int kstrequal(char* s1, char* s2){
    int i = 0;
    for(; s1[i] && s2[i]; i++){
        if(s1[i] != s2[i])
            return 0;
    }
    if(s1[i] == s2[i])
        return 1;
    return 0;
    
}