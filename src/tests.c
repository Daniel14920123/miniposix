#include "tests/tests.h"

#include "std.h"
#include "convert.h"
#include "tables/tables.h"
#include "kdrivers/kdrivers.h"
#include "tmterm/tmterm.h"
#include "textmode.h"
#include "paging/paging.h"
#include "vesa/vesa.h"
#include "macros/macros.h"
#include "syscalls/syscalls.h"
#include "memorymanager/mapping.h"
#include "memorymanager/memorymanager.h"
#include "errors/errors.h"
#include "vfs/vfs.h"
#include "kstring.h"
#include "process/process.h"
#include "helpers/kmalloc.h"
#include "imported/imported.h"
#include "elf/elf.h"
#include "usermode/usermode.h"

#define test(c, x) assert(c, x)

uint32_t test_allocator(uint32_t limit){
	for(uint32_t i = 1; i < limit+1; i++){
		uint32_t* frame = (uint32_t*)get_new_page();
		set_frame(frame, i);
		for(uint32_t y = 0; y < 1024; y++){
			if((uint32_t)(frame[y]) != i)
				return i;
		}
	}
	return 0;
}

void test_alloc(){
	char* mystr = my_calloc(8192, sizeof(char));
	kstrcpy("Hello World\n", mystr);
	kstrcpy("Hello World\n", mystr+4096);

	___SYSCALL(1, PRINT, mystr);
	___SYSCALL(1, PRINT, mystr+4096);
	my_free(mystr);

	char* secstr = my_malloc(20*sizeof(char));
	kstrcpy("Votai test.\n", secstr);
	___SYSCALL(1, PRINT, secstr);
}

void test_vmap(){
	extern process_break_info info;
	
#define LIMIT 17000
	char* lol =  add_space(&info, (void*)0xfeeddad, LIMIT, PG_WRITABLE);
	for(size_t i = 0; i < LIMIT; i++)
		lol[i] = 0;
	kstrcpy("Hello World of the sheitan\n", lol);
	q_print(lol);

	char* lel =  add_space(&info, (void*)0xfeeddaf, LIMIT, PG_WRITABLE);
	for(size_t i = 0; i < LIMIT; i++)
		lel[i] = 0;
	kstrcpy("Hello World of the pinguin\n", lel);
	q_print(lel);
	q_print(lol);

	char* pikachu = "Hello World of mapped.\n";
	char* addr = set_then_apply(&info, (void*)0xdeadbeef, pikachu, 24, PG_NOFLAG);
	ASM_I(
		push %eax;
		movl	%cr3,%eax;
		movl	%eax,%cr3;
		pop %eax;
	);
	// Next line should trigger page protection violation:
	// addr[1] = 'l';
	q_print(addr);
#undef LIMIT
}

void print_ramdisk(){
	for(size_t i = 0; i < imported_file_n; i++){
		q_print("\n+ Filename: ");
		q_print(imported_files[i].filename);
		q_print("\n- Size: ");
		q_print_size_t(imported_files[i].size);
		q_print("\n- Text:\n");
		q_print(imported_files[i].str);
	}
}

void test_elf(){
	DISABLE_INTERRUPTS();
	//elf_load_file(RAMDISK_TO_FILE(1));
	// elf_load_file(RAMDISK_TO_FILE(2));
	// elf_load_file(RAMDISK_TO_FILE(3));
	elf_load_file(RAMDISK_TO_FILE(0));
	// elf_load_file(RAMDISK_TO_FILE(1));
	// elf_load_file(RAMDISK_TO_FILE(2));
	// elf_load_file(RAMDISK_TO_FILE(2));
	// elf_load_file(RAMDISK_TO_FILE(0));
	//ENABLE_INTERRUPTS();
}

void test_ring3(){
	extern process_break_info info;
	void* pikalul = add_space(&info, (void*)0xffadbeef, 8192, PG_USERLAND | PG_WRITABLE);
	pikalul += 8180;
	asm volatile("mov %0,%%esp"::"r"(pikalul));
	jump_usermode_global();
}

void launch_tests(){
	// test_vmap();
	// test_alloc();
	// print_ramdisk();
	test_elf();
	// test_ring3();
}