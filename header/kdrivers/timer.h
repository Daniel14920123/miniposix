#include "std.h"

/**
 * @brief inits timer count.
 * 
 * @param frequency 
 */
void init_timer(uint32_t frequency);