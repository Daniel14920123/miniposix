#ifndef kDRIVERS_STDIN_H
#define kDRIVERS_STDIN_H

#include "std.h"

void startInput();
char* getInput();
void stopInput();
void Inputn(char* buffer, size_t n, char* del);

#endif