#ifndef kDRIVERS_H
#define kDRIVERS_H

#include "tables/tables.h"
#include "timer.h"
#include "casm/inlineassembly.h"
#include "keyboard.h"
#include "stdin.h"

/**
 * @brief Loads all built-in (plug and play) drivers.
 * 
 */
void loadKDrivers(void);

#endif