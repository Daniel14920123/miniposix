#ifndef TEXTMODESHELL_H
#define TEXTMODESHELL_H

#include "std.h"
#include "tmterm/tmterm.h"

void print_prompt(char* username);
void handle_command(char* command);
void input();

#endif