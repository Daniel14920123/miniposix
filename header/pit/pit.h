#ifndef PIT_H
#define PIT_H


size_t ms_to_tick(size_t delay);
void set_pit_count(unsigned count);
void pit_set_mode(int32_t mode);
void timer_phase(int hz);

#endif