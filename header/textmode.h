/**
 * @file textmode.h
 * @author daniel.frederic
 * @brief File used for VGA text mode display
 * @version 0.1
 * @date 2021-01-12
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef kTEXT_MODE_H
#define kTEXT_MODE_H

#include "std.h"
#include "kstring.h"

/**
 * @brief Number of rows, aka 25 here
 * 
 */
#define VGA_ROW 25

/**
 * @brief Number of columns, aka 80 here
 * 
 */
#define VGA_COLUMN 80

/**
 * @brief Number of bytes per pixel
 * 
 */
#define VGA_BYTE_PER_PIXEL 2

/**
 * @brief Light gray on black text color
 * 
 */
#define WHITE_TXT 0x07 /* light gray on black text */

/**
 * @brief Clears the whole screen
 * 
 */
void k_clear_screen();

/**
 * @brief Low level function to print line
 * 
 * @param message the text, should be of length < VGA_COLUMN
 * @param line The line, between 0 and VGA_ROW
 * @param n ptr to the line number
 * @return unsigned int 
 */
unsigned int k_printf(char *message, unsigned int line, int* n);

/**
 * @brief k_printf with scrolling
 * 
 * @param message 
 * @return unsigned int 
 */
unsigned int scroll_printf(char *message);

/**
 * @brief prints an hex number
 * 
 * @param nombre 
 */
void printfHex(uint16_t nombre);

/**
 * @brief Prints
 * 
 * @param message The text
 * @return unsigned int error code later, but garbage atm
 */
void kprintf(char *message);

#endif