#ifndef kPAGING_FLAGS_H
#define kPAGING_FLAGS_H

#define PG_NOFLAG    0
#define PG_PRESENT   1
#define PG_WRITABLE  2
#define PG_USERLAND  4
#define PG_W_THROUGH 8
#define PG_CACHE_DIS 16
#define PG_ACCESSED  32

#define PG_STACK     512

#endif