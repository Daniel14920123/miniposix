#ifndef kPAGING
#define kPAGING

#include "std.h"
#include "tmterm/tmterm.h"
#include "flags.h"

#define NEW_EMPTY_TABLE(x) \
	uint32_t x[1024] __attribute__((aligned(4096))); \
	for(unsigned int x##i = 0; x##i < 1024; x##i ++){ \
		x[x##i] = (x##i * 0x1000) | 2; \
	}

#define NEW_TABLE(x, flags) \
	uint32_t x[1024] __attribute__((aligned(4096))); \
	for(unsigned int x##i = 0; x##i < 1024; x##i ++){ \
		x[x##i] = (x##i * 0x1000) | (flags); \
	}

#define NEW_DIRECTORY(x) \
	uint32_t x[1024] __attribute__((aligned(4096))); \
	for(unsigned int x##i = 0; x##i < 1024; x##i ++){ \
		x[x##i] = 2; \
	} \
	x[1023] = (uint32_t)x | 3; 

#define UNPACK_ADDR(x, index) ((uint32_t*)((x)[index] & ~0xFFF))
#define ASSIGN_PRESENT_ADDR(dir, index, page_or_addr) dir[index] = (uint32_t)page_or_addr | 3

void loadPageDirectory(unsigned int* table);
void enablePaging();
void disablePaging();

void set_frame(void* frame, uint32_t value);
void identity_map_table(void* frame, size_t pde_entry);
void identity_map_directory(void* frame);
void* set_up_directory();
void* craft_addr(uint16_t pde, uint16_t pte, uint16_t offset);

void* get_physaddr(void* virtualaddr);
void map_page(void * physaddr, void * virtualaddr, unsigned int flags);
uint32_t map_pte(uint32_t entry);
uint32_t copy_frame(uint32_t entry);
uint32_t duplicate_frame(uint32_t entry);
void* get_physical(void* vaddr, uint32_t* page_directory);
#endif