#ifndef kMACRO_H
#define kMACRO_H

#include "convert.h"

#define stringify2(x) #x
#define stringify(x) stringify2(x)
#define LINE_TO_S stringify(__LINE__)
/*
                    char PANIC_LINE[7] =  {0}; \
                    itoa(__LINE__, PANIC_LINE); \
                    multiprint(get_current_term(), __FILE__); \
                    multiprint(get_current_term(), " l. "); \
                    multiprint(get_current_term(), PANIC_LINE); \
                    multiprint(get_current_term(), " -> "); \
                    multiprint(get_current_term(), x); \
*/

#define PANIC(x)    {asm volatile("cli"); \
                    multiprint(get_current_term(), __FILE__ " l. " LINE_TO_S " -> "); \
                    multiprint(get_current_term(), x); \
                    multiflush(); \
                    while(1);}

#define assert(c, x) if(!(c)) {multiprint(get_current_term(), #c); multiprint(get_current_term(), " not met : "); PANIC(x)}


#define ASM_I(...) asm volatile(#__VA_ARGS__)
#define ENABLE_INTERRUPTS() asm volatile ("sti");
#define DISABLE_INTERRUPTS() asm volatile ("cli");

#define WAIT_INTERRUPTS() while(1);

#endif