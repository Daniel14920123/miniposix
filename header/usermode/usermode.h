#ifndef kUSERMODE
#define kUSERMODE

extern void jump_usermode(void* _start);
extern void jump_usermode_global();
extern void* jump_addr;

#endif