#ifndef POSIX
#define POSIX

//https://linuxhint.com/list_of_linux_syscalls/

#include "std.h"

#include "posix_constants.h"

typedef size_t ssize_t;
typedef uint8_t mode_t;
typedef size_t nfds_t;
typedef size_t off_t;


/**
 * @brief Reads from a specified file using a file descriptor. Before using this call, you must first obtain a file descriptor using the opensyscall. 
 * 
 * @param fd file descriptor
 * @param buf pointer to the buffer to fill with read contents
 * @param count number of bytes to read
 * @return ssize_t bytes read successfully
 */
ssize_t read(int fd, void *buf, size_t count);

/**
 * @brief Writes to a specified file using a file descriptor. Before using this call, you must first obtain a file descriptor using the open syscall. Returns bytes written successfully.
 * 
 * @param fd file descriptor
 * @param buf pointer to the buffer to write
 * @param count number of bytes to write
 * @return ssize_t bytes written successfully
 */
ssize_t write(int fd, const void *buf, size_t count);

/**
 * @brief Opens or creates a file, depending on the flags passed to the call. Returns an integer with the file descriptor.
 * 
 * @param pathname pointer to a buffer containing the full path and filename
 * @param flags integer with operation flags (see below)
 * @param mode (optional) defines the permissions mode if file is to be created
 * @return int file descriptor
 */
int open(const char *pathname, int flags, mode_t mode);

/**
 * @brief Close a file descriptor. After successful execution, it can no longer be used to reference the file.
 * 
 * @param fd file descriptor to close
 * @return int 
 */
int close(int fd);

/**
 * @brief Returns information about a file in a structure named stat.
 * 
 * @param path pointer to the name of the file
 * @param buf pointer to the structure to receive file information
 * @return int 
 */
int stat(const char *path, struct stat *buf);

/**
 * @brief Works exactly like the stat syscall except a file descriptor (fd) is provided instead of a path.
 * 
 * @param fd file descriptor
 * @param buf pointer to stat buffer (described in stat syscall)
 * @return int 
 */
int fstat(int fd, struct stat *buf);

/**
 * @brief Works exactly like the stat syscall, but if the file in question is a symbolic link, information on the link is returned rather than its target.
 * 
 * @param path full path to file
 * @param buf pointer to stat buffer (described in stat syscall)
 * @return int 
 */
int lstat(const char *path, struct stat *buf);

/**
 * @brief Wait for an event to occur on the specified file descriptor.
 * 
 * @param fds pointer to an array of pollfd structures (described below)
 * @param nfds number of pollfd items in the fds array
 * @param timeout sets the number of milliseconds the syscall should block (negative forces poll to return immediately)
 * @return int 
 */
int poll(struct pollfd *fds, nfds_t nfds, int timeout);

/**
 * @brief This syscall repositions the read/write offset of the associated file descriptor. Useful for setting the position to a specific location to read or write starting from that offset.
 * 
 * @param fd file descriptor
 * @param offset offset to read/write from
 * @param whence specifies offset relation and seek behavior
 * @return off_t Returns resulting offset in bytes from the start of the file.
 */
off_t lseek(int fd, off_t offset, int whence);

/**
 * @brief Maps files or devices into memory.
 * 
 * @param addr location hint for mapping location in memory, otherwise, if NULL, kernel assigns address
 * @param length length of the mapping
 * @param prot specifies memory protection of the mapping
 * @param flags control visibility of mapping with other processes
 * @param fd file descriptor
 * @param offset file offset
 * @return void* Returns a pointer to the mapped file in memory.
 */
void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);

/**
 * @brief Sets or adjusts protection on a region of memory.
 * 
 * @param addr pointer to region in memory
 * @param len (NOT IN OUR DOCUMENTATION) length
 * @param prot protection flag
 * @return int 0 when successful
 */
int mprotect(void *addr, size_t len, int prot);

/**
 * @brief Unmaps mapped files or devices.
 * 
 * @param addr pointer to mapped address
 * @param len size of mapping
 * @return int 
 */
int munmap(void *addr, size_t len);

/**
 * @brief Allows for altering the program break that defines end of process’s data segment.
 * 
 * @param addr new program break address pointer
 * @return int 0 when successful.
 */
int brk(void *addr);

/**
 * @brief Change action taken when process receives a specific signal (except SIGKILL and SIGSTOP).
 * 
 * @param signum signal number
 * @param act structure for the new action
 * @param oldact structure for the old action
 * @return int 
 */
int rt_sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);


int sigprocmask(int how, const sigset_t *set, sigset_t *oldset);

/**
 * @brief Return from signal handler and clean the stack frame.
 * 
 * @param __unused 
 * @return int 
 */
int sigreturn(unsigned long __unused);

/**
 * @brief Set parameters of device files.
 * 
 * @param d open file descriptor the device file
 * @param request request code
 * @param ... untyped pointer
 * @return int 
 */
int ioctl(int d, int request, ...);

/**
 * @brief Read from file or device starting at a specific offset.
 * 
 * @param fd file descriptor
 * @param buf pointer to read buffer
 * @param count bytes to read
 * @param offset offset to read from
 * @return ssize_t bytes read
 */
ssize_t pread64(int fd, void *buf, size_t count, off_t offset);

/**
 * @brief Write to file or device starting at a specific offset.
 * 
 * @param fd file descriptor
 * @param buf pointer to buffer
 * @param count bytes to write
 * @param offset offset to start writing
 * @return ssize_t bytes written
 */
ssize_t pwrite64(int fd, void *buf, size_t count, off_t offset);

/**
 * @brief 
 * 
 * @param fd 
 * @param iov 
 * @param iovcnt 
 * @return ssize_t 
 */
ssize_t readv(int fd, const struct iovec *iov, int iovcnt);

#endif