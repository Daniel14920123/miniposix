#ifndef kIPC_STRUCTURES
#define kIPC_STRUCTURES

#include "std.h"
#include "process/process.h"

/**
 * @brief 
 * @warning Shall stay in the other virtual memory map, should copy
 */
typedef struct {
    size_t signal;
    size_t data_size;
    void* data;
} message;

typedef struct mailbox{
    message data;
    process* sender;
    struct mailbox* next;
} mailbox;

typedef struct port{
    process* linked;
    mailbox* msg_queue;
    size_t port_name;
    size_t white_list_size;
    size_t* white_list;
    struct port* next;
} port;


#endif