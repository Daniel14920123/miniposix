#ifndef kIPC
#define kIPC

#include "std.h"
#include "structures.h"

void request(size_t port);
size_t receive(size_t port_pid, void* addr, size_t len, pid_t* sender);
void send(size_t port_pid, process *sender_proc, message msg);
size_t bind_a_port(process* proc);
port *GetPortWithId(size_t pid);
bool bind_that_port(process* proc, size_t port_id);
#endif