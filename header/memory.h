#ifndef kMEMORY_H
#define kMEMORY_H

#include "std.h"

void *memset(void *b, int c, size_t len);
void *memcpy(void *dest, const void *src, size_t n);

#endif