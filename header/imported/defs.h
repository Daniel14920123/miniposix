#ifndef kIMPORTED_DEFS_H
#define kIMPORTED_DEFS_H

#include "std.h"

typedef struct {
	char* filename;
	char* str;
	unsigned long size;
} Ramdisk_file;

extern const uint16_t imported_file_n;
extern Ramdisk_file imported_files[];

#endif