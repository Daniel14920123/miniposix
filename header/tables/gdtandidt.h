#ifndef kTABLE_GDT_H
#define kTABLE_GDT_H

#include "std.h"
#include "isr.h"
/*
typedef struct {
   uint16_t limit_low;           // The lower 16 bits of the limit.
   uint16_t base_low;            // The lower 16 bits of the base.
   uint8_t  base_middle;         // The next 8 bits of the base.
   uint8_t  access;              // Access flags, determine what ring this segment can be used in.
   uint8_t  granularity;
   uint8_t  base_high;           // The last 8 bits of the base.
} gdt_entry __attribute__((packed));

typedef struct {
   uint16_t limit;               // The upper 16 bits of all selector limits.
   uint32_t base;                // The address of the first gdt_entry_t struct.
} gdt_ptr;

*/

#define IDT_DESC_PRESENT 0x80
/**
 * @brief gdt entry. packed to get rid of bus-alignment.
 * 
 */
typedef struct{
	uint16_t limit_low;
	uint16_t base_low;
	uint8_t base_middle;
	uint8_t access;
	uint8_t granularity;
	uint8_t base_high;
}__attribute__((packed)) gdt_entry_t;

/**
 * @brief pointer to gdt.
 * 
 */
typedef struct{
	uint16_t limit;		// First 16 bits of all selector
	uint32_t base;		// Adress of the first entry
} __attribute__((packed)) gdt_ptr_t;


typedef struct
{
	uint16_t	ie_base_lo; /* The lower 16 bits of the address to jump to when this interrupt fires. */
	uint16_t	ie_sel;     /* Kernel segment selector. */
	uint8_t		ie_always0; /* This must always be zero. */
	uint8_t		ie_flags;   /* More flags. See documentation. */
	uint16_t	ie_base_hi; /* The upper 16 bits of the address to jump to. */
}__attribute__((packed)) idt_entry_t;

/**
 * @brief pointer to an array of interrupt handlers, suitable for 'lidt' call
 * 
 */
typedef struct
{
	uint16_t limit;
	uint32_t base; 
}__attribute__((packed)) idt_ptr_t;

typedef struct{
	uint16_t handlerAddressLowBits;
	uint16_t gdt_codeSegmentSelector;
	uint8_t reserved;
	uint8_t access;
	uint16_t handlerAddressHighBits;
} GateDescriptor;

void init_descriptor_tables();

extern void  isr0(void);
extern void  isr1(void);
extern void  isr2(void);
extern void  isr3(void);
extern void  isr4(void);
extern void  isr5(void);
extern void  isr6(void);
extern void  isr7(void);
extern void  isr8(void);
extern void  isr9(void);
extern void isr10(void);
extern void isr11(void);
extern void isr12(void);
extern void isr13(void);
extern void isr14(void);
extern void isr15(void);
extern void isr16(void);
extern void isr17(void);
extern void isr18(void);
extern void isr19(void);
extern void isr20(void);
extern void isr21(void);
extern void isr22(void);
extern void isr23(void);
extern void isr24(void);
extern void isr25(void);
extern void isr26(void);
extern void isr27(void);
extern void isr28(void);
extern void isr29(void);
extern void isr30(void);
extern void isr31(void);
extern void  irq0(void);
extern void  irq1(void);
extern void  irq2(void);
extern void  irq3(void);
extern void  irq4(void);
extern void  irq5(void);
extern void  irq6(void);
extern void  irq7(void);
extern void  irq8(void);
extern void  irq9(void);
extern void irq10(void);
extern void irq11(void);
extern void irq12(void);
extern void irq13(void);
extern void irq14(void);
extern void irq15(void);

///ADD all additional handlers and in assembly/gdtandidt.asm
extern void isr128(void);

/**
 * @brief Flushes gdt.
 * 
 */
extern void gdt_flush(uint32_t);

/**
 * @brief Flushed idt.
 * 
 */
extern void idt_flush(uint32_t);

/**
 * @brief buffer of packed interrupt handlers.
 * 
 */
extern isr_t interrupt_handlers[];

void SetInterruptDescriptorTableEntry(
		uint8_t interruptNumber,
		uint16_t codeSegmentSelectorOffset,
		void (*handler)(),
		uint8_t DescriptorPrivilege,
		uint8_t DescriptorType
		);


#endif