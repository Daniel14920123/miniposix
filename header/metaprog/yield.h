#define yieldpush() while(1){static int state=0; switch(state) { case 0:
#define yieldinst(code, x) { state=__LINE__; {code;} return x ; case __LINE__:; }
#define yieldnoinst(x) { state=__LINE__; return x; case __LINE__:; }
#define yieldselector(_a1, _a2, pick, ...) pick
#define yield(...) yieldselector(__VA_ARGS__, yieldinst, yieldnoinst) (__VA_ARGS__)
#define yieldpop() state = 0;}}