#ifndef kELF_H
#define kELF_H

#include "std.h"
#include "v2.h"

typedef struct{
    size_t position;
    char* string;
    size_t strlength;
    uint8_t flags;
} FILE;

#define RAMDISK_TO_FILE(index) ((FILE){.position = 0, .string = imported_files[index].str, .strlength = imported_files[index].size, .flags = 0})

void *elf_load_file(FILE file);


#endif