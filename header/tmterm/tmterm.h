#ifndef kTMTERM_H
#define kTMTERM_H

#include "std.h"
#include "kstring.h"

/**
 * @brief Number of rows, aka 25 here
 * 
 */
#define VGA_ROW 25

/**
 * @brief Number of columns, aka 80 here
 * 
 */
#define VGA_COLUMN 80

/**
 * @brief Number of bytes per pixel
 * 
 */
#define VGA_BYTE_PER_PIXEL 2

/**
 * @brief Light gray on black text color
 * 
 */
#define WHITE_TXT 0x07 /* light gray on black text */

#define MAX_TERM_NUMBER 5

#define multiflush() multiswitch(get_current_term())
#define q_print(x) {multiprint(get_current_term(), x); multiflush();}
#define q_print_int(x) {char buff##__LINE__ [7]; itoa(x, buff##__LINE__); multiprint(get_current_term(), buff##__LINE__);} multiflush()
#define q_print_size_t(x) {char buff##__LINE__ [11]; sttoa(x, buff##__LINE__); multiprint(get_current_term(), buff##__LINE__);} multiflush()

#ifdef NO_PRINT
#undef q_print
#undef q_print_int
#undef q_print_size_t
#define q_print(x)
#define q_print_int(x)
#define q_print_size_t(x)
#endif

extern char multiterms[MAX_TERM_NUMBER][VGA_COLUMN * VGA_ROW * 2];
extern int pos[MAX_TERM_NUMBER];
extern uint8_t current_term;

void multiprint(uint8_t term_num, char* message);

void multiswitch(uint8_t num);

uint8_t get_current_term();
int8_t add_term();
void remove_term(uint8_t index);
uint8_t get_term_number();

#endif