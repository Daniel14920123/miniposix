#ifndef kDEFINES_H
#define kDEFINES_H

// SYSCALLS
#define REGIONS(x)

REGIONS(-=~=-SYSCALL-=~=-)
#define ADD_DRIVER_LEGACY           0
#define PRINT                       1
#define HANDLE_KRISBOOL_INPUT       2
#define RANDOM                      3
#define SRANDOM                     4
#define ATTACH_TO_CLOCK             5
#define EXIT                        6
#define FORK                        7
#define GETPID                      8
#define GETPPID                     9
#define SEND                        10
#define RECEIVE                     11
#define BIND_PORT                   12
#define REQUEST                     13
#define SBRK                        14
#define SEND_SIGNAL                 15
#define SET_SIGNAL_HANDLER          16
#define SIGHANDLER_RETURN           17
#define EXECVE                      18
#define RQST_IO_PERMS               19
#define GET_PHYS_BUFF               20
#define BIND_TO_ISR                 21
#define RECEIVE_IRQS_ON             22
#define THREAD                      23
#define BIND_THAT_PORT              24
#define MMAP                        25
#define MUNMAP                      26

REGIONS(-=~=-SIGNALS-=~=-)
#define MAX_SIGNALS 10

#define SIGTERM     1
#define SIGSEGV     2
#define SIGABRT     3
#define SIGCHILD    4
#define SIGARITH    5
#define SIGPERM     6

#define RAND_MAX 32767

//defines if each error should trigger kernel panic
// #define PANIC_MODE
// #define NO_PRINT
#endif