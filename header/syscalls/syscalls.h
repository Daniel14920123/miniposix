#ifndef kSYSCALLS_H
#define kSYSCALLS_H

#include "std.h"
#include "defines/defines.h"
#include "tables/tables.h"

#define ___SYSCALL_STACK_b0(code) \
	{asm volatile("pusha"); \
	asm volatile("mov %%eax, %%eax": : "a" (&code)); \
	asm volatile("int $0x80");\
	asm volatile("popa");}

#define ___SYSCALL_STACK_b1(code, value) \
	{uint32_t buff[2] = {0};\
	buff[0] = (uint32_t)code; \
	buff[1] = (uint32_t)value; \
	asm volatile("pusha"); \
	asm volatile("mov %%eax, %%eax": : "a" (buff)); \
	asm volatile("int $0x80");\
	asm volatile("popa");}

#define ___SYSCALL(n, code, value) ___SYSCALL_STACK_b##n (code, value)

void syscall_callback(registers_t* regs);
void intern_callback(registers_t* regs);

#endif