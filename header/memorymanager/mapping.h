#ifndef kMAPPING_H
#define kMAPPING_H

#include "std.h"

typedef struct SMAP_entry {
 
	uint32_t BaseL; // base address uint64_t
	uint32_t BaseH;
	uint32_t LengthL; // length uint64_t
	uint32_t LengthH;
	uint32_t Type; // entry Type
	uint32_t ACPI; // extended
 
}__attribute__((packed)) SMAP_entry_t;

int __attribute__((noinline)) __attribute__((regparm(3))) detectMemory(SMAP_entry_t* buffer, int maxentries);

void count_memory(void);

#ifdef ___EFI
#define MAP_MEMORY() get_efi_mmap()
#else
#define MAP_MEMORY() get_bios_mmap()
#endif

#define BEGINNING_ADDR() 0x00100000
#define END_ADDR()      (0x10000000)
// #define END_ADDR() (0xC0000000 - 4096)
#define MEMORY_SIZE() (END_ADDR() - BEGINNING_ADDR())

#endif