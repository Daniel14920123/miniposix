#ifndef kMEMORYMANAGER
#define kMEMORYMANAGER

#include "std.h"
#include "paging/paging.h"

#define MAX_PAGE_DIRECTORY_NUM 1
/*
CRASHES THE KERNEL BECAUSE TOO BIG
typedef struct{
    uint32_t directory[1024];
    uint32_t tables[1024][1024];
} memory_map;

memory_map new_memory_map();
*/

void frame_init();

void new_empty_directory(uint32_t* to_set);
void* get_new_page();
int32_t add_page_to(uint32_t* page_directory);
void identity_map_from(uint32_t* directory, uint32_t start_addr, uint32_t end_addr);

#endif