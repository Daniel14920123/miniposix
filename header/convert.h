#ifndef OSS117_CONVERT_H_INCLUDED
#define OSS117_CONVERT_H_INCLUDED

#include "std.h"

/**
 * @brief Number of digits
 * @author daniel.frederic
 * @param number The number 
 * @return int the number of digits
 */
int digit_count(int number);

/**
 * @brief absolute value
 * @author daniel.frederic
 * @param number The number which the absolute value should be computed.
 * @return int The absolute value of number.
 */
int abs_i(int number);

/**
 * @brief String reprensentation of signed integer
 * @author daniel.frederic
 * @param number The number whose string should be got.
 * @param ret The buffer which will be filled. should be of size 7.
 */
void itoa(int number, char ret[]);

int32_t atoi(char* str);

/**
 * @brief String representation of unsigned 32-bit integer
 * @author daniel.frederic
 * @param number The number whose string should be got
 * @param ret The buffer which will be filled. should be of size 11.
 */
void sttoa(uint32_t number, char ret[]);
#endif