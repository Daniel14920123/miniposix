#ifndef kINLINE_ASSEMBLY
#define kINLINE_ASSEMBLY

#include "../std.h"

/**
 * @brief Read a 8/16/32-bit value at a given memory location using another segment than the default C data segment.\
 * Unfortunately there is no constraint for manipulating segment registers directly, so issuing the mov <reg>, <segmentreg> manually is required.
 * 
 */
static inline uint32_t farpeekl(uint16_t sel, void* off){
    uint32_t ret;
    asm ( "push %%fs\n\t"
          "mov  %1, %%fs\n\t"
          "mov  %%fs:(%2), %0\n\t"
          "pop  %%fs"
          : "=r"(ret) : "g"(sel), "r"(off) );
    return ret;
}

/**
 * @brief Write a 8/16/32-bit value to a segment:offset address too. Note that much like in farpeek,\
 *  this version of farpoke saves and restore the segment register used for the access.
 * 
 */
static inline void farpokeb(uint16_t sel, void* off, uint8_t v){
    asm ( "push %%fs\n\t"
          "mov  %0, %%fs\n\t"
          "movb %2, %%fs:(%1)\n\t"
          "pop %%fs"
          : : "g"(sel), "r"(off), "r"(v) );
    /* TODO: Should "memory" be in the clobber list here? */
}

static inline uint8_t Port8Bit_read(uint16_t port_number) {
  uint8_t result;
  __asm__ volatile("inb %1, %0" : "=a"(result) : "Nd"(port_number));
  return result;
}
static inline uint16_t Port16Bit_read(uint16_t port_number) {
  uint16_t result;
  __asm__ volatile("inw %1, %0" : "=a"(result) : "Nd"(port_number));

  return result;
}
static inline uint32_t Port32Bit_read(uint16_t port_number) {
  int32_t result;
  __asm__ volatile("inl %1, %0" : "=a"(result) : "Nd"(port_number));

  return result;
}

static inline void Port8Bit_write(uint16_t port_number, uint8_t _data){
    __asm__ volatile("outb %0, %1" : : "a"(_data), "Nd"(port_number));
}

static inline void Port16Bit_write(uint16_t port_number, uint16_t _data){
    __asm__ volatile("outw %0, %1" : : "a"(_data), "Nd"(port_number));
}

static inline void Port32Bit_write(uint16_t port_number, uint32_t _data){
    __asm__ volatile("outl %0, %1" : : "a"(_data), "Nd"(port_number));
}

/**
 * @brief Returns a true boolean value if irq are enabled for the CPU.
 * 
 */
static inline bool are_interrupts_enabled(){
    unsigned long flags;
    asm volatile ( "pushf\n\t"
                   "pop %0"
                   : "=g"(flags) );
    return flags & (1 << 9);
}

/**
 * @brief unsigned long f = save_irqdisable();\
    do_whatever_without_irqs();\
    irqrestore(f);
 * 
 * @return unsigned long flags which will be restored with irqrestore
 */
static inline unsigned long save_irqdisable(void){
    unsigned long flags;
    asm volatile ("pushf\n\tcli\n\tpop %0" : "=r"(flags) : : "memory");
    return flags;
    // return flags; ???
}

/**
 * @brief Restores irq after desablind made with save_irqdisable()
 * 
 * @param flags flags saved with save_irqdisable().
 */
static inline void irqrestore(unsigned long flags){
    asm ("push %0\n\tpopf" : : "rm"(flags) : "memory","cc");
}

/**
 * @brief Defines a new interrupt table.
 * 
 * @param base address of new interrupt table
 * @param size size of the new interrupt table
 */
static inline void lidt(void* base, uint16_t size)
{   // This function works in 32 and 64bit mode
    struct {
        uint16_t length;
        void*    base;
    } __attribute__((packed)) IDTR = { size, base };
 
    asm ( "lidt %0" : : "m"(IDTR) );  // let the compiler choose an addressing mode
}

/**
 * @brief reads the value in control register.
 * 
 * @return unsigned long read value.
 */
static inline unsigned long read_cr0(void){
    unsigned long val;
    asm volatile ( "mov %%cr0, %0" : "=r"(val) );
    return val;
}

/**
 * @brief Invalidates the TLB (Translation Lookaside Buffer) for one specific virtual address.\
 *  The next memory reference for the page will be forced to re-read PDE and PTE from main memory.\
 *  Must be issued every time you update one of those tables. warning i486
 * 
 * @param m The m pointer points to a logical address, not a physical or virtual one:\
 *  an offset for your ds segment. 
 */
static inline void invlpg(void* m){
    /* Clobber memory to avoid optimizer re-ordering access before invlpg, which may cause nasty bugs. */
    asm volatile ( "invlpg (%0)" : : "b"(m) : "memory" );
}


#endif