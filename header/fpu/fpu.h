#ifndef kFPU_H
#define kFPU_H

#include "tmterm/tmterm.h"

extern void test_fpu(void);
extern void init_sse(void);

void fpu_load_control_word(const uint16_t control);
uint16_t fpu_get_control_word();
uint16_t fpu_get_status_word();

#endif