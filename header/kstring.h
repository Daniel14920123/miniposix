#ifndef STRING_H_OS_INCLUDED
#define STRING_H_OS_INCLUDED

/**
 * @brief Length of null terminated string
 * 
 * @param str Buffer with the string
 * @return int Number of characters
 */
int kstrlen(char* str);

/**
 * @brief Strcpy, kernel version
 * 
 * @param src Source address
 * @param dest Destination address
 */
void kstrcpy(char* src, char* dest);

/**
 * @brief tells whether two string are equals or not.
 * 
 * @param s1 a string
 * @param s2 a string
 * @return int 0 if different, 1 otherwise
 */
int kstrequal(char* s1, char* s2);

#endif