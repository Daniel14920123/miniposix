#ifndef kSCHEDULING_H
#define kSCHEDULING_H

#define PROCESS_TICK 1;

#include "structure.h"

void Init_scheduler();

void schedule_tick(registers_t *regs);

void Add_process_to_schedule(process *process_to_add);

// NEED TO FREE PROCESS AFTER CALLING THIS FUCNTION
void Remove_process_to_schedule(process *process_to_remove);
void setup_scheduler();

void switch_task(registers_t *regs);
process copy_current_proc();
void remove_current_proc();
bool are_tasks_scheduled();

#endif