#ifndef kPROCESS_STRUCTURE_H
#define kPROCESS_STRUCTURE_H

#include "std.h"
#include "tables/tables.h"

#define size_t uint32_t

#define TIME_TO_SPLIT 1000
#define PROCESS_OFFSET (3 << 22)

typedef uint8_t ptime_t;
typedef uint16_t pid_t;
typedef uint16_t signal_t;

typedef struct {
    uint32_t* page_directory;
    size_t program_break;
    size_t allocated_in_current_frame;
    uint16_t pde;
    uint16_t next_pte;
} process_break_info;

typedef size_t clock_t;
struct tms {
    clock_t tms_utime;  /* user time */
    clock_t tms_stime;  /* system time */
    clock_t tms_cutime; /* user time of children */
    clock_t tms_cstime; /* system time of children */
};

typedef struct signal{
    signal_t sigcode;
    struct signal* next;
}signal;

typedef struct process_struct{
    process_break_info pbi;
    pid_t pid;
    pid_t parent;
    ptime_t time;
    registers_t regs;
    bool has_been_launched;
    size_t is_paused;
    uint32_t signal_handler;
    registers_t regs_vault_in_case_of_signal;
    signal* signal_queue;
    struct process_struct *next;
    signal_t* sigswap;
    size_t message_sent;
    bool should_exit;
} process;

#endif