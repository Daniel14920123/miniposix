#ifndef kPROCESS_SBRK_H
#define kPROCESS_SBRK_H

#include "std.h"
#include "process/structure.h"

//size_t add_memory(uint32_t size, uint32_t* page_directory);
void* sbrk(process_break_info* proc, int32_t to_add);

#endif