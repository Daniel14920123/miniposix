#ifndef kPROCESS_H
#define kPROCESS_H

#include "std.h"
#include "structure.h"
#include "scheduling.h"
#include "mapping.h"

extern void* process_directory;

void* beginning_of_frame(void* addr);
void* end_of_frame(void* addr);

process new_process(pid_t parent, ptime_t time);
void* spawnProcess(size_t text_size, size_t rodata_size);

void enableProcessFirst(process* to_enable, registers_t* regs);
void enableProcess(process* to_enable, registers_t* regs);
void stopProcess(process*);

void enableProcesses();
process_break_info fork_pbi(process_break_info info);

process fork(process* proc);
process thread(process* proc);

#endif