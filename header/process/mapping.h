#ifndef kPROCESS_MAPPING_H
#define kPROCESS_MAPPING_H
#include "std.h"
#include "structure.h"

void* add_space(process_break_info* proc, void* hintaddr, size_t length, int flags);
void* set_then_apply(process_break_info* proc, void* addr, char* mem, size_t len, int flags);
void* sbrk(process_break_info* proc, int32_t to_add);

#endif