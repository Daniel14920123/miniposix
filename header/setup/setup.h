#ifndef kSETUP_H
#define kSETUP_H

#include "std.h"
#include "convert.h"
#include "tables/tables.h"
#include "kdrivers/kdrivers.h"
#include "tmterm/tmterm.h"
#include "textmode.h"
#include "paging/paging.h"
#include "vesa/vesa.h"
#include "macros/macros.h"
#include "syscalls/syscalls.h"
#include "memorymanager/mapping.h"
#include "memorymanager/memorymanager.h"
#include "errors/errors.h"
#include "vfs/vfs.h"
#include "kstring.h"
#include "process/process.h"
#include "helpers/kmalloc.h"
#include "imported/imported.h"

#define ON_FIRST_RUN(...) \
    static int first_run = 1; \
	if(first_run) \
		{ \
            __VA_ARGS__; \
        } \
	first_run = 0;

void setup_heap();
void setup_services();
void init_terms();

#endif