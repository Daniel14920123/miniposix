#ifndef kVFS_H
#define kVFS_H

#if 0
#include "inode.h"
#include "fs.h"
#include "initrd.h"
#endif

#include "std.h"
#include "macros/macros.h"

#define DATA_MAX 512
#define MAX_NODES 1024
#define FOLDER_PATH_SEPARATOR '/'
#define ERROR_FILE_NO 0

typedef uint32_t file_descriptor;

typedef struct node{
    uint32_t fd;
    char filename[32];
    char data[DATA_MAX];
    bool is_folder;
    size_t dataeof;
    struct node* link;
    struct node* link_sub;
    struct node* symlink;
} node;

node* new_file();
node* new_file_in(node* folder);
bool free_file(file_descriptor fd);
bool get_text(file_descriptor fd, char* buffer, uint32_t buffer_size);
bool store_text(file_descriptor fd, char* buffer);
bool get_subfiles(file_descriptor folder, file_descriptor* list_of_descriptors, uint32_t list_length);
file_descriptor get_actual_file(file_descriptor fd);
file_descriptor path(file_descriptor root, char* p);

node* initrd_init();


#endif