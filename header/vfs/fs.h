#ifndef k_VFS_FS
#define k_VFS_FS

#include "std.h"
#include "inode.h"

/**
 * @brief Root of the filesystem
 * 
 */
extern inode *fs_root;

typedef struct{
  char name[128];   // Filename.
  uint32_t ino;     // Inode number. Required by POSIX.
} dirent; 

uint32_t read_fs(inode *node, uint32_t offset, uint32_t size, uint8_t *buffer);

uint32_t write_fs(inode *node, uint32_t offset, uint32_t size, uint8_t *buffer);

void open_fs(inode *node, uint8_t read, uint8_t write);

void close_fs(inode *node);

dirent *readdir_fs(inode *node, uint32_t index);

inode *finddir_fs(inode *node, char *name); 

#endif