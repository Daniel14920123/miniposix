GCCPARAM = -m32 -g -std=gnu99 -fno-stack-protector -I"./header/" -O0 -ffreestanding -nostdlib -lgcc -c -fno-zero-initialized-in-bss -fno-omit-frame-pointer -Wall -Wextra
ASMPARAM = -f elf32 -F dwarf
LDPARAMS = -m elf_i386
OUTPUT   = bin
PKGMGR   = apt install

objects = obj/kernel.o obj/textmode.o obj/kstring.o obj/convert.o obj/core.o obj/tables.o obj/memory.o obj/syscalls.o obj/drivers.o obj/tmterm.o obj/paging.o  obj/memory_manager.o obj/errors.o obj/vfs.o obj/process.o obj/scheduling.o obj/kmemory.o obj/malloc.o obj/fpu.o obj/tests.o obj/elf.o obj/setup.o obj/packed_fs.o obj/IPC.o obj/pit.o

ifneq (, $(findstring dbg,$(MAKECMDGOALS)))
STRICT = -Wall -Wextra
endif

run: qemu_iso

# Build section
obj/%.o: src/%.c .lol
	@mkdir -p $(@D)
	@gcc $(GCCPARAM) $(STRICT) -o $@ -c $<

obj/%.o: assembly/%.asm .lol
	@mkdir -p $(@D)
	@nasm $(ASMPARAM) -o $@ $<

$(OUTPUT)/pEpitOS: ./link.ld pack_ramdisk $(objects)
	@mkdir -p $(OUTPUT)
	@ld $(LDPARAMS) -T $< -o $@ $(objects)
	@$(MAKE) clean -C ./ramdisk_packer -s

# Multiboot spec checker
check_header: $(OUTPUT)/pEpitOS _check_header

_check_header:
	@chmod +x ./check_multiboot.sh
	@./check_multiboot.sh

# Build iso
pEpitOS.iso: $(OUTPUT)/pEpitOS check_header
	@mkdir -p iso
	@mkdir -p iso/boot
	@mkdir -p iso/boot/grub
	@touch iso/boot/grub/grub.cfg
	@cp -r $(OUTPUT) iso/boot/$(OUTPUT)
	@echo 'menuentry "pEpitOS" {'  				>> iso/boot/grub/grub.cfg 
	@echo '  multiboot /boot/$<'          		>> iso/boot/grub/grub.cfg 
	@echo '}'                                  	>> iso/boot/grub/grub.cfg ;
	@echo "---------------------------"
	@echo "+ BEGINNING OF XORRISO LOG:"
	@echo "---------------------------"
	@grub-mkrescue /usr/lib/grub/i386-pc -o $@ iso
	@echo "---------------------------"
	@echo "+ END OF XORRISO LOG       "
	@echo "---------------------------"
	@rm -rf iso

# Call SubMakefiles
## Ramdisk
pack_ramdisk: .lol build_subs
	@$(MAKE) -C ./ramdisk_packer -s

## Build and pack Elfs
build_subs: .lol
	@$(MAKE) -C ./userspace/elf_builder -s
	@cp -a ./userspace/elf_builder/build/. ./ramdisk_packer/target/

build_drivers: .lol
	@$(MAKE) -C ./userspace/drivers -s
	@cp -a ./userspace/drivers/build/. ./ramdisk_packer/target/

# Emulators, to fix
qemu_iso: pEpitOS.iso
	@qemu-system-i386 -S -gdb tcp::9000 -cdrom $< -no-reboot -monitor stdio -vga std -D ./log.txt -d int,guest_errors -m 512M -boot d -usbdevice disk:myimage.img ##-hdb fat:./ramdisk_packer/target/

qemu: check_header
	@qemu-system-i386 -S -gdb tcp::9000 -kernel $(OUTPUT)/kernel -no-reboot -monitor stdio -vga std -D ./log.txt -d int,guest_errors -m 512M

bochs: pEpitOS.iso
	bochs debug -f bochsconfig.cfg

vmware: kernel.iso
	vmware pEpitOS.iso

# Distribution
distrib: pEpitOS.iso
	@bzip2 $<

# Dependencies
configure:
	sudo $(PKGMGR) qemu nasm grub-pc-bin grub-common
	mkdir bin

# Cleaning
.PHONY: clean

clean:
	@$(MAKE) -C ./userspace/elf_builder -s clean
	@$(MAKE) -C ./ramdisk_packer -s clean
	rm -rf obj kernel.bin kernel.iso
	rm -rf obj
	rm -f kernel
	rm -f log.txt
	rm -f pEpitOS.iso
	rm -f pEpitOS.iso.bz2
	rm -f bin/*
	rm -f log.txt

#force target
.lol: