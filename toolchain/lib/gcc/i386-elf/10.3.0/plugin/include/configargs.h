/* Generated automatically. */
static const char configuration_arguments[] = "../gcc/configure --target=i386-elf --prefix=/home/sda/Workspace/miniposix/toolchain/ --disable-nls --enable-languages=c,c++ --without-headers";
static const char thread_model[] = "single";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "i386" }, { "arch", "i386" } };
