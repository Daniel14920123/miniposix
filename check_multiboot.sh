#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

grub-file --is-x86-multiboot bin/pEpitOS 2> /dev/null
if [ $? -eq 0 ]; then
    echo -e "${GREEN}[OK] This file is multiboot 1 compliant."
else
    echo -e "${RED}[FAIL] This file is not multiboot 1 compliant."
fi

grub-file --is-x86-multiboot2 bin/pEpitOS 2> /dev/null
if [ $? -eq 0 ]; then
    echo -e "${GREEN}[OK] This file is multiboot 2 compliant."
else
    echo -e "${RED}[FAIL] This file is not multiboot 2 compliant."
fi
echo -e "${NC}"