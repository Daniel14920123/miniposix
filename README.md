# pEpitOS-i386

This project aims to be an hobbyist kernel. It will be ported on other architectures with a better code design.
This one runs on i386 processors. It is a highly customisable microkernel with preemptive multitasking.
You also have linker scripts and a toolchain joined.

## Tests

If you want to test functionalities, go to userspace/elf_builder. Modify the executables target in the Makefile. Some tests are pre-made.
Juste uncomment the desired line.
[Elf builder](./userspace/elf_builder)
[Makefile](./userspace/elf_builder/Makefile)

## Bootloader

We are using grub at the moment (kernel multibbot 1 compliant), but there is also another bootloader written but not yet integrated.
See [this repo](https://gitlab.com/Daniel14920123/loboot)

## Running the project

### Basics

First uncomment the desired test in [this Makefile](./userspace/elf_builder/Makefile). Then go to the root of the project.
Enter the following commands:

- On Ubuntu:

```bash
make config
make clean
make
```

If you already have all the dependencies, you don't have to execute make config.

- On other systems:

Make sure you have qemu-system-i386, nasm, grub-pc-bin, grub-common, and an i386 gcc

```
mkdir bin
make
```
